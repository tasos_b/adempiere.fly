package org.fly.swing.controls.base;


import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.util.HashMap;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.plaf.FontUIResource;

import org.fly.swing.controls.IControlCommon;

public class FLYBaseGrid extends FLYBaseControl implements IControlCommon{

	private static final long serialVersionUID = 5532092477907424157L;
	
	JTable table ;
	JScrollPane scrollPane ;
	private Font _font = new FontUIResource("Verdana", Font.PLAIN, 18);

	public FLYBaseGrid(){
		CreateTable();
	}
	
	public void CreateTable(){
		
		final Object rows[][] = { { "one", "1", 22.98 }, { "two", "2" , 22.98 }, { "three", "3" , 22.98} };
	    final Object headers[] = { "English", "#" , "gfgf"};
	    table = new JTable(rows, headers);
	    table.setSize(table.getMaximumSize()); 
		
	    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.add(scrollPane);
		scrollPane.setViewportView(table);
		setCustomFont(_font);
	}

	
	public JTable getTable(){
		return table;
	}

	@Override
	public void setCustomFont(Font font) {
		table.setFont(_font);
		table.getTableHeader().setFont(_font);
		Canvas c = new Canvas();
		FontMetrics metrics = c.getFontMetrics(_font);
		int height = metrics.getHeight() + (metrics.getHeight() / 2);
		table.setRowHeight(height);
		scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(metrics.getHeight() + (metrics.getHeight() / 2), 0));
		scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(metrics.getHeight() + (metrics.getHeight() / 2), 10));
		
	}

	@Override
	public void notify(int message, HashMap<String, Object> data) {
		// TODO Auto-generated method stub
		
	}

	
}
