
insert into adempiere.AD_EntityType
(entitytype, ad_client_id, ad_org_id, ad_entitytype_id, createdby, updatedby, 
name, help, version, processing)
values
('FLY', 0, 0, 80000, 0, 0,
'FLY Pos', 'FLY Pos', 1.00, 'N');


CREATE TABLE adempiere.fl_terminal (
  fl_terminal_id numeric(10,0) NOT NULL,
  fl_terminal_uu character varying(36) DEFAULT NULL::character varying,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL,
  CONSTRAINT fl_terminal_pkey PRIMARY KEY (fl_terminal_id)
);


CREATE TABLE adempiere.fl_control (
  fl_control_id numeric(10,0) NOT NULL,
  fl_control_uu character varying(36) DEFAULT NULL::character varying,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL,
  CONSTRAINT fl_control_pkey PRIMARY KEY (fl_control_id)
);


insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (100, 0, 0, 0, 0, 'Free Buttons');


insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (101, 0, 0, 0, 0, 'Receipt Lines');

insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (102, 0, 0, 0, 0, 'Product Categories');

insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (103, 0, 0, 0, 0, 'Products');


insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (104, 0, 0, 0, 0, 'Receipt Summary');


insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (105, 0, 0, 0, 0, 'Barcode');


insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (106, 0, 0, 0, 0, 'Quantity Numpad');


CREATE TABLE adempiere.fl_layout (
  fl_layout_id numeric(10,0) NOT NULL,
  fl_layout_uu character varying(36) DEFAULT NULL::character varying,
  fl_terminal_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL,
  CONSTRAINT fl_layout_pkey PRIMARY KEY (fl_layout_id),
  CONSTRAINT fl_layout_fl_terminal_fkey FOREIGN KEY (fl_terminal_id)
  	REFERENCES adempiere.fl_terminal(fl_terminal_id)
);


CREATE TABLE adempiere.fl_layoutcontrol (
  fl_layoutcontrol_id numeric(10,0) NOT NULL,
  fl_layoutcontrol_uu character varying(36) DEFAULT NULL::character varying,
  fl_layout_id numeric(10,0) NOT NULL,
  fl_control_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  control_x numeric(10,0) NOT NULL DEFAULT 0,
  control_y numeric(10,0) NOT NULL DEFAULT 0,
  control_width numeric(10,0) NOT NULL DEFAULT 200,
  control_height numeric(10,0) NOT NULL DEFAULT 200,
  name character varying(60) NOT NULL,
  CONSTRAINT fl_layoutcontrol_pkey PRIMARY KEY (fl_layoutcontrol_id),
  CONSTRAINT fl_layoutcontrol_fl_layout_fkey FOREIGN KEY (fl_layout_id)
  	REFERENCES adempiere.fl_layout(fl_layout_id),
  CONSTRAINT fl_layoutcontrol_fl_control_fkey FOREIGN KEY (fl_control_id)
  	REFERENCES adempiere.fl_control(fl_control_id)
);

CREATE TABLE adempiere.fl_process (
  fl_process_id numeric(10,0) NOT NULL,
  ad_process_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL,
  data bytea,
  CONSTRAINT fl_process_pkey PRIMARY KEY (fl_process_id),
  CONSTRAINT fl_process_ad_process_fkey FOREIGN KEY (ad_process_id)
	REFERENCES adempiere.ad_process(ad_process_id)
);


alter table adempiere.ad_process
add isaction character(1) NOT NULL DEFAULT 'N'::bpchar;


alter table adempiere.fl_process
add runbutton character varying(60) ;




alter table adempiere.fl_layoutcontrol
add   data bytea;

alter table adempiere.fl_layoutcontrol
add runbutton character varying(60) ;

alter table adempiere.fl_layoutcontrol
add ad_process_id numeric(10,0) ;

alter table adempiere.fl_layoutcontrol
add  CONSTRAINT fl_layoutcontrol_ad_process_fkey FOREIGN KEY (ad_process_id)
	REFERENCES adempiere.ad_process(ad_process_id);
	

insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (107, 0, 0, 0, 0, 'Product Classification');


insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (108, 0, 0, 0, 0, 'Product Class');


insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (109, 0, 0, 0, 0, 'Product Group');




CREATE TABLE adempiere.fl_ticket (
  fl_ticket_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  CONSTRAINT fl_ticket_pkey PRIMARY KEY (fl_ticket_id)
);

CREATE TABLE adempiere.fl_ticket_order (
  fl_ticket_order_id numeric(10,0) NOT NULL,
  fl_ticket_id numeric(10,0) NOT NULL,
  C_Order_ID numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  CONSTRAINT fl_ticket_order_pkey PRIMARY KEY (fl_ticket_order_id),
  CONSTRAINT fl_ticket_order_fl_ticket_fkey FOREIGN KEY (fl_ticket_id)
  	REFERENCES adempiere.fl_ticket(fl_ticket_id),
  CONSTRAINT fl_ticket_order_c_order_fkey FOREIGN KEY (C_Order_ID)
  	REFERENCES adempiere.C_Order(C_Order_ID)

);

alter table adempiere.fl_ticket
add C_Order_ID numeric(10,0);

alter table adempiere.fl_ticket
add C_OrderLine_ID numeric(10,0);

alter table adempiere.fl_ticket
add CONSTRAINT fl_ticket_c_order_fkey FOREIGN KEY (C_Order_ID)
  	REFERENCES adempiere.C_Order(C_Order_ID);

alter table adempiere.fl_ticket
add CONSTRAINT fl_ticket_c_orderline_fkey FOREIGN KEY (C_OrderLine_ID)
  	REFERENCES adempiere.C_OrderLine(C_OrderLine_ID);

alter table adempiere.fl_ticket
add fl_terminal_id numeric(10,0);
 
alter table adempiere.fl_ticket
add CONSTRAINT fl_ticket_fl_terminal_fkey FOREIGN KEY (fl_terminal_id)
  	REFERENCES adempiere.fl_terminal(fl_terminal_id);

alter table adempiere.fl_ticket
add isclosed character(1) NOT NULL DEFAULT 'N'::bpchar;

alter table adempiere.fl_ticket_order
add isclosed character(1) NOT NULL DEFAULT 'N'::bpchar;

alter table adempiere.fl_terminal 
add C_BPartner_ID numeric(10,0);

alter table adempiere.fl_terminal 
add CONSTRAINT fl_terminal_C_BPartner_fkey FOREIGN KEY (C_BPartner_ID)
  	REFERENCES adempiere.C_BPartner(C_BPartner_ID);
  	
 	
CREATE TABLE adempiere.fl_process_group (
  fl_process_group_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL,
  CONSTRAINT fl_process_group_pkey PRIMARY KEY (fl_process_group_id)
);

CREATE TABLE adempiere.fl_process_group_link (
  fl_process_group_link_id numeric(10,0) NOT NULL,
  fl_process_group_id numeric(10,0) NOT NULL,
  fl_process_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL,
  CONSTRAINT fl_process_group_link_pkey PRIMARY KEY (fl_process_group_link_id),
  CONSTRAINT fl_process_group_link_fl_process_group_fkey FOREIGN KEY (fl_process_group_id)
  	REFERENCES adempiere.fl_process_group(fl_process_group_id),
  CONSTRAINT fl_process_group_link_fl_process_fkey FOREIGN KEY (fl_process_id)
  	REFERENCES adempiere.fl_process(fl_process_id)
);

CREATE TABLE adempiere.fl_layoutcontrol_button (
  fl_layoutcontrol_button_id numeric(10,0) NOT NULL,
  fl_layoutcontrol_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL,
  CONSTRAINT fl_layoutcontrol_button_pkey PRIMARY KEY (fl_layoutcontrol_button_id),
  CONSTRAINT fl_layoutcontrol_button_fl_layoutcontrol_fkey FOREIGN KEY (fl_layoutcontrol_id)
  	REFERENCES adempiere.fl_layoutcontrol(fl_layoutcontrol_id)
);

alter table adempiere.fl_layoutcontrol_button
add AD_Image_ID numeric(10,0) ;


alter table adempiere.fl_layoutcontrol_button
add CONSTRAINT fl_layoutcontrol_button_ad_image_fkey  FOREIGN KEY (AD_Image_ID)
	REFERENCES adempiere.ad_image(AD_Image_ID);

alter table adempiere.fl_layoutcontrol_button
add forecolor numeric(10,0) ;


alter table adempiere.fl_layoutcontrol_button
add backcolor numeric(10,0) ;

alter table adempiere.fl_layoutcontrol_button
add fl_process_group_id numeric(10,0) ;

alter table adempiere.fl_layoutcontrol_button
add fl_process_id numeric(10,0) ;

alter table adempiere.fl_layoutcontrol_button
add CONSTRAINT fl_layoutcontrol_button_fl_process_fkey  FOREIGN KEY (fl_process_id)
	REFERENCES adempiere.fl_process(fl_process_id);

alter table adempiere.fl_layoutcontrol_button
add CONSTRAINT fl_layoutcontrol_button_fl_process_group_fkey  FOREIGN KEY (fl_process_group_id)
	REFERENCES adempiere.fl_process_group(fl_process_group_id);

alter table adempiere.fl_process_group_link
add line numeric(10, 0);

alter table adempiere.fl_layoutcontrol_button
add line numeric(10, 0);

insert into adempiere.fl_control
(fl_control_id, ad_client_id, ad_org_id, createdby,  updatedby, name)
values (110, 0, 0, 0, 0, 'Receipt');

alter table adempiere.fl_terminal
add M_PriceList_ID  numeric(10,0);

alter table adempiere.fl_terminal
ADD  CONSTRAINT fl_terminal_M_PriceList_fkey FOREIGN KEY (M_PriceList_ID)
  	REFERENCES adempiere.M_PriceList(M_PriceList_ID);

alter table adempiere.fl_terminal
add M_Warehouse_ID  numeric(10,0);

alter table adempiere.fl_terminal
ADD  CONSTRAINT fl_terminal_M_Warehouse_fkey FOREIGN KEY (M_Warehouse_ID)
  	REFERENCES adempiere.M_Warehouse(M_Warehouse_ID);
  	
alter table adempiere.fl_terminal
add C_DocType_ID  numeric(10,0);

alter table adempiere.fl_terminal
ADD  CONSTRAINT fl_terminal_C_DocType_fkey FOREIGN KEY (C_DocType_ID)
  	REFERENCES adempiere.C_DocType(C_DocType_ID);
  	

alter table fl_ticket
add documentno character varying(30) ;

alter table fl_ticket
add dateordered timestamp without time zone;


alter table adempiere.fl_terminal
add salesrep_id numeric(10,0) ;

alter table adempiere.fl_terminal
add CONSTRAINT salesrep_fl_terminal FOREIGN KEY (salesrep_id)
      REFERENCES adempiere.ad_user (ad_user_id);
      
alter table adempiere.fl_terminal
add C_CashBook_ID numeric(10,0)  ;    

alter table adempiere.fl_terminal
add CONSTRAINT fl_terminal_c_cashbook FOREIGN KEY (C_CashBook_ID)
      REFERENCES adempiere.C_CashBook (C_CashBook_ID);
      
alter table adempiere.fl_terminal
add C_BankAccount_ID numeric(10,0);

alter table adempiere.fl_terminal
add CONSTRAINT fl_terminal_C_BankAccount FOREIGN KEY (C_BankAccount_ID)
      REFERENCES adempiere.C_BankAccount (C_BankAccount_ID);
      
alter table adempiere.fl_terminal
add ismodifyprice character(1) NOT NULL DEFAULT 'N'::bpchar;

CREATE TABLE adempiere.fl_payment_setup (
  fl_payment_setup_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,
  name character varying(60) NOT NULL,
  C_Currency_ID numeric(10,0) NOT NULL,	
  CONSTRAINT fl_payment_setup_pkey PRIMARY KEY (fl_payment_setup_id),
  CONSTRAINT fl_payment_setup_c_currency_fkey FOREIGN KEY (C_Currency_ID)
	REFERENCES adempiere.C_Currency(C_Currency_ID)
);

CREATE TABLE adempiere.fl_banknote (
  fl_banknote_id numeric(10,0) NOT NULL,
  fl_payment_setup_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,

  name character varying(60) NOT NULL,
  moneyvalue numeric NOT NULL,
  AD_Image_ID numeric(10,0),
  line numeric(10, 0) NOT NULL,
  
  CONSTRAINT fl_banknote_pkey PRIMARY KEY (fl_banknote_id),
  CONSTRAINT fl_banknote_fl_payment_setup_fkey FOREIGN KEY (fl_payment_setup_id)
	REFERENCES adempiere.fl_payment_setup(fl_payment_setup_id),
  CONSTRAINT fl_banknotes_AD_Image_fkey FOREIGN KEY (AD_Image_ID)
	REFERENCES adempiere.AD_Image(AD_Image_ID) 

);

CREATE TABLE adempiere.fl_coin (
  fl_coin_id numeric(10,0) NOT NULL,
  fl_payment_setup_id numeric(10,0) NOT NULL,
  ad_client_id numeric(10,0) NOT NULL,
  ad_org_id numeric(10,0) NOT NULL,
  isactive character(1) NOT NULL DEFAULT 'Y'::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  createdby numeric(10,0) NOT NULL,
  updated timestamp without time zone NOT NULL DEFAULT now(),
  updatedby numeric(10,0) NOT NULL,

  name character varying(60) NOT NULL,
  moneyvalue numeric NOT NULL,
  AD_Image_ID numeric(10,0),
  line numeric(10, 0) NOT NULL,
  
  CONSTRAINT fl_coin_pkey PRIMARY KEY (fl_coin_id),
  CONSTRAINT ffl_coin_fl_payment_setup_fkey FOREIGN KEY (fl_payment_setup_id)
	REFERENCES adempiere.fl_payment_setup(fl_payment_setup_id),
  CONSTRAINT fl_coin_AD_Image_fkey FOREIGN KEY (AD_Image_ID)
	REFERENCES adempiere.AD_Image(AD_Image_ID) 

);

delete from adempiere.fl_control
where fl_control_id=104;

alter table adempiere.fl_layoutcontrol_button
drop column forecolor;
alter table adempiere.fl_layoutcontrol_button
drop column backcolor;
alter table adempiere.fl_layoutcontrol_button
drop column AD_Image_ID;

alter table adempiere.M_Product
add column buttoninfo bytea;

alter table adempiere.M_Product_Category
add column buttoninfo bytea;

alter table adempiere.M_Product_Class
add column buttoninfo bytea;

alter table adempiere.M_Product_Classification
add column buttoninfo bytea;

alter table adempiere.M_Product_Group
add column buttoninfo bytea;

alter table adempiere.fl_layoutcontrol_button
add column buttoninfo bytea;

alter table adempiere.M_Product
add runbuttoninfo character varying(60) ;

alter table adempiere.M_Product_Category
add runbuttoninfo character varying(60) ;

alter table adempiere.M_Product_Class
add runbuttoninfo character varying(60) ;

alter table adempiere.M_Product_Classification
add runbuttoninfo character varying(60) ;

alter table adempiere.M_Product_Group
add runbuttoninfo character varying(60) ;

alter table adempiere.fl_layoutcontrol_button
add runbuttoninfo character varying(60) ;

alter table adempiere.AD_Process
add iscontrolsetup character(1) NOT NULL DEFAULT 'N'::bpchar;

ALTER TABLE adempiere.fl_payment_setup 
ADD CONSTRAINT un_fl_payment_setup_C_Currency_ID UNIQUE (C_Currency_ID);

--irene last
