insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	 0, 
	 0, 
	 0, 
	 0,
	'Product_Category_Btn_Group', 
	'Product Category Button Group',
	'Product Category Button Group', 
	'7', 
	'FLY',
	'Y'
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Width',
	'Button Width', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Category_Btn_Group'), 
	10, 
	11, 
	'Button_Width', 
	4, 
	'Y', 
	'100'
	
);	



insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Height',
	'Button Height', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Category_Btn_Group'), 
	20, 
	11, 
	'Button_Height', 
	4, 
	'Y', 
	'80'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Product Category',
	'Product Category', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Category_Btn_Group'), 
	30, 
	18, 
	'M_Product_Category_ID', 
	4, 
	'Y'
	
);	


insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Products_Btn_Group', 
	'Products Button Group',
	'Products Button Group', 
	'7', 
	'FLY',
	'Y'
);


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1,  
	0, 
	0, 
	0, 
	0,
	'Button Width',
	'Button Width', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Products_Btn_Group'), 
	10, 
	11, 
	'Button_Width', 
	4, 
	'Y', 
	'80'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Height',
	'Button Height', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Products_Btn_Group'), 
	20, 
	11, 
	'Button_Height', 
	4, 
	'Y', 
	'80'
	
);


insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0,
	0,
	0,
	'Product_Classification_Btn_Group', 
	'Product Classification Button Group',
	'Product Classification Button Group', 
	'7', 
	'FLY',
	'Y'
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Width',
	'Button Width', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Classification_Btn_Group'), 
	10, 
	11, 
	'Button_Width', 
	4, 
	'Y', 
	'100'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Height',
	'Button Height', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Classification_Btn_Group'), 
	20, 
	11, 
	'Button_Height', 
	4, 
	'Y', 
	'80'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Product Classification',
	'Product Classification', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Classification_Btn_Group'), 
	30, 
	18, 
	'M_Product_Classification_ID', 
	4, 
	'Y'
	
);	


insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Product_Class_Btn_Group', 
	'Product Class Button Group',
	'Product Class Button Group', 
	'7', 
	'FLY',
	'Y'
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Width',
	'Button Width', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Class_Btn_Group'), 
	10, 
	11, 
	'Button_Width', 
	4, 
	'Y', 
	'100'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Height',
	'Button Height', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Class_Btn_Group'), 
	20, 
	11, 
	'Button_Height', 
	4, 
	'Y', 
	'80'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Product Class',
	'Product Class', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Class_Btn_Group'), 
	30, 
	18, 
	'M_Product_Class_ID', 
	4, 
	'Y'
	
);	

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Product_Group_Btn_Group', 
	'Product Group Button Group',
	'Product Group Button Group', 
	'7', 
	'FLY',
	'Y'
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Width',
	'Button Width', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Group_Btn_Group'), 
	10, 
	11, 
	'Button_Width', 
	4, 
	'Y', 
	'100'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Height',
	'Button Height', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Group_Btn_Group'), 
	20, 
	11, 
	'Button_Height', 
	4, 
	'Y', 
	'80'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Product Group',
	'Product Group', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Product_Group_Btn_Group'), 
	30, 
	18, 
	'M_Product_Group_ID', 
	4, 
	'Y'
	
);	

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype, 
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Free_Buttons', 
	'Free Buttons',
	'Free Buttons', 
	'7', 
	'FLY', 
	'Y'
); 

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Width',
	'Button Width', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Free_Buttons'), 
	10, 
	11, 
	'Button_Width', 
	4, 
	'Y', 
	'100'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Button Height',
	'Button Height', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Free_Buttons'), 
	20, 
	11, 
	'Button_Height', 
	4, 
	'Y', 
	'80'
	
);	

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype, 
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Create_New_Ticket', 
	'Create New Ticket',
	'Create New Ticket', 
	'7', 
	'FLY', 
	'Y'
); 


update adempiere.Ad_Process
set ClassName='org.compiere.process.NewTicketProcess' 
where Ad_Process_ID=(SELECT AD_Process_ID from adempiere.AD_Process where value='Create_New_Ticket');

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype, 
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Add_Ticket_Line', 
	'Add Ticket Line',
	'Add Ticket Line', 
	'7', 
	'U', 
	'Y'
);

update adempiere.Ad_Process
set ClassName='org.compiere.process.AddTicketLine' 
where Ad_Process_ID=(SELECT AD_Process_ID from adempiere.AD_Process where value='Add_Ticket_Line');

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Receipt_Lines', 
	'Receipt Lines',
	'Receipt Lines', 
	'7', 
	'FLY',
	'Y'
);


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Name',
	'Show Name', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Lines'), 
	10, 
	20, 
	'Name', 
	4, 
	'Y', 
	'Y'
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Quantity',
	'Show Quantity', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Lines'), 
	20, 
	20, 
	'Quantity', 
	4, 
	'Y', 
	'Y'
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Unit Of Measure',
	'Show Unit Of Measure', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Lines'), 
	30, 
	20, 
	'UOM', 
	4, 
	'Y', 
	'Y'
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Price Actual',
	'Show Price Actual', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Lines'), 
	40, 
	20, 
	'PriceActual', 
	4, 
	'Y', 
	'Y'
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Line Net Amount',
	'Show Line Net Amount', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Lines'), 
	50, 
	20, 
	'LineNetAmt', 
	4, 
	'Y', 
	'Y'
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Tax Indicator',
	'Show Tax Indicator', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Lines'), 
	60, 
	20, 
	'C_Tax_ID', 
	4, 
	'Y', 
	'Y'
	
);	


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Product',
	'Product ', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Add_Ticket_Line'), 
	10, 
	18, 
	'M_Product_ID', 4, 
	'Y'
	
);
update adempiere.Ad_Process_Para
set AD_Reference_ID=30 where Ad_Process_Para_ID=(SELECT AD_Process_Para_ID from adempiere.AD_Process_Para WHERE ColumnName='M_Product_ID' AND Name='Product'
AND AD_Process_ID=(SELECT AD_Process_ID from adempiere.AD_Process WHERE value='Add_Ticket_Line'));

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Add_Ticket_Product', 
	'Add Ticket Product',
	'Add Ticket Product', 
	'7', 
	'U'
);
update adempiere.Ad_Process
set ClassName='org.fly.swing.actions.AddProductAction' 
where Ad_Process_ID=(SELECT AD_Process_ID from adempiere.AD_Process where value='Add_Ticket_Product');;

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Find_Ticket', 
	'Find Ticket',
	'Find Ticket', 
	'7', 
	'FLY',
	'Y'
);

update adempiere.Ad_Process
set ClassName='org.fly.swing.actions.SelectTicketAction' 
where Ad_Process_ID=(SELECT AD_Process_ID from adempiere.AD_Process where value='Find_Ticket');

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Change_Ticket_Customer', 
	'Change Ticket Customer',
	'Change Ticket Customer', 
	'7', 
	'FLY',
	'Y'
);

update adempiere.Ad_Process
set ClassName='org.fly.swing.actions.ChangeTicketCustomerAction' 
where Ad_Process_ID=(SELECT AD_Process_ID from adempiere.AD_Process where value='Change_Ticket_Customer');

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isAction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Receipt_Infos', 
	'Receipt Infos',
	'Receipt Infos', 
	'7', 
	'U',
	'Y'
);

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Document No',
	'Show Document No', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Infos'), 
	10, 
	20, 
	'DocumentNo', 
	4, 
	'Y', 
	true
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Business Partner',
	'Show Business Partner', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Infos'), 
	20, 
	20, 
	'BPartner', 
	4, 
	'Y', 
	true
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show SubTotal',
	'Show SubTotal', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Infos'), 
	30, 
	20, 
	'SubTotal', 
	4, 
	'Y', 
	true
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Tax Amount',
	'Show Tax Amount', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Infos'), 
	40, 
	20, 
	'TaxAmt', 
	4, 
	'Y', 
	true
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Grand Total',
	'Show Grand Total', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Infos'), 
	50, 
	20, 
	'GrandTotal', 
	4, 
	'Y', 
	true
	
);	

update adempiere.Ad_Process
set iscontrolsetup='Y' where value='Product_Category_Btn_Group';

update adempiere.Ad_Process
set iscontrolsetup='Y' where value='Products_Btn_Group';

update adempiere.Ad_Process
set iscontrolsetup='Y' where value='Product_Classification_Btn_Group';

update adempiere.Ad_Process
set iscontrolsetup='Y' where value='Product_Class_Btn_Group';

update adempiere.Ad_Process
set iscontrolsetup='Y' where value='Product_Group_Btn_Group';

update adempiere.Ad_Process
set iscontrolsetup='Y' where value='Free_Buttons';

update adempiere.Ad_Process
set iscontrolsetup='Y' where value='Receipt_Lines';

update adempiere.Ad_Process
set iscontrolsetup='Y' where value='Receipt_Infos';


insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Payment Total',
	'Show Payment Total', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Infos'), 
	60, 
	20, 
	'PaymentTotal', 
	4, 
	'Y', 
	true
	
);	

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Show Balance Due',
	'Show Balance Due', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Receipt_Infos'), 
	70, 
	20, 
	'BalanceDue', 
	4, 
	'Y', 
	true
	
);	

--irene last

insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Show_Changes', 
	'Show Changes',
	'Show Changes', 
	'7', 
	'FLY',
	'Y'
);

update adempiere.Ad_Process
set ClassName='org.fly.swing.actions.TicketChangesAction' 
where Ad_Process_ID=(SELECT AD_Process_ID from adempiere.AD_Process where value='Show_Changes');







insert into adempiere.Ad_Process(
	Ad_Process_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	value, 
	name, 
	description,
	accesslevel, 
	entitytype,
	isaction
	
)values(
	(select max(Ad_Process_ID) from adempiere.Ad_Process)  + 1,
	0, 
	0, 
	0, 
	0,
	'Cash_Payoff', 
	'Cash Payoff',
	'Cash Payoff', 
	'7', 
	'FLY',
	'Y'
);

update adempiere.Ad_Process
set ClassName='org.compiere.process.CashPayoffProcess' 
where Ad_Process_ID=(SELECT AD_Process_ID from adempiere.AD_Process where value='Cash_Payoff');

insert into adempiere.Ad_Process_Para(
	Ad_Process_Para_ID, 
	AD_Client_ID, 
	AD_Org_ID,  
	CreatedBy, 
	UpdatedBy, 
	name, 
	description,
	entitytype, 
	Ad_Process_ID, 
	seqno, 
	AD_Reference_ID, 
	columnname, 
	fieldlength,
	ismandatory, 
	defaultvalue
	
)values(
	(select max(Ad_Process_Para_ID) from adempiere.Ad_Process_Para)  + 1, 
	0, 
	0, 
	0, 
	0,
	'Do_Cash_Payoff',
	'Do Cash Payoff', 
	'U', 
	(SELECT AD_Process_ID from adempiere.AD_Process where value='Show_Changes'), 
	10, 
	20, 
	'DoCashPayoff', 
	4, 
	'Y', 
	true
	
);	

