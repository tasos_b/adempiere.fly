-- View: adempiere.fl_ticket_linetax_v

-- DROP VIEW adempiere.fl_ticket_linetax_v;
set search_path to adempiere;

CREATE OR REPLACE VIEW adempiere.fl_ticket_linetax_v AS 
 SELECT ol.ad_client_id, --1 
    ol.ad_org_id,--2
    ol.isactive,--3
    ol.created,--4
    ol.createdby,--5
    ol.updated,--6
    ol.updatedby,--7
    'en_US'::text AS ad_language, --8
    ol.c_order_id,--9
    ol.c_orderline_id,--10
    ol.c_tax_id,--11
    t.taxindicator,--12
    ol.c_bpartner_id,--13
    ol.c_bpartner_location_id,--14
    bp.name AS bpname,--15
    bpl.c_location_id,--16
    ol.line,--17
    p.m_product_id,--18
    po.vendorproductno,--19
        CASE
            WHEN ol.qtyordered <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.qtyordered
            ELSE NULL::numeric
        END AS qtyordered,--20
        CASE
            WHEN ol.qtyentered <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.qtyentered
            ELSE NULL::numeric
        END AS qtyentered,--21
        CASE
            WHEN ol.qtyentered <> 0::numeric OR ol.m_product_id IS NOT NULL THEN uom.uomsymbol
            ELSE NULL::character varying
        END AS uomsymbol,--22
    COALESCE(c.name, (p.name::text || adempiere.productattribute(ol.m_attributesetinstance_id)::text)::character varying, ol.description) AS name,--23
        CASE
            WHEN COALESCE(c.name, p.name) IS NOT NULL THEN ol.description
            ELSE NULL::character varying
        END AS description,--24
    p.documentnote,--25
    p.upc,--26
    p.sku,--27
    COALESCE(pp.vendorproductno, p.value) AS productvalue,--28
    ra.description AS resourcedescription,--29
        CASE
            WHEN i.isdiscountprinted = 'Y'::bpchar AND ol.pricelist <> 0::numeric THEN ol.pricelist
            ELSE NULL::numeric
        END AS pricelist,--30
        CASE
            WHEN i.isdiscountprinted = 'Y'::bpchar AND ol.pricelist <> 0::numeric AND ol.qtyentered <> 0::numeric THEN ol.pricelist * ol.qtyordered / ol.qtyentered
            ELSE NULL::numeric
        END AS priceenteredlist,--31
        CASE
            WHEN i.isdiscountprinted = 'Y'::bpchar AND ol.pricelist > ol.priceactual AND ol.pricelist <> 0::numeric THEN (ol.pricelist - ol.priceactual) / ol.pricelist * 100::numeric
            ELSE NULL::numeric
        END AS discount,--32
        CASE
            WHEN ol.priceactual <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.priceactual
            ELSE NULL::numeric
        END AS priceactual,--33
        CASE
            WHEN ol.priceentered <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.priceentered
            ELSE NULL::numeric
        END AS priceentered,--34
        CASE
            WHEN ol.linenetamt <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.linenetamt
            ELSE NULL::numeric
        END AS linenetamt,--35
    p.description AS productdescription,--36
    ol.c_campaign_id,--37
    ol.c_project_id,--38
    ol.c_activity_id,--39
    ol.c_projectphase_id,--40
    ol.c_projecttask_id --41
   FROM adempiere.c_orderline ol
     JOIN adempiere.c_uom uom ON ol.c_uom_id = uom.c_uom_id
     JOIN adempiere.c_order i ON ol.c_order_id = i.c_order_id
     LEFT JOIN adempiere.m_product p ON ol.m_product_id = p.m_product_id
     LEFT JOIN adempiere.m_product_po po ON p.m_product_id = po.m_product_id AND i.c_bpartner_id = po.c_bpartner_id
     LEFT JOIN adempiere.s_resourceassignment ra ON ol.s_resourceassignment_id = ra.s_resourceassignment_id
     LEFT JOIN adempiere.c_charge c ON ol.c_charge_id = c.c_charge_id
     LEFT JOIN adempiere.c_bpartner_product pp ON ol.m_product_id = pp.m_product_id AND i.c_bpartner_id = pp.c_bpartner_id
     JOIN adempiere.c_bpartner bp ON ol.c_bpartner_id = bp.c_bpartner_id
     JOIN adempiere.c_bpartner_location bpl ON ol.c_bpartner_location_id = bpl.c_bpartner_location_id
     LEFT JOIN adempiere.c_tax t ON ol.c_tax_id = t.c_tax_id
UNION
 SELECT ol.ad_client_id,--1
    ol.ad_org_id,--2
    ol.isactive,--3
    ol.created,--4
    ol.createdby,--5
    ol.updated,--6
    ol.updatedby,--7
    'en_US'::text AS ad_language,--8
    ol.c_order_id,--9
    ol.c_orderline_id,--10
    ol.c_tax_id,--11
    NULL::character varying AS taxindicator,--12
    NULL::numeric AS c_bpartner_id,--13
    NULL::numeric AS c_bpartner_location_id,--14
    NULL::character varying AS bpname,--15
    NULL::numeric AS c_location_id,--16
    ol.line + bl.line / 100::numeric AS line,--17
    p.m_product_id,--18
    po.vendorproductno,--19
        CASE
            WHEN bl.isqtypercentage = 'N'::bpchar THEN ol.qtyordered * bl.qtybom
            ELSE ol.qtyordered * (bl.qtybatch / 100::numeric)
        END AS qtyordered, --20
        CASE
            WHEN bl.isqtypercentage = 'N'::bpchar THEN ol.qtyentered * bl.qtybom
            ELSE ol.qtyentered * (bl.qtybatch / 100::numeric)
        END AS qtyentered,--21
    uom.uomsymbol,--22
    p.name,--23
    bl.description,--24
    p.documentnote,--25
    p.upc,--26
    p.sku,--27
    p.value AS productvalue,--28
    NULL::character varying AS resourcedescription, --29
    NULL::numeric AS pricelist,--30
    NULL::numeric AS priceenteredlist,--31
    NULL::numeric AS discount,--32
    NULL::numeric AS priceactual,--33
    NULL::numeric AS priceentered,--34
    NULL::numeric AS linenetamt,--35
    p.description AS productdescription,--36
    ol.c_campaign_id,--37
    ol.c_project_id,--38
    ol.c_activity_id,--39
    ol.c_projectphase_id,--40
    ol.c_projecttask_id--41
   FROM adempiere.pp_product_bom b
     JOIN adempiere.c_orderline ol ON b.m_product_id = ol.m_product_id
     JOIN adempiere.c_order i ON ol.c_order_id = i.c_order_id
     JOIN adempiere.m_product bp ON bp.m_product_id = ol.m_product_id AND bp.isbom = 'Y'::bpchar AND bp.isverified = 'Y'::bpchar AND bp.isinvoiceprintdetails = 'Y'::bpchar
     JOIN adempiere.pp_product_bomline bl ON bl.pp_product_bom_id = b.pp_product_bom_id
     JOIN adempiere.m_product p ON p.m_product_id = bl.m_product_id
     LEFT JOIN adempiere.m_product_po po ON p.m_product_id = po.m_product_id AND i.c_bpartner_id = po.c_bpartner_id
     JOIN adempiere.c_uom uom ON p.c_uom_id = uom.c_uom_id
UNION
 SELECT c_order.ad_client_id,--1
    c_order.ad_org_id,--2
    c_order.isactive,--3
    c_order.created,--4
    c_order.createdby,--5
    c_order.updated,--6
    c_order.updatedby,--7
    'en_US'::text AS ad_language,--8
    c_order.c_order_id,--9
    NULL::numeric AS c_orderline_id,--10
    NULL::numeric AS c_tax_id,--11
    NULL::character varying AS taxindicator,--12
    NULL::numeric AS c_bpartner_id,--13
    NULL::numeric AS c_bpartner_location_id,--14
    NULL::character varying AS bpname,--15
    NULL::numeric AS c_location_id,--16
    NULL::numeric AS line,--17
    NULL::numeric AS m_product_id,--18
    NULL::character varying AS vendorproductno,--19
    NULL::numeric AS qtyordered,--20
    NULL::numeric AS qtyentered,--21
    NULL::character varying AS uomsymbol,--22
    NULL::character varying AS name,--23
    NULL::character varying AS description,--24
    NULL::character varying AS documentnote,--25
    NULL::character varying AS upc,--26
    NULL::character varying AS sku,--27
    NULL::character varying AS productvalue,--28
    NULL::character varying AS resourcedescription,--29
    NULL::numeric AS pricelist,--30
    NULL::numeric AS priceenteredlist,--31
    NULL::numeric AS discount,--32
    NULL::numeric AS priceactual,--33
    NULL::numeric AS priceentered,--34
    NULL::numeric AS linenetamt,--35
    NULL::character varying AS productdescription,--35
    NULL::numeric AS c_campaign_id,
    NULL::numeric AS c_project_id,
    NULL::numeric AS c_activity_id,
    NULL::numeric AS c_projectphase_id,
    NULL::numeric AS c_projecttask_id
   FROM adempiere.c_order
UNION
 SELECT ot.ad_client_id,
    ot.ad_org_id,
    ot.isactive,
    ot.created,
    ot.createdby,
    ot.updated,
    ot.updatedby,
    'en_US'::text AS ad_language,
    ot.c_order_id,
    NULL::numeric AS c_orderline_id,
    ot.c_tax_id,
    t.taxindicator,
    NULL::numeric AS c_bpartner_id,
    NULL::numeric AS c_bpartner_location_id,
    NULL::character varying AS bpname,
    NULL::numeric AS c_location_id,
    NULL::numeric AS line,
    NULL::numeric AS m_product_id,
    NULL::character varying AS vendorproductno,
    NULL::numeric AS qtyordered,
    NULL::numeric AS qtyentered,
    NULL::character varying AS uomsymbol,
    t.name,
    NULL::character varying AS description,
    NULL::character varying AS documentnote,
    NULL::character varying AS upc,
    NULL::character varying AS sku,
    NULL::character varying AS productvalue,
    NULL::character varying AS resourcedescription,
    NULL::numeric AS pricelist,
    NULL::numeric AS priceenteredlist,
    NULL::numeric AS discount,
        CASE
            WHEN ot.istaxincluded = 'Y'::bpchar THEN ot.taxamt
            ELSE ot.taxbaseamt
        END AS priceactual,
        CASE
            WHEN ot.istaxincluded = 'Y'::bpchar THEN ot.taxamt
            ELSE ot.taxbaseamt
        END AS priceentered,
        CASE
            WHEN ot.istaxincluded = 'Y'::bpchar THEN NULL::numeric
            ELSE ot.taxamt
        END AS linenetamt,
    NULL::character varying AS productdescription,
    NULL::numeric AS c_campaign_id,
    NULL::numeric AS c_project_id,
    NULL::numeric AS c_activity_id,
    NULL::numeric AS c_projectphase_id,
    NULL::numeric AS c_projecttask_id
   FROM adempiere.c_ordertax ot
     JOIN adempiere.c_tax t ON ot.c_tax_id = t.c_tax_id;

ALTER TABLE adempiere.c_order_linetax_v
  OWNER TO adempiere;
  
  
CREATE OR REPLACE VIEW adempiere.fl_ticket_line_v AS 
 SELECT ol.ad_client_id,
    ol.ad_org_id,
    ol.isactive,
    ol.created,
    ol.createdby,
    ol.updated,
    ol.updatedby,
    'en_US'::text AS ad_language,
    ol.c_order_id,
    ol.c_orderline_id,
    ol.c_tax_id,
    t.taxindicator,
    ol.c_bpartner_id,
    ol.c_bpartner_location_id,
    bp.name AS bpname,
    bpl.c_location_id,
    ol.line,
    p.m_product_id,
    po.vendorproductno,
        CASE
            WHEN ol.qtyordered <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.qtyordered
            ELSE NULL::numeric
        END AS qtyordered,
        CASE
            WHEN ol.qtyentered <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.qtyentered
            ELSE NULL::numeric
        END AS qtyentered,
        CASE
            WHEN ol.qtyentered <> 0::numeric OR ol.m_product_id IS NOT NULL THEN uom.uomsymbol
            ELSE NULL::character varying
        END AS uomsymbol,
    COALESCE(c.name, (p.name::text || adempiere.productattribute(ol.m_attributesetinstance_id)::text)::character varying, ol.description) AS name,
        CASE
            WHEN COALESCE(c.name, p.name) IS NOT NULL THEN ol.description
            ELSE NULL::character varying
        END AS description,
    p.documentnote,
    p.upc,
    p.sku,
    COALESCE(pp.vendorproductno, p.value) AS productvalue,
    ra.description AS resourcedescription,
        CASE
            WHEN i.isdiscountprinted = 'Y'::bpchar AND ol.pricelist <> 0::numeric THEN ol.pricelist
            ELSE NULL::numeric
        END AS pricelist,
        CASE
            WHEN i.isdiscountprinted = 'Y'::bpchar AND ol.pricelist <> 0::numeric AND ol.qtyentered <> 0::numeric THEN ol.pricelist * ol.qtyordered / ol.qtyentered
            ELSE NULL::numeric
        END AS priceenteredlist,
        CASE
            WHEN i.isdiscountprinted = 'Y'::bpchar AND ol.pricelist > ol.priceactual AND ol.pricelist <> 0::numeric THEN (ol.pricelist - ol.priceactual) / ol.pricelist * 100::numeric
            ELSE NULL::numeric
        END AS discount,
        CASE
            WHEN ol.priceactual <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.priceactual
            ELSE NULL::numeric
        END AS priceactual,
        CASE
            WHEN ol.priceentered <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.priceentered
            ELSE NULL::numeric
        END AS priceentered,
        CASE
            WHEN ol.linenetamt <> 0::numeric OR ol.m_product_id IS NOT NULL THEN ol.linenetamt
            ELSE NULL::numeric
        END AS linenetamt,
    p.description AS productdescription,
    ol.c_campaign_id,
    ol.c_project_id,
    ol.c_activity_id,
    ol.c_projectphase_id,
    ol.c_projecttask_id
   FROM adempiere.c_orderline ol
     JOIN adempiere.c_uom uom ON ol.c_uom_id = uom.c_uom_id
     JOIN adempiere.c_order i ON ol.c_order_id = i.c_order_id
     LEFT JOIN adempiere.m_product p ON ol.m_product_id = p.m_product_id
     LEFT JOIN adempiere.m_product_po po ON p.m_product_id = po.m_product_id AND i.c_bpartner_id = po.c_bpartner_id
     LEFT JOIN adempiere.s_resourceassignment ra ON ol.s_resourceassignment_id = ra.s_resourceassignment_id
     LEFT JOIN adempiere.c_charge c ON ol.c_charge_id = c.c_charge_id
     LEFT JOIN adempiere.c_bpartner_product pp ON ol.m_product_id = pp.m_product_id AND i.c_bpartner_id = pp.c_bpartner_id
     JOIN adempiere.c_bpartner bp ON ol.c_bpartner_id = bp.c_bpartner_id
     JOIN adempiere.c_bpartner_location bpl ON ol.c_bpartner_location_id = bpl.c_bpartner_location_id
     LEFT JOIN adempiere.c_tax t ON ol.c_tax_id = t.c_tax_id;



CREATE OR REPLACE VIEW adempiere.fl_ticket_order_infos_v AS   
SELECT 
    tm.fl_terminal_id,
    tm.name as terminalname,
    tk.fl_ticket_id,
    co.c_order_id,
    co.c_bpartner_id,
    co.documentno,
    co.created,
    co.updated,
    co.dateordered,
    bp.name,
    CASE
       WHEN ot.istaxincluded = 'Y'::bpchar THEN ot.taxamt
       ELSE ot.taxbaseamt
    END AS amount,
    CASE
       WHEN ot.istaxincluded = 'Y'::bpchar THEN NULL::numeric
       ELSE ot.taxamt
    END AS taxamount,
	co.grandtotal
   FROM adempiere.c_order co
	left join adempiere.c_bpartner bp
	   on co.c_bpartner_id=bp.c_bpartner_id
	left join adempiere.c_ordertax ot
	    on ot.c_order_id=co.c_order_id  
	left join adempiere.fl_ticket tk
	    on tk.c_order_id=co.c_order_id  
	left join adempiere.fl_terminal tm
	    on tk.fl_terminal_id=tm.fl_terminal_id  
	        
	where  tk.fl_ticket_id is not null   ;

