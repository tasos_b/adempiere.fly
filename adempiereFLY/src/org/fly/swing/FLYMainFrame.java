/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.RepaintManager;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

import org.compiere.Adempiere;
import org.compiere.apps.AKeyboardFocusManager;
import org.compiere.swing.CFrame;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;


public class FLYMainFrame {
	private javax.swing.JButton m_ShowLeftPanelButton;
	private CPanel m_BaseLeftPanel;
	private CPanel m_ButtonPanel;
	private CPanel m_Container;
	private CPanel m_MenuPanel;
	private CPanel m_BaseRightPanel;
	private CPanel m_StatusPanel;
	private CLabel m_StatusLabel;
	
	
	private FLYStaticMenuBG m_menuButtons;

	public FLYStaticMenuBG getM_menuButtons() {
		return m_menuButtons;
	}

	private Icon menu_open;
	private Icon menu_close;
	
	CFrame m_mainFrame;
	
	public static FLYMainFrame instance;
	 
	public static void initialize(GraphicsConfiguration conf){
		instance = new FLYMainFrame();
		instance.createFrame(conf);
		
	}
	
	public void Close(){
		m_mainFrame.dispose();
	}
	
	public CFrame getFrame(){
		return m_mainFrame;
	}
	public CPanel getControlPanel(){
		return m_Container;
	}
	
	public void clearControls(){
		m_Container.removeAll();
		m_Container.repaint();
	}
	
	public void setStatus(String status){
		m_StatusLabel.setForeground(Color.black);
		m_StatusLabel.setText(status);
	}
	
	public void createFrame(GraphicsConfiguration conf){
		
		m_mainFrame = new CFrame(conf);
		// Focus Traversal
		KeyboardFocusManager
				.setCurrentKeyboardFocusManager(AKeyboardFocusManager.get());
		m_mainFrame.setIconImage(Adempiere.getImage16());

		m_mainFrame.setExtendedState(m_mainFrame.getExtendedState() | JFrame.MAXIMIZED_BOTH);

		m_mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponents();
		completeComponents();
		
	}
	
	public void Show(){
		m_mainFrame.pack();
		m_mainFrame.setVisible(true);
	}
	
	
	private void initComponents() {

		m_BaseLeftPanel = new CPanel();
		m_ButtonPanel = new CPanel();
		m_ShowLeftPanelButton = new javax.swing.JButton();
		m_MenuPanel = new CPanel();
		m_BaseRightPanel = new CPanel();
		m_Container = new CPanel();
		
		m_StatusPanel = new CPanel();
		m_StatusLabel = new CLabel();
		
		m_mainFrame.getContentPane().setLayout(new java.awt.BorderLayout());

		m_BaseLeftPanel.setLayout(new java.awt.BorderLayout());

		m_ButtonPanel.setLayout(new javax.swing.BoxLayout(m_ButtonPanel,
				javax.swing.BoxLayout.Y_AXIS));

		m_ShowLeftPanelButton.setFocusPainted(false);
		m_ShowLeftPanelButton.setFocusable(false);
		m_ShowLeftPanelButton.setMargin(new java.awt.Insets(14, 2, 14, 5));
		m_ShowLeftPanelButton.setRequestFocusEnabled(false);
		m_ShowLeftPanelButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});
		m_ButtonPanel.add(m_ShowLeftPanelButton);

		m_BaseLeftPanel.add(m_ButtonPanel, java.awt.BorderLayout.LINE_END);
		m_BaseLeftPanel.add(m_MenuPanel, java.awt.BorderLayout.CENTER);

		m_mainFrame.getContentPane().add(m_BaseLeftPanel, java.awt.BorderLayout.LINE_START);

		m_BaseRightPanel.setLayout(new java.awt.BorderLayout());

		//m_Container.setLayout(new java.awt.CardLayout());
		m_Container.setLayout(null);
		//m_Container.setBackground(Color.RED);
		m_BaseRightPanel.add(m_Container, java.awt.BorderLayout.CENTER);

		m_mainFrame.getContentPane().add(m_BaseRightPanel, java.awt.BorderLayout.CENTER);
		m_MenuPanel.setPreferredSize(new Dimension(300, 500));

		// m_jPanelLeft.setBackground(Color.BLUE);
		//m_jPanelContainer.setBackground(Color.WHITE);

		if (m_ShowLeftPanelButton.getComponentOrientation().isLeftToRight()) {
			menu_open = new javax.swing.ImageIcon(getClass().getResource(
					"/org/fly/swing/images/menu-right.png"));
			menu_close = new javax.swing.ImageIcon(getClass().getResource(
					"/org/fly/swing/images/menu-left.png"));
		} else {
			menu_open = new javax.swing.ImageIcon(getClass().getResource(
					"/org/fly/swing/images/menu-left.png"));
			menu_close = new javax.swing.ImageIcon(getClass().getResource(
					"/org/fly/swing/images/menu-right.png"));
		}
		//statusbar
		m_StatusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		m_mainFrame.getContentPane().add(m_StatusPanel, BorderLayout.SOUTH);
		m_StatusPanel.setPreferredSize(new Dimension(m_mainFrame.getWidth(), 16));
		m_StatusPanel.setLayout(new BoxLayout(m_StatusPanel, BoxLayout.X_AXIS));
		m_StatusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		m_StatusPanel.add(m_StatusLabel);

		assignMenuButtonIcon();

	}
	
	private void completeComponents(){
		m_MenuPanel.setLayout((LayoutManager) new BoxLayout(m_MenuPanel, BoxLayout.Y_AXIS));
		m_menuButtons = new FLYStaticMenuBG();
		m_MenuPanel.add(m_menuButtons);
		
	}

	private void assignMenuButtonIcon() {
		m_ShowLeftPanelButton.setIcon(m_MenuPanel.isVisible() ? menu_close : menu_open);
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

		setMenuVisible(!m_MenuPanel.isVisible());

	}

	private void setMenuVisible(boolean value) {

		m_MenuPanel.setVisible(value);
		assignMenuButtonIcon();
		m_mainFrame.revalidate();
	}

	public void updateUI(){
		SwingUtilities.updateComponentTreeUI(m_mainFrame);
		m_mainFrame.validate();
		RepaintManager mgr = RepaintManager.currentManager(m_mainFrame);
		Component childs[] = m_mainFrame.getComponents();
		for (Component c : childs) {
			if (c instanceof JComponent)
				mgr.markCompletelyDirty((JComponent)c);
		}
		m_mainFrame.repaint();

	}

	public void setError(String status) {
		m_StatusLabel.setForeground(Color.red);
		m_StatusLabel.setText(status);
	}
	

}
