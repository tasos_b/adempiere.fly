/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.utils;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;


import org.fly.core.utils.StringUtil;

public class SwingUtil {
	public static void addImageToButton(JButton button, ImageIcon icon){
		
		BufferedImage bi =  new BufferedImage(
			    icon.getIconWidth(),
			    icon.getIconHeight(),
			    BufferedImage.TYPE_INT_ARGB); 
		Graphics g = bi.createGraphics();
		// paint the Icon to the BufferedImage.
		icon.paintIcon(null, g, 0,0);
		g.dispose();
		Image img = bi.getScaledInstance(-1, button.getHeight() / 2, Image.SCALE_DEFAULT);
		button.setIcon(new ImageIcon(img));
		button.setVerticalTextPosition(SwingConstants.BOTTOM);
		button.setHorizontalTextPosition(SwingConstants.CENTER);
	}
	
	public static void addTextToButton(JButton button, BigDecimal foreColor, 
			BigDecimal backColor, String text, String defColor, int fontSize){

		button.setVerticalTextPosition(SwingConstants.BOTTOM);
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		
		if(foreColor != null){
			if(backColor != null){
				button.setText(StringUtil.stringToHtml(text, 
						foreColor.intValue(), backColor.intValue(), fontSize));
				
			}
			else{
				button.setText(StringUtil.stringToHtml(text, 
						foreColor.intValue(), fontSize));
			}
		}
		else{
			button.setText(StringUtil.stringToHtml(text, defColor, fontSize));
		}
	}
}
