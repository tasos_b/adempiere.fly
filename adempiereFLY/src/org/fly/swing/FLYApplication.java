/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing;

import java.util.Properties;

import org.adempiere.plaf.AdempierePLAF;
import org.compiere.Adempiere;
import org.compiere.apps.AEnv;
import org.compiere.model.MSession;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Splash;
import org.fly.core.FLYEnv;
import org.fly.swing.FLYSwingController;

public class FLYApplication {

	private Properties m_ctx;
	private static CLogger		s_log = CLogger.getCLogger (FLYApplication.class);

	public FLYApplication() {
		try {
			Adempiere.startup(true); // needs to be here for UI
			AdempierePLAF.setPLAF();
			Splash splash = Splash.getSplash();
			FLYLogin login = new FLYLogin(splash);

			if (!login.initLogin()) // no automatic login
			{
				// Center the window
				try {
					AEnv.showCenterScreen(login); // HTML load errors
				} catch (Exception ex) {

				}
				if (!login.isConnected() || !login.isOKpressed())
					AEnv.exit(1);
			}

			// Check Build
			if (!DB.isBuildOK(m_ctx))
				AEnv.exit(1);

			// Check DB (AppsServer Version checked in Login)
			DB.isDatabaseOK(m_ctx);

			splash.setText(Msg.getMsg(m_ctx, "Loading"));
			splash.toFront();
			splash.paint(splash.getGraphics());

			//
			if (!Adempiere.startupEnvironment(true)) // Load Environment
				System.exit(1);
			MSession.get(Env.getCtx(), true); // Start Session

			//Get Settings to learn which terminal am i 

			//Get terminal, layouts, load cache
			FLYEnv.initialize();
			
			//Create the ui
			FLYMainFrame.initialize(splash.getGraphicsConfiguration());
			FLYSwingController.initialize();
			
			splash.dispose();
			splash = null;

		} catch (Exception ex) {
			//FLYDialog.error(ex.getMessage());
			ex.printStackTrace();
			s_log.severe(ex.getMessage());
			
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new FLYApplication();
		
	}

}
