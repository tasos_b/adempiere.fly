package org.fly.swing.actions;

import java.awt.Dimension;
import java.util.Properties;

import org.compiere.model.Mticket;
import org.compiere.process.ProcessCall;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.Trx;
import org.fly.core.FLYProcessHandler;
import org.fly.core.FLYProcessResultHandler;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.dialogs.SelectCustomerDialog;

public class ChangeTicketCustomerAction extends FLYProcessHandler implements ProcessCall{

	@Override
	public boolean startProcess(Properties ctx, ProcessInfo pi, Trx trx) {
		SelectCustomerDialog form = new SelectCustomerDialog("Change Ticket Customer", pi, ctx);
		form.setM_ctx(ctx);
		form.setHandler(this);
		form.setSize(new Dimension(800, 600));
		form.setLocationRelativeTo(FLYMainFrame.instance.getFrame());
		form.setVisible(true);
		return true;

	}

	@Override
	public void handle(Properties ctx, ProcessInfo pi, Trx trx) {
		ProcessInfoParameter pPartner = FLYProcessUtil.getParameter(pi, "C_BPartner_ID") ;
		ProcessInfoParameter pOrder = FLYProcessUtil.getParameter(pi, "C_Order_ID") ;
		if(pPartner != null && pOrder != null){
			int C_BPartner_ID = pPartner.getParameterAsInt();
			int M_Order_ID = pOrder.getParameterAsInt();
			Mticket.changeTicketCustomer(ctx, M_Order_ID,  C_BPartner_ID);
			new FLYProcessResultHandler().handle(ctx, pi, null);

		}
		
		
		
	}

}
