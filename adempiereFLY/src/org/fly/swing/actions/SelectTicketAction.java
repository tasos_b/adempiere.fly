package org.fly.swing.actions;

import java.awt.Dimension;
import java.util.Properties;

import org.compiere.process.ProcessCall;
import org.compiere.process.ProcessInfo;
import org.compiere.util.Trx;
import org.fly.core.FLYProcessHandler;
import org.fly.core.FLYProcessResultHandler;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.dialogs.SelectTicketDialog;

public class SelectTicketAction extends FLYProcessHandler implements ProcessCall{

	@Override
	public boolean startProcess(Properties ctx, ProcessInfo pi, Trx trx) {
		SelectTicketDialog form = new SelectTicketDialog("Select Ticket", pi, ctx);
		form.setM_ctx(ctx);
		form.setHandler(this);
		form.setSize(new Dimension(800, 600));
		form.setLocationRelativeTo(FLYMainFrame.instance.getFrame());
		form.setVisible(true);
		return true;
	}

	@Override
	public void handle(Properties ctx, ProcessInfo pi, Trx trx) {
		new FLYProcessResultHandler().handle(ctx,  pi, null);

		
	}

}
