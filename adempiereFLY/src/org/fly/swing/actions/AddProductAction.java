package org.fly.swing.actions;

import java.awt.Dimension;
import java.util.Properties;

import org.compiere.process.AddTicketLine;
import org.compiere.process.ProcessCall;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.Trx;
import org.fly.core.FLYProcessHandler;
import org.fly.core.FLYProcessResultHandler;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.dialogs.SelectProductDialog;


public class AddProductAction extends FLYProcessHandler implements ProcessCall{

	@Override
	public boolean startProcess(Properties ctx, ProcessInfo pi, Trx trx) {
		SelectProductDialog form = new SelectProductDialog("Add Product", pi, ctx);
		form.setM_ctx(ctx);
		form.setHandler(this);
		form.setSize(new Dimension(800, 600));
		form.setLocationRelativeTo(FLYMainFrame.instance.getFrame());
		form.setVisible(true);
		return true;

	}


	@Override
	public void handle(Properties ctx, ProcessInfo pi, Trx trx) {
		ProcessInfoParameter param = FLYProcessUtil.getParameter(pi, "M_Product_ID") ;
		if(param != null){
			AddTicketLine tl = new AddTicketLine();
			tl.startProcess(ctx, pi, null);
			new FLYProcessResultHandler().handle(ctx, pi, null);

		}
	}

}
