package org.fly.swing.actions;

import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.Properties;

import org.compiere.model.Mticket;
import org.compiere.process.ProcessCall;
import org.compiere.process.ProcessInfo;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.fly.core.FLYProcessHandler;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.swing.dialogs.PaymentChangesDialog;

public class TicketChangesAction extends FLYProcessHandler implements ProcessCall{
	@Override
	public boolean startProcess(Properties ctx, ProcessInfo pi, Trx trx) {
		int C_Order_ID = pi.getParameterAsInt("C_Order_ID");
		BigDecimal balanceDue = Mticket.getBalanceDue(C_Order_ID);
		FLYProcessUtil.setParameter(pi, "Total", balanceDue);
		
		
		PaymentChangesDialog form = new PaymentChangesDialog("Changes", pi, Env.getCtx());
		form.setM_ctx(Env.getCtx());
		form.setSize(new Dimension(1000, 700));
		form.setLocationRelativeTo(/*FLYMainFrame.instance.getFrame()*/null);
		form.setVisible(true);
		return true;

	}
	@Override
	public void handle(Properties ctx, ProcessInfo pi, Trx trx) {
	}
}
