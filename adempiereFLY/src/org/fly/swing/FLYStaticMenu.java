/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import org.compiere.apps.FLYPreference;
import org.compiere.process.ProcessInfo;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.fly.core.utils.StringUtil;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.dialogs.MultiPaymentDialog;




public class FLYStaticMenu {

	private Icon m_close_ico;
	private Icon m_error_ico;
	private Icon m_theme_ico;
	private Icon m_save_layout_ico;
	private Icon m_lock_layout_ico;
	
	int m_MenuFontSize = 10;

	
	public FLYStaticMenu() {
		
	}
	
	public void CreateExitButton(){
		JButton exitButton = FLYMainFrame.instance.getM_menuButtons()
				.getButtons().get(FLYStaticMenuBG.B_EXIT);
		exitButton.setText(StringUtil.stringToHtml(Msg.getMsg(Env.getCtx(), "fl.exit", false), "blue", m_MenuFontSize));
		exitButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				FLYMainFrame.instance.Close();
			}
		});
		m_close_ico = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/Logout24.png"));
		exitButton.setIcon(m_close_ico);
		exitButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		exitButton.setHorizontalTextPosition(SwingConstants.CENTER);
	}

	public void CreateErrorButton(){
		JButton preferenceButton = FLYMainFrame.instance.getM_menuButtons()
				.getButtons().get(FLYStaticMenuBG.B_PREFERENCES);
		preferenceButton.setText(StringUtil.stringToHtml(Msg.getMsg(Env.getCtx(), "fl.error.log", false), "red", "yellow", m_MenuFontSize));
		preferenceButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				FLYPreference errors = new FLYPreference (FLYMainFrame.instance.getFrame(), 0, FLYPreference.MODE_ERROR);
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				errors.setSize(screenSize.width, screenSize.height);
				errors.setLocation(new Point(0, 0));
				errors.setVisible(true);
			}
		
		});
		m_error_ico = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/End24.gif"));
		preferenceButton.setIcon(m_error_ico);
		preferenceButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		preferenceButton.setHorizontalTextPosition(SwingConstants.CENTER);

	}	
	
	
	public void CreateThemeButton(){
		JButton preferenceButton = FLYMainFrame.instance.getM_menuButtons()
				.getButtons().get(FLYStaticMenuBG.B_THEMES);
		preferenceButton.setText(StringUtil.stringToHtml(Msg.getMsg(Env.getCtx(), "fl.themes", false), "blue", m_MenuFontSize));
		preferenceButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				FLYPreference errors = new FLYPreference (FLYMainFrame.instance.getFrame(), 0, FLYPreference.MODE_THEME);
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				errors.setSize(screenSize.width, screenSize.height);
				errors.setLocation(new Point(0, 0));
				errors.setVisible(true);
			}
		});
		m_theme_ico = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/VPreference24.gif"));
		preferenceButton.setIcon(m_theme_ico);
		preferenceButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		preferenceButton.setHorizontalTextPosition(SwingConstants.CENTER);
	}	
	
	public void CreateSaveLayoutButton(){
		JButton saveLayoutButton = FLYMainFrame.instance.getM_menuButtons()
				.getButtons().get(FLYStaticMenuBG.B_SAVE_LAYOUT);
		saveLayoutButton.setText(StringUtil.stringToHtml(Msg.getMsg(Env.getCtx(), "fl.savelayout", false), "blue", m_MenuFontSize));
		saveLayoutButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				FLYSwingController.instance.saveControls();
			}
		
		});
		m_save_layout_ico = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/Save24.gif"));
		saveLayoutButton.setIcon(m_save_layout_ico);
		saveLayoutButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		saveLayoutButton.setHorizontalTextPosition(SwingConstants.CENTER);

	}	

	public void CreateUnlockLayoutButton(){
		JButton unlockLayoutButton = FLYMainFrame.instance.getM_menuButtons()
				.getButtons().get(FLYStaticMenuBG.B_UNLOCK_LAYOUT);
		String text = "fl.unlock.layout";
		unlockLayoutButton.setText(StringUtil.stringToHtml(Msg.getMsg(Env.getCtx(), text, false), "blue", m_MenuFontSize));
		unlockLayoutButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				//if(!FLYSwingController.instance.isM_layout_locked())
				
				FLYSwingController.instance.setM_layout_locked(!FLYSwingController.instance.isM_layout_locked());
				FLYSwingController.instance.lockControls(FLYSwingController.instance.isM_layout_locked());
				String text = "fl.unlock.layout";
				String text2 = "fl.lock.layout";
				((JButton)evt.getSource()).setText(StringUtil.stringToHtml(Msg.getMsg(Env.getCtx(), FLYSwingController.instance.isM_layout_locked() ? text : text2, false), "blue", m_MenuFontSize));
				
			}
		
		});
		m_lock_layout_ico = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/LockX24.gif"));
		unlockLayoutButton.setIcon(m_lock_layout_ico);
		unlockLayoutButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		unlockLayoutButton.setHorizontalTextPosition(SwingConstants.CENTER);

	}	
	public void CreateTestButton(){
		JButton button = FLYMainFrame.instance.getM_menuButtons()
				.getButtons().get(FLYStaticMenuBG.B_TEST);
		button.setText(StringUtil.stringToHtml("Test", "blue", m_MenuFontSize));
		button.setVerticalTextPosition(SwingConstants.BOTTOM);
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		button.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				/*
				ProcessInfo pi = new ProcessInfo("aaa", 0);
				pi.addParameter("C_Currency_ID", 102, "C_Currency_ID");
				pi.addParameter("Total", new BigDecimal(102), "Total");
				
				PaymentChangesDialog form = new PaymentChangesDialog("Test", pi, Env.getCtx());
				form.setM_ctx(Env.getCtx());
				form.setSize(new Dimension(1000, 700));
				form.setLocationRelativeTo(null);
				form.setVisible(true);
				*/
				ProcessInfo pi = new ProcessInfo("aaa", 0);

				MultiPaymentDialog form = new MultiPaymentDialog("Test", pi, Env.getCtx());
				form.setM_ctx(Env.getCtx());
				form.setSize(new Dimension(1000, 700));
				form.setLocationRelativeTo(null);
				form.setVisible(true);
			}
		});
	}


}
