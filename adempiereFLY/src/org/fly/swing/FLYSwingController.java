/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing;



import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.compiere.model.Mlayout;
import org.compiere.model.Mlayoutcontrol;
import org.fly.core.FLYEnv;
import org.fly.core.classes.FLYProductCategory;
import org.fly.core.classes.FLYProductClass;
import org.fly.core.classes.FLYProductClassification;
import org.fly.core.classes.FLYProductGroup;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.controls.FLYControl;
import org.fly.swing.controls.FLYProducts;
import org.fly.swing.controls.FLYReceipt;
import org.fly.swing.controls.FLYReceiptLines;
import org.fly.swing.controls.base.Resizable;
import org.fly.swing.controls.base.ResizableBorder;


public class FLYSwingController {
	
	private List<FLYControl> m_controls;
	
	
	
	private boolean m_layout_locked = true;
	
	public static FLYSwingController instance;
	
	
	public FLYSwingController(){
		m_controls = new ArrayList<FLYControl>();
	}
	
	public static void initialize(){
		instance = new FLYSwingController();
		instance.instanceInit();
	}
	
	private void instanceInit(){
		instance.createStaticMenus();
		FLYMainFrame.instance.getControlPanel().setLayout(null);
		
		
		Mlayout m_current_layout = FLYEnv.instance.getM_current_layout();
		if(m_current_layout == null){
			List<Mlayout> layouts = FLYEnv.instance.getLayouts();
			if( layouts != null && layouts.size() > 0){
				m_current_layout = layouts.get(0);
				FLYEnv.instance.setM_current_layout(m_current_layout);
			}
		}
		
		if(m_current_layout != null){
			instance.loadControls(m_current_layout.get_ID());
			
		}

		FLYMainFrame.instance.Show();
	}
	
	public void setStatus(String status){
		FLYMainFrame.instance.setStatus(status);
	}
	
	public void setError(String status){
		//FLYMainFrame.instance.setError(status);
		FLYDialog.error(status);
	}
	
	public boolean isM_layout_locked() {
		return m_layout_locked;
	}

	public void setM_layout_locked(boolean m_layout_locked) {
		this.m_layout_locked = m_layout_locked;
	}


	public void saveControls(){
		if(m_controls == null || m_controls.size() == 0)
			return;
		for(Iterator<FLYControl> i = m_controls.iterator(); i.hasNext(); ) {
			FLYControl item = i.next();
			if(item.getM_control() != null)
				item.save();
		}
	}
	
	public void lockControls(boolean b_lock){
		if(m_controls == null || m_controls.size() == 0)
			return;
		for(Iterator<FLYControl> i = m_controls.iterator(); i.hasNext(); ) {
			FLYControl item = i.next();
			if(item.getM_control() != null){
				Resizable res = item.getM_res();
				res.setResizable(!b_lock);
				ResizableBorder border = (ResizableBorder)res.getBorder();
				border.setBorderColor(b_lock ?  item.getM_control().getBackground() : border.getDesignColor());
				res.repaint();
			}
		}
	}

	private void createStaticMenus(){
		FLYStaticMenu staticMenu = new FLYStaticMenu();
		staticMenu.CreateExitButton();
		staticMenu.CreateErrorButton();
		staticMenu.CreateThemeButton();
		staticMenu.CreateSaveLayoutButton();
		staticMenu.CreateUnlockLayoutButton();
		staticMenu.CreateTestButton();
	}
	
	public void loadControls(int layout_ID){
		m_controls.clear();
		FLYMainFrame.instance.clearControls();
		List<Mlayoutcontrol> layoutControls = FLYEnv.instance.getControls(layout_ID);
		if(layoutControls == null)
			return;
		for(Iterator<Mlayoutcontrol> i = layoutControls.iterator(); i.hasNext(); ) {
			Mlayoutcontrol item = i.next();
			FLYControl control = new FLYControl(item);
			m_controls.add(control);
		}
		FLYMainFrame.instance.getControlPanel().revalidate();
		FLYMainFrame.instance.getControlPanel().repaint();
		lockControls(true);
	}
	

	public void categoryChanged(FLYProductCategory category){
		//find product controls and load buttons
		List<FLYControl> productControls = m_controls
				.stream()
				.filter(p -> (p.getM_layoutcontrol().getfl_control_ID() == 103)
						).collect(Collectors.toList());
		if(productControls != null && productControls.size() > 0){
			for(Iterator<FLYControl> i = productControls.iterator(); i.hasNext(); ) {
				FLYProducts item = (FLYProducts)i.next().getM_control();
				item.loadButtons(FLYControl.PRODUCT_CATEGORIES, category.getM_product_category_id());
				
				
			}
		}
	}
	public void classificationChanged(FLYProductClassification classification){
		//find product controls and load buttons
		List<FLYControl> productControls = m_controls
				.stream()
				.filter(p -> (p.getM_layoutcontrol().getfl_control_ID() == 103)
						).collect(Collectors.toList());
		if(productControls != null && productControls.size() > 0){
			for(Iterator<FLYControl> i = productControls.iterator(); i.hasNext(); ) {
				FLYProducts item = (FLYProducts)i.next().getM_control();
				item.loadButtons(FLYControl.PRODUCT_CLASSIFICATION, classification.getM_product_classification_id());
				
				
			}
		}
	}

	public void classChanged(FLYProductClass classi){
		//find product controls and load buttons
		List<FLYControl> productControls = m_controls
				.stream()
				.filter(p -> (p.getM_layoutcontrol().getfl_control_ID() == 103)
						).collect(Collectors.toList());
		if(productControls != null && productControls.size() > 0){
			for(Iterator<FLYControl> i = productControls.iterator(); i.hasNext(); ) {
				FLYProducts item = (FLYProducts)i.next().getM_control();
				item.loadButtons(FLYControl.PRODUCT_CLASS, classi.getM_product_class_id());
				
				
			}
		}
	}

	public void groupChanged(FLYProductGroup group){
		//find product controls and load buttons
		List<FLYControl> productControls = m_controls
				.stream()
				.filter(p -> (p.getM_layoutcontrol().getfl_control_ID() == 103)
						).collect(Collectors.toList());
		if(productControls != null && productControls.size() > 0){
			for(Iterator<FLYControl> i = productControls.iterator(); i.hasNext(); ) {
				FLYProducts item = (FLYProducts)i.next().getM_control();
				item.loadButtons(FLYControl.PRODUCT_GROUP, group.getM_product_group_id());
				
				
			}
		}
	}
	
	public void refreshLines(int C_Order_ID){
		List<FLYControl> receiptControls = m_controls
				.stream()
				.filter(p -> (p.getM_layoutcontrol().getfl_control_ID() == 101)
						).collect(Collectors.toList());
		if(receiptControls != null && receiptControls.size() > 0){
			for(Iterator<FLYControl> i = receiptControls.iterator(); i.hasNext(); ) {
				FLYReceiptLines item = (FLYReceiptLines)i.next().getM_control();
				item.updateTable(C_Order_ID);
				
				
			}
		}
		
	}
	public void refreshDocInfos(int C_Order_ID){
		List<FLYControl> items = m_controls
				.stream()
				.filter(p -> (p.getM_layoutcontrol().getfl_control_ID() == 110)
						).collect(Collectors.toList());
		if(items != null && items.size() > 0){
			for(Iterator<FLYControl> i = items.iterator(); i.hasNext(); ) {
				FLYReceipt item = (FLYReceipt)i.next().getM_control();
				item.updateInfos(C_Order_ID);
				
				
			}
		}
		
	}
	
	public void refreshControls(int C_Order_ID){
		if(C_Order_ID != 0){
			refreshDocInfos(C_Order_ID);
			refreshLines(C_Order_ID);
			
		}
	}

}
