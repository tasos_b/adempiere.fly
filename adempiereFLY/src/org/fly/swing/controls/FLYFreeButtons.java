/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import org.compiere.process.FLYProcessList;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.Env;
import org.fly.core.FLYEnv;
import org.fly.core.FLYProcessResultHandler;
import org.fly.core.classes.FLYPInstance;
import org.fly.core.classes.FLYPInstanceList;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.StringUtil;
import org.fly.swing.base.FLYButton;
import org.fly.swing.controls.base.FLYBaseButtonGroup;
import org.fly.swing.utils.SwingUtil;

public class FLYFreeButtons extends FLYBaseButtonGroup {

	/**
	 * 
	 */
	private int m_fl_layoutcontrol_ID;

	private int m_buttonWidth = 100;
	private int m_buttonHeight = 80;
	private int m_fontSize = 10;
	FLYPInstanceList list = null;

	private static final long serialVersionUID = 2951989096903731178L;

	public FLYFreeButtons(byte[] setup, int fl_layoutcontrol_ID) {
		super();
		if (setup != null && setup.length > 0) {
			ProcessInfo pInfo = ByteArrayUtil.byteArrayToProcessInfo(setup);
			ProcessInfoParameter[] params = pInfo.getParameter();
			ProcessInfoParameter paramFound = FLYProcessUtil.getParameter(
					params, "Button_Width");
			if (paramFound != null)
				m_buttonWidth = paramFound.getParameterAsInt();
			paramFound = FLYProcessUtil.getParameter(params, "Button_Height");
			if (paramFound != null)
				m_buttonHeight = paramFound.getParameterAsInt();
		}
		m_fl_layoutcontrol_ID = fl_layoutcontrol_ID;
		loadButtons();
	}

	private void loadButtons() {
		disposeButtons();
		list = new FLYPInstanceList(m_fl_layoutcontrol_ID);
		if (list.getFreeButtons() == null)
			return;
		ArrayList<FLYPInstance> instances = list.getFreeButtons();
		this.setButtonCount(instances.size());
		this.setButtonWidth(m_buttonWidth);
		this.setButtonHeight(m_buttonHeight);
		int idx = 0;
		for (FLYPInstance item : instances) {
			FLYButton button = getButtons().get(idx);

			// Set Text
			String title = item.getName();
			if (!StringUtil.isNullOrEmpty(item.getTitle()))
				title = item.getTitle();
			SwingUtil.addTextToButton(button, item.getForecolor(),
					item.getBackcolor(), title, "black", m_fontSize);

			// Set Icon
			if (item.getImage() != null) {
				ImageIcon ico = item.getImage();
				SwingUtil.addImageToButton(button, ico);
			}
			FLYProcessList processList = list.getProcessListFromPInstance(item);
			button.setTag(processList);
			button.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					FLYButton btn = (FLYButton) evt.getSource();
					Object tagObject = btn.getTag();
					if (tagObject instanceof FLYProcessList) {
						FLYProcessList pList = (FLYProcessList) tagObject;
						ProcessInfo pi = pList.execute(FLYEnv.instance
								.getEnvVars());
						new FLYProcessResultHandler().handle(Env.getCtx(), pi,
								null);
					}
				}
			});
			idx++;
		}
	}

}
