/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls;

import java.awt.Color;

import org.compiere.model.Mlayoutcontrol;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.controls.base.FLYBaseControl;
import org.fly.swing.controls.base.Resizable;
import org.fly.swing.controls.base.ResizableBorder;

public class FLYControl {

	public static final int FREE_BUTTONS 			= 100;
	public static final int RECEIPT_LINES 			= 101;
	public static final int PRODUCT_CATEGORIES 		= 102;
	public static final int PRODUCTS 				= 103;
	//public static final int RECEIPT_SUMMARY 		= 104;
	public static final int BARCODE 				= 105;
	public static final int QUANTITY_NUMPAD 		= 106;
	public static final int PRODUCT_CLASSIFICATION 	= 107;
	public static final int PRODUCT_CLASS		 	= 108;
	public static final int PRODUCT_GROUP		 	= 109;
	public static final int RECEIPT 				= 110;

	private Mlayoutcontrol 	m_layoutcontrol;
	private FLYBaseControl 	m_control;
	private Resizable 		m_res;

	public Resizable getM_res() {
		return m_res;
	}

	public void setM_res(Resizable m_res) {
		this.m_res = m_res;
	}

	public FLYControl(Mlayoutcontrol layoutcontrol) {
		m_layoutcontrol = layoutcontrol;
		
		create();
	}

	public Mlayoutcontrol getM_layoutcontrol() {
		return m_layoutcontrol;
	}

	public void setM_layoutcontrol(Mlayoutcontrol m_layoutcontrol) {
		this.m_layoutcontrol = m_layoutcontrol;
	}

	public FLYBaseControl getM_control() {
		return m_control;
	}

	public void setM_control(FLYBaseControl m_control) {
		this.m_control = m_control;
	}
	
	public void save(){
		if(m_control == null || m_res == null)
			return;
		m_layoutcontrol.setcontrol_x(m_res.getBounds().x);
		m_layoutcontrol.setcontrol_y(m_res.getBounds().y);
		m_layoutcontrol.setcontrol_width(m_res.getBounds().width);
		m_layoutcontrol.setcontrol_height(m_res.getBounds().height);
		m_layoutcontrol.save();
		
	}


	private void create() {
		int ID = m_layoutcontrol.getfl_control_ID();
		switch (ID) {
		case (PRODUCT_CATEGORIES):
			m_control = new FLYPCategory(m_layoutcontrol.getdata());
			break;
		case (PRODUCT_CLASSIFICATION):
			m_control = new FLYPClassification(m_layoutcontrol.getdata());
			break;
		case (PRODUCT_CLASS):
			m_control = new FLYPClass(m_layoutcontrol.getdata());
			break;
		case (PRODUCT_GROUP):
			m_control = new FLYPGroup(m_layoutcontrol.getdata());
			break;
		case (PRODUCTS):
			m_control = new FLYProducts(m_layoutcontrol.getdata());
			break;
		case (FREE_BUTTONS):
			m_control = new FLYFreeButtons(m_layoutcontrol.getdata(), m_layoutcontrol.getfl_layoutcontrol_ID());
			break;
		case (RECEIPT_LINES):
			m_control = new FLYReceiptLines(m_layoutcontrol.getdata());
			break;
		case (BARCODE):
			m_control = new FLYBarcode();
			break;
		case (RECEIPT):
			m_control = new FLYReceipt(m_layoutcontrol.getdata());
			break;
		default:
			m_control = null;
		}
		if(m_control != null){
			int x = m_layoutcontrol.getcontrol_x();
			int y = m_layoutcontrol.getcontrol_y();
			int width = m_layoutcontrol.getcontrol_width();
			int height = m_layoutcontrol.getcontrol_height();
			m_res = new Resizable(m_control);
			m_res.setResizable(true);
			m_res.setBounds(x, y, width, height);
			ResizableBorder border = (ResizableBorder)m_res.getBorder();
			border.setBorderColor(m_control.getBackground());
			border.setBorderColor(Color.black);
			
			FLYMainFrame.instance.getControlPanel().add(m_res);
		}

	}
}
