/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls.base;



import java.awt.Dimension;
import java.awt.Font;
import java.util.HashMap;

import javax.swing.JScrollPane;

import javax.swing.ListSelectionModel;

import net.miginfocom.swing.MigLayout;

import org.compiere.minigrid.MiniTable;

public class FLYBaseGrid extends FLYBaseControl implements IControlCommon{

	private static final long serialVersionUID = 5532092477907424157L;
	
	private MiniTable m_table = null;
	private JScrollPane m_scrollPane = new JScrollPane();

	

	private int m_scrollWidth = 30;
	//private Font _font = new FontUIResource("Verdana", Font.PLAIN, 18);

	public FLYBaseGrid(){
		 m_table = new MiniTable();
		//this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setLayout(new MigLayout("fill, ins 0 0"));
		add(m_scrollPane , "growx, spanx, growy, pushy, h 200:300:");
		m_scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		m_scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		m_scrollPane.getViewport().add(m_table, null);
		m_scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(m_scrollWidth, 0));
		m_scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(0,m_scrollWidth));
		getTable().setRowSelectionAllowed(true);
		getTable().setColumnSelectionAllowed(false);
		getTable().setMultiSelection(false);
		getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getTable().setRowHeight(30);
		getTable().setAutoResize(false);
	}
	public JScrollPane getM_scrollPane() {
		return m_scrollPane;
	}
	public int getM_scrollWidth() {
		return m_scrollWidth;
	}
	

	public MiniTable getTable(){
		return m_table;
	}

	@Override
	public void setCustomFont(Font font) {
		m_table.setFont(font);
		m_table.getTableHeader().setFont(font);
	}

	@Override
	public void notify(int message, HashMap<String, Object> data) {
		// TODO Auto-generated method stub
		
	}

	
}
