/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls.base;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Icon;
import javax.swing.plaf.FontUIResource;

import org.fly.swing.base.FLYButton;

public class FLYBaseButtonGroup extends FLYBaseControl implements
		IControlCommon {

	private static final long serialVersionUID = -1862112870289764040L;

	private ArrayList<FLYButton> _buttons = new ArrayList<FLYButton>();
	private int _buttonCount = 0;
	private FLYButton btnPrev;
	private FLYButton btnNext;
	private int _idx;
	private int _lastIdx = 0;
	private int _gap = 2;
	private int _buttonWidth;
	private int _buttonHeight;
	private int buttonsCapacity;
	
	private Icon m_parent;
	private Icon m_detail;


	private Font _font = new FontUIResource("Verdana", Font.PLAIN, 12);

	private boolean inDraw = false;

	public FLYBaseButtonGroup() {
		setLayout(null);
		setDoubleBuffered(true);
		m_parent = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/Parent24.gif"));
		m_detail = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/Detail24.gif"));


		this.addComponentListener(new ComponentListener() {
			public void componentResized(ComponentEvent e) {
				resizeButtons();
			}

			@Override
			public void componentHidden(ComponentEvent arg0) {
			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
			}

			@Override
			public void componentShown(ComponentEvent arg0) {
				resizeButtons();
			}
		});
	}

	private void resizeAllButtons(int w, int h) {
		inDraw = true;
		try {
			if (btnPrev != null)
				btnPrev.setSize(w, h);
			if (btnNext != null)
				btnNext.setSize(w, h);
			if (_buttons != null) {
				for (int i = 0; i < _buttons.size(); i++) {
					_buttons.get(i).setSize(w, h);
				}
			}
		} finally {
			inDraw = false;
		}
	}

	public void firstPacket() {
		if (inDraw)
			return;
		_idx = 0;
		drawButtons();
	}

	private void nextPacket() {
		if (inDraw)
			return;
		drawButtons();

	}

	public void resizeButtons() {
		if (inDraw)
			return;
		_idx = _lastIdx;
		if (_idx < getButtonCount())
			drawButtons();
	}

	private void previusPacket() {
		if (inDraw)
			return;
		_idx = _lastIdx - buttonsCapacity + 2;
		if (_idx < 2)
			_idx = 0;
		drawButtons();
	}

	private void hideButtons() {
		if (_buttons != null) {
			for (int i = _buttons.size() - 1; i >= 0; i--)
				_buttons.get(i).setVisible(false);
			btnPrev.setVisible(false);
			btnNext.setVisible(false);
		}
	}

	public void drawButtons() {
		inDraw = true;
		try {
			if (getButtonCount() == 0)
				return;
			hideButtons();
			if (getButtonWidth() == 0)
				setButtonWidth(55);
			if (getButtonHeight() == 0)
				setButtonHeight(50);
			boolean bPrevius = false;
			boolean bNext = false;
			int buttonWidth = getButtonWidth() + getGap();
			int buttonHeight = getButtonHeight() + getGap();
			int areaWidth = this.getWidth() - (getGap());
			int areaHeight = this.getHeight() - (getGap());

			int buttonsHorizontal = areaWidth / (buttonWidth + getGap());
			int buttonsVertical = areaHeight / (buttonHeight + getGap());
			int x = getGap();
			int y = getGap();
			buttonsCapacity = buttonsHorizontal * buttonsVertical;
			int btnStart = _idx;
			_lastIdx = _idx;
			if (btnStart > 1)
				bPrevius = true;
			int bDiff = (getButtonCount() - 1 + (bPrevius ? 1 : 0) - btnStart);
			if (bDiff >= buttonsCapacity) {
				bNext = true;

			}
			int btnEnd = btnStart
					+ (buttonsCapacity > getButtonCount() ? getButtonCount()
							: buttonsCapacity) - (bPrevius ? 1 : 0)
					- (bNext ? 1 : 0);
			if (btnEnd > getButtonCount())
				btnEnd = getButtonCount();
			int btnCount = btnEnd - btnStart;
			int cnt = 0;

			boolean bStop = false;
			for (int i = 0; i < buttonsVertical; i++) {
				for (int j = 0; j < buttonsHorizontal; j++) {
					if (bStop)
						break;
					if (i == 0 && j == 0 && bPrevius) {
						this.add(btnPrev);
						btnPrev.setLocation(x, y);
						btnPrev.setVisible(true);
						btnPrev.setFocusable(false);
						btnPrev.setIcon(m_parent);

					} else if (cnt < btnCount) {
						if (_idx < getButtonCount()) {
							this.add(_buttons.get(_idx));
							_buttons.get(_idx).setLocation(x, y);
							_buttons.get(_idx).setVisible(true);
							_idx++;
							cnt++;
						}

					} else if (cnt >= btnCount) {
						if (bNext && _idx < (getButtonCount() - 1)) {
							this.add(btnNext);
							btnNext.setLocation(x, y);
							btnNext.setVisible(true);
							btnNext.setFocusable(false);
							btnNext.setIcon(m_detail);
						}
						bStop = true;
						break;

					}
					if (j == buttonsHorizontal - 1)
						x = getGap();
					else
						x += getGap() + buttonWidth;
				}
				y += getGap() + buttonHeight;
			}

		} finally {

			inDraw = false;

		}
	}

	private void addButtons(int cnt) {
		if (getButtonWidth() == 0)
			setButtonWidth(55);
		if (getButtonHeight() == 0)
			setButtonHeight(50);
		int buttonWidth = getButtonWidth() + getGap();
		int buttonHeight = getButtonHeight() + getGap();
		btnPrev = new FLYButton();
		btnPrev.setFocusable(false);
		btnPrev.setFocusPainted(false);

		btnPrev.setFont(_font);
		btnPrev.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				previusPacket();
			}
		});
		btnNext = new FLYButton();
		btnNext.setFocusable(false);
		btnNext.setFocusPainted(false);

		btnNext.setFont(_font);
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextPacket();
			}
		});

		for (int i = 0; i < cnt; i++) {
			FLYButton btn = new FLYButton();
			btn.setFocusable(false);
			btn.setFocusPainted(false);

			btn.setFont(_font);
			_buttons.add(btn);
		}
		resizeAllButtons(buttonWidth, buttonHeight);

	}

	public void disposeButtons() {
		removeAll();
		/*
		 * if (_buttons != null) { for (int i = _buttons.size() - 1; i >= 0;
		 * i--){ FLYButton btn = _buttons.get(i); _buttons.remove(i); btn =
		 * null; } _buttons.clear(); }
		 */
		if (_buttons != null) {
			_buttons.clear();
		}
		if (btnNext != null) {

			btnNext = null;
		}
		if (btnPrev != null) {
			btnPrev = null;
		}
		_idx = 0;
		_lastIdx = 0;
		revalidate();
		repaint();
	}

	public int getButtonWidth() {
		return _buttonWidth;
	}

	public int getButtonHeight() {
		return _buttonHeight;
	}

	public void setButtonWidth(int width) {
		_buttonWidth = width;
		resizeAllButtons(_buttonWidth, _buttonHeight);
	}

	public void setButtonHeight(int height) {
		_buttonHeight = height;
		resizeAllButtons(_buttonWidth, _buttonHeight);
	}

	public ArrayList<FLYButton> getButtons() {
		return _buttons;
	}

	public int getGap() {
		return _gap;
	}

	public void setGap(int gap) {
		_gap = gap;
	}

	public int getButtonCount() {
		return _buttonCount;
	}

	public void setButtonCount(int cnt) {
		disposeButtons();
		addButtons(cnt);

		_buttonCount = cnt;
		_idx = 0;

	}

	@Override
	public void setCustomFont(Font font) {
		_font = font;

	}

	@Override
	public void notify(int message, HashMap<String, Object> data) {
		// TODO Auto-generated method stub

	}

}
