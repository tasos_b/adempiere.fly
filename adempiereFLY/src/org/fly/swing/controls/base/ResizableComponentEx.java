/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls.base;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class ResizableComponentEx extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3173311711510539294L;
	private Resizable res;
	private Resizable res2;

    public ResizableComponentEx(boolean resizable) {

        initUI(resizable);
    }
    
 
    private void initUI(boolean resizable) {

        JPanel pnl = new JPanel(null);
        add(pnl);

        JPanel area = new JPanel();
        area.setBackground(Color.red);
        res = new Resizable(area);
        res.setResizable(resizable);
        res.setBounds(50, 50, 200, 150);
        pnl.add(res);
        
        JPanel area2 = new JPanel();
        area2.setBackground(Color.green);
        res2 = new Resizable(area2);
        res2.setResizable(resizable);
        res2.setBounds(100, 100, 200, 150);
        pnl.add(res2);
        

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent me) {

                requestFocus();
                res.repaint();
                res2.repaint();
            }
        });

        setSize(350, 300);
        setTitle("Resizable component");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                ResizableComponentEx ex = new ResizableComponentEx(true);
                
                ex.setVisible(true);
            }
        });
    }
}