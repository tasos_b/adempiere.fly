/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls.base;


import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import org.compiere.model.Mlayoutcontrol;

public class FLYBaseControl extends JPanel implements IControlCommon{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -383412046787778796L;

	Mlayoutcontrol m_control;

	public FLYBaseControl(){
		Border raisedbevel = BorderFactory.createRaisedBevelBorder();
		raisedbevel.getBorderInsets(this).top = 1;
		raisedbevel.getBorderInsets(this).left = 1;
		raisedbevel.getBorderInsets(this).right = 1;
		raisedbevel.getBorderInsets(this).bottom = 1;
		//Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		//Border compound = BorderFactory.createCompoundBorder(
        //        raisedbevel, loweredbevel);
		//setBorder(compound);
		setBorder(raisedbevel);
		
	}
	
	public Mlayoutcontrol getM_control() {
		return m_control;
	}

	@Override
	public void setCustomFont(Font font) {
		
		
	}

	@Override
	public void notify(int message, HashMap<String, Object> data) {
		
		
	}

	@Override
	public void load() {
		this.setLocation(new Point(m_control.getcontrol_x(), m_control.getcontrol_y()));
		this.setSize(new Dimension(m_control.getcontrol_width(), m_control.getcontrol_height()));
	}
	
	@Override
	public void save() {
		m_control.setcontrol_x(this.getX());
		m_control.setcontrol_y(this.getY());
		m_control.setcontrol_width(this.getWidth());
		m_control.setcontrol_height(this.getHeight());
		m_control.save();
		
	}

	@Override
	public void setFlControl(Mlayoutcontrol control) {
		m_control = control;
		
	}

}
