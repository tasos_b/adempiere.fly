/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls.base;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;


public class FLYBaseKeyPad extends JPanel {

 
	private static final long serialVersionUID = -232962140440874167L;
	private int maxDigits = 100;
    private final JTextArea text = new JTextArea(1, maxDigits);
    private final JButton clear = new JButton(new Clear("Clear"));
    private final JButton comma = new JButton(new Enter("."));
    private final List<NumberButton> numbers = new ArrayList<NumberButton>();
    private final JButton enter = new JButton(new Enter("Enter"));


    public FLYBaseKeyPad() {
        super(new BorderLayout());

        JPanel display = new JPanel();
        text.setFont(new Font("Dialog", Font.PLAIN, 24));
        text.setEditable(false);
        text.setFocusable(false);
        display.add(text);
        this.add(display, BorderLayout.NORTH);

        JPanel pad = new JPanel(new GridLayout(4, 3));
        for (int i = 0; i < 10; i++) {
            NumberButton n = new NumberButton(i);
            numbers.add(n);
            if (i > 0) {
                pad.add(n);
            }
        }
        pad.add(clear);
        clear.setFocusable(false);
        this.getInputMap().put(KeyStroke.getKeyStroke(
            KeyEvent.VK_CLEAR, 0), clear.getText());
        this.getActionMap().put(clear.getText(), new Click(clear));
        pad.add(numbers.get(0));
        pad.add(comma);
        enter.setFocusable(false);
        this.getInputMap().put(KeyStroke.getKeyStroke(
            KeyEvent.VK_ENTER, 0), enter.getText());
        this.getActionMap().put(enter.getText(), new Click(enter));
        this.add(pad, BorderLayout.CENTER);
        this.add(enter, BorderLayout.SOUTH);
    }


    public FLYBaseKeyPad(int maxDigits) {
        this();
        this.maxDigits = maxDigits;
        this.text.setColumns(maxDigits);
    }

    private class Clear extends AbstractAction {

        /**
		 * 
		 */
		private static final long serialVersionUID = -6652246201361916167L;

		public Clear(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            text.setText("");
            for (NumberButton n : numbers) {
                n.setEnabled(true);
            }
        }
    }

    private class Enter extends AbstractAction {

        /**
		 * 
		 */
		private static final long serialVersionUID = -9106586714438760573L;

		public Enter(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Entered: " + text.getText());
            clear.getAction().actionPerformed(e);
        }
    }

    private class Click extends AbstractAction {

		private static final long serialVersionUID = -7668530405233082578L;
		JButton button;

        public Click(JButton button) {
            this.button = button;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            button.doClick();
        }
    }


    private class NumberButton extends JButton {

 
		private static final long serialVersionUID = -7732461007347721263L;

		public NumberButton(int number) {
            super(String.valueOf(number));
            this.setFocusable(false);
            this.setAction(new AbstractAction(this.getText()) {


				private static final long serialVersionUID = 882780830766933031L;

				@Override
                public void actionPerformed(ActionEvent e) {
                    String cmd = e.getActionCommand();
                    if (text.getText().length() < maxDigits) {
                        text.append(cmd);
                    }
                    if (text.getText().length() == maxDigits) {
                        for (NumberButton n : numbers) {
                            n.setEnabled(false);
                        }
                    }
                }
            });
            FLYBaseKeyPad.this.getInputMap().put(KeyStroke.getKeyStroke(
                KeyEvent.VK_0 + number, 0), this.getText());
            FLYBaseKeyPad.this.getInputMap().put(KeyStroke.getKeyStroke(
                KeyEvent.VK_NUMPAD0 + number, 0), this.getText());
            FLYBaseKeyPad.this.getActionMap().put(this.getText(), new Click(this));
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame f = new JFrame();
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                f.add(new FLYBaseKeyPad(7));
                f.pack();
                f.setLocationRelativeTo(null);
                f.setVisible(true);
            }
        });
    }
}