/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls;


import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;

import org.adempiere.exceptions.DBException;
import org.compiere.model.Mticket;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.swing.CLabel;
import org.compiere.swing.CTextField;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.swing.controls.base.FLYBaseControl;

public class FLYReceipt extends FLYBaseControl {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4648625206126172302L;

	private boolean showDocumentNo = true;
	private boolean showBPartner = true;
	private boolean showSubTotal = true;
	private boolean showTaxAmt = true;
	private boolean showGrandTotal = true;
	private boolean showPaymentTotal = true;
	private boolean showBalanceDue = true;
	

	private CLabel l_DocumentNO;
	private CTextField f_DocumentNo;

	private CLabel l_BPartner;
	private CTextField f_BPartner;

	private CLabel l_SubTotal;
	private JFormattedTextField f_SubTotal;

	private CLabel l_TaxAmt;
	private JFormattedTextField f_TaxAmt;

	private CLabel l_GrandTotal;
	private JFormattedTextField f_GrandTotal;

	private CLabel l_PaymentTotal;
	private JFormattedTextField f_PaymentTotal;

	private CLabel l_BalanceDue;
	private JFormattedTextField f_BalanceDue;
	
	
	public FLYReceipt(byte[] setup) {
		if (setup != null && setup.length > 0) {
			ProcessInfo pInfo = ByteArrayUtil.byteArrayToProcessInfo(setup);
			ProcessInfoParameter[] params = pInfo.getParameter();

			ProcessInfoParameter paramFound = FLYProcessUtil.getParameter(
					params, "DocumentNo");
			if (paramFound != null)
				showDocumentNo = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "BPartner");
			if (paramFound != null)
				showBPartner = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "SubTotal");
			if (paramFound != null)
				showSubTotal = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "TaxAmt");
			if (paramFound != null)
				showTaxAmt = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "GrandTotal");
			if (paramFound != null)
				showGrandTotal = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "PaymentTotal");
			if (paramFound != null)
				showPaymentTotal = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "BalanceDue");
			if (paramFound != null)
				showBalanceDue = paramFound.getParameterAsBoolean();

		}

		l_DocumentNO = new CLabel(Msg.getMsg(Env.getCtx(), "DocumentNo"));
		l_BPartner = new CLabel(Msg.getMsg(Env.getCtx(), "C_BPartner_ID"));
		l_SubTotal = new CLabel(Msg.getMsg(Env.getCtx(), "SubTotal"));
		l_TaxAmt = new CLabel(Msg.getMsg(Env.getCtx(), "TaxAmt"));
		l_GrandTotal = new CLabel(Msg.getMsg(Env.getCtx(), "GrandTotal"));
		l_PaymentTotal = new CLabel(Msg.getMsg(Env.getCtx(), "PaymentTotal"));
		l_BalanceDue = new CLabel(Msg.getMsg(Env.getCtx(), "BalanceDue"));

		f_DocumentNo = new CTextField("");
		f_DocumentNo.setName("DocumentNo");
		f_DocumentNo.setEditable(false);
		f_DocumentNo.setFocusable(false);

		f_BPartner = new CTextField();
		f_BPartner.setEditable(false);
		f_BPartner.setName("BPartner");
		f_BPartner.setFocusable(false);

		f_SubTotal = new JFormattedTextField(
				DisplayType.getNumberFormat(DisplayType.Amount));
		f_SubTotal.setHorizontalAlignment(JTextField.TRAILING);
		f_SubTotal.setEditable(false);
		f_SubTotal.setFocusable(false);
		f_SubTotal.setValue(Env.ZERO);

		f_TaxAmt = new JFormattedTextField(
				DisplayType.getNumberFormat(DisplayType.Amount));
		f_TaxAmt.setHorizontalAlignment(JTextField.TRAILING);
		f_TaxAmt.setEditable(false);
		f_TaxAmt.setFocusable(false);
		f_TaxAmt.setValue(Env.ZERO);

		f_GrandTotal = new JFormattedTextField(
				DisplayType.getNumberFormat(DisplayType.Amount));
		f_GrandTotal.setHorizontalAlignment(JTextField.TRAILING);
		f_GrandTotal.setEditable(false);
		f_GrandTotal.setFocusable(false);
		f_GrandTotal.setValue(Env.ZERO);

		f_PaymentTotal = new JFormattedTextField(
				DisplayType.getNumberFormat(DisplayType.Amount));
		f_PaymentTotal.setHorizontalAlignment(JTextField.TRAILING);
		f_PaymentTotal.setEditable(false);
		f_PaymentTotal.setFocusable(false);
		f_PaymentTotal.setValue(Env.ZERO);

		f_BalanceDue = new JFormattedTextField(
				DisplayType.getNumberFormat(DisplayType.Amount));
		f_BalanceDue.setHorizontalAlignment(JTextField.TRAILING);
		f_BalanceDue.setEditable(false);
		f_BalanceDue.setFocusable(false);
		f_BalanceDue.setValue(Env.ZERO);
		
		
		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

		ParallelGroup pg = layout.createParallelGroup();
		hGroup.addGroup(pg);
		
		if (showDocumentNo)
			pg.addComponent(l_DocumentNO);
		if (showBPartner)
			pg.addComponent(l_BPartner);
		if (showSubTotal)
			pg.addComponent(l_SubTotal);
		if (showTaxAmt)
			pg.addComponent(l_TaxAmt);
		if (showGrandTotal)
			pg.addComponent(l_GrandTotal);
		if (showPaymentTotal)
			pg.addComponent(l_PaymentTotal);
		if (showBalanceDue)
			pg.addComponent(l_BalanceDue);
		ParallelGroup pg2 = layout.createParallelGroup();
		hGroup.addGroup(pg2);
		if (showDocumentNo)
			pg2.addComponent(f_DocumentNo);
		if (showBPartner)
			pg2.addComponent(f_BPartner);
		if (showSubTotal)
			pg2.addComponent(f_SubTotal);
		if (showTaxAmt)
			pg2.addComponent(f_TaxAmt);
		if (showGrandTotal)
			pg2.addComponent(f_GrandTotal);
		if (showPaymentTotal)
			pg2.addComponent(f_PaymentTotal);
		if (showBalanceDue)
			pg2.addComponent(f_BalanceDue);
		layout.setHorizontalGroup(hGroup);

		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
		if (showDocumentNo)
			vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
					.addComponent(l_DocumentNO).addComponent(f_DocumentNo));
		if (showBPartner)
			vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
					.addComponent(l_BPartner).addComponent(f_BPartner));
		if (showSubTotal)
			vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
					.addComponent(l_SubTotal).addComponent(f_SubTotal));
		if (showTaxAmt)
			vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
					.addComponent(l_TaxAmt).addComponent(f_TaxAmt));
		if (showGrandTotal)
			vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
					.addComponent(l_GrandTotal).addComponent(f_GrandTotal));
		if (showPaymentTotal)
			vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
					.addComponent(l_PaymentTotal).addComponent(f_PaymentTotal));
		if (showBalanceDue)
			vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
					.addComponent(l_BalanceDue).addComponent(f_BalanceDue));

		layout.setVerticalGroup(vGroup);
	}
	public void updateInfos(int C_Order_ID){
		if(C_Order_ID == 0){
			f_DocumentNo.setText("");
			f_BPartner.setText("");
			f_SubTotal.setValue(Env.ZERO);
			f_TaxAmt.setValue(Env.ZERO);
			f_GrandTotal.setValue(Env.ZERO);
			f_PaymentTotal.setValue(Env.ZERO);
			f_BalanceDue.setValue(Env.ZERO);
		}
		String sql = "SELECT documentno, name, amount, taxamount, grandtotal " 
				+"from fl_ticket_order_infos_v "
				+"WHERE c_order_id=?";
		SQLUtil util = new SQLUtil(sql, null, C_Order_ID);
		
		try{
			assignFields(util.getResultSet(), C_Order_ID);
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

	}
	public void assignFields(ResultSet rs, int C_Order_ID) throws SQLException {
		while (rs.next()) {
			f_DocumentNo.setText(rs.getString("documentno"));
			f_BPartner.setText(rs.getString("name"));
			Object amount = rs.getObject("amount");
			f_SubTotal.setValue(amount);
			Object taxamount = rs.getObject("taxamount");
			f_TaxAmt.setValue(taxamount);
			BigDecimal grandtotal = rs.getBigDecimal("grandtotal");
			f_GrandTotal.setValue(grandtotal);
			BigDecimal paidAmt = Mticket.getPaidAmt(C_Order_ID);
			f_PaymentTotal.setValue(paidAmt);
			f_BalanceDue.setValue(grandtotal.subtract(paidAmt));
			
		}
		
		
	}
}
