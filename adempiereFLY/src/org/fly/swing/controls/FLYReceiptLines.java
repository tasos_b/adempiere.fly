/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls;

import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;

import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.compiere.apps.AppsAction;
import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.swing.CButton;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.fly.core.FLYEnv;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.swing.FLYSwingController;
import org.fly.swing.controls.base.FLYBaseGrid;

public class FLYReceiptLines extends FLYBaseGrid implements ActionListener,
		ListSelectionListener {

	private static final long serialVersionUID = -2486831226253196056L;
	private static CLogger log = CLogger.getCLogger(FLYReceiptLines.class);

	private boolean showName = true;
	private boolean showQuantity = true;
	private boolean showUOM = true;
	private boolean showPriceActual = true;
	private boolean showLineNetAmt = true;
	private boolean showTaxIndicator = true;

	String buttonSize = "w 50!, h 50!,";
	private CButton f_up;
	private CButton f_down;
	private CButton f_plus;
	private CButton f_minus;
	private CButton f_delete;

	private String m_sql;

	/** From Clause */
	private static String s_sqlFrom = "fl_ticket_line_v";
	/** Where Clause */
	private static String s_sqlWhere = "C_Order_ID=? ";

	public FLYReceiptLines(byte[] setup) {
		super();
		if (setup != null && setup.length > 0) {
			ProcessInfo pInfo = ByteArrayUtil.byteArrayToProcessInfo(setup);
			ProcessInfoParameter[] params = pInfo.getParameter();

			ProcessInfoParameter paramFound = FLYProcessUtil.getParameter(
					params, "Name");
			if (paramFound != null)
				showName = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "Quantity");
			if (paramFound != null)
				showQuantity = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "UOM");
			if (paramFound != null)
				showUOM = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "PriceActual");
			if (paramFound != null)
				showPriceActual = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "LineNetAmt");
			if (paramFound != null)
				showLineNetAmt = paramFound.getParameterAsBoolean();

			paramFound = FLYProcessUtil.getParameter(params, "C_Tax_ID");
			if (paramFound != null)
				showTaxIndicator = paramFound.getParameterAsBoolean();
		}

		getTable().setRowSelectionAllowed(true);
		getTable().setColumnSelectionAllowed(false);
		getTable().setMultiSelection(false);
		getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getTable().setRowHeight(30);
		getTable().setAutoResize(false);
		init();
	}

	/** Table Column Layout Info */
	private static ColumnInfo[] s_layout = new ColumnInfo[] {
			new ColumnInfo(" ", "C_OrderLine_ID", IDColumn.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "Name",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Qty"), "QtyOrdered",
					Double.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "C_UOM_ID"),
					"UOMSymbol", String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "PriceActual"),
					"PriceActual", BigDecimal.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "LineNetAmt"),
					"LineNetAmt", BigDecimal.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "C_Tax_ID"),
					"TaxIndicator", String.class), };

	public void init() {
		f_up = createButtonAction("Previous",
				KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0));
		add(f_up, buttonSize);
		f_down = createButtonAction("Next",
				KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0));
		add(f_down, buttonSize);
		f_minus = createButtonAction("Minus", null);
		add(f_minus, buttonSize);
		f_plus = createButtonAction("Plus", null);
		add(f_plus, buttonSize);
		f_delete = createButtonAction("Cancel",
				KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, Event.SHIFT_MASK));
		add(f_delete, buttonSize);

		initMiniTable();

	}

	private void enableButtons() {
		boolean enabled = true;
		if (getTable() == null || getTable().getRowCount() == 0
				|| getTable().getSelectedRowKey() == null) {
			enabled = false;
		}
		f_down.setEnabled(enabled);
		f_up.setEnabled(enabled);
		f_minus.setEnabled(enabled);
		f_plus.setEnabled(enabled);
		f_delete.setEnabled(enabled);
		/*
		 * f_delete.setEnabled(enabled); f_minus.setEnabled(enabled);
		 * f_plus.setEnabled(enabled); f_quantity.setEnabled(enabled);
		 * f_price.setEnabled(enabled);
		 */
	}

	private void initMiniTable() {
		m_sql = getTable().prepareTable(s_layout, s_sqlFrom, s_sqlWhere, false,
				"fl_ticket_line_v")
				+ " ORDER BY Line";
		getTable().getSelectionModel().addListSelectionListener(this);
		getTable().setColumnVisibility(getTable().getColumn(0), false);

		getTable().setColumnVisibility(getTable().getColumn(1), showName);
		getTable().setColumnVisibility(getTable().getColumn(2), showQuantity);
		getTable().setColumnVisibility(getTable().getColumn(3), showUOM);
		getTable()
				.setColumnVisibility(getTable().getColumn(4), showPriceActual);
		getTable().setColumnVisibility(getTable().getColumn(5), showLineNetAmt);
		getTable().setColumnVisibility(getTable().getColumn(6),
				showTaxIndicator);

		getTable().getColumn(1).setPreferredWidth(175);
		getTable().getColumn(2).setPreferredWidth(75);
		getTable().getColumn(3).setPreferredWidth(30);
		getTable().getColumn(4).setPreferredWidth(100);
		getTable().getColumn(5).setPreferredWidth(100);
		getTable().getColumn(6).setPreferredWidth(60);
		getTable().setFocusable(false);
		getTable().setFillsViewportHeight(true);
		enableButtons();
	}

	public void updateTable(int C_Order_ID) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(m_sql, null);
			pstmt.setInt(1, C_Order_ID);
			rs = pstmt.executeQuery();
			getTable().loadTable(rs);
		} catch (Exception e) {
			log.log(Level.SEVERE, m_sql, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		if (getTable().getRowCount() > 0 && FLYEnv.instance.getC_Order_Line_ID() > 0) {
			// int idx = getTable().getRowCount() - 1;
			for (int i = 0; i < getTable().getRowCount(); i++) {
				IDColumn key = (IDColumn) getTable().getModel()
						.getValueAt(i, 0);
				int envID = FLYEnv.instance.getC_Order_Line_ID();
				if (key.getRecord_ID() == envID) {
					// FLYEnv.instance.setC_Order_Line_ID(key.getRecord_ID());
					getTable().getSelectionModel().setSelectionInterval(i, i);
					break;
				}
			}
		}
		enableButtons();
		refreshScrollBars();
	}

	private void refreshScrollBars() {
		getM_scrollPane().setHorizontalScrollBarPolicy(
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		getM_scrollPane().setVerticalScrollBarPolicy(
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		getM_scrollPane().getVerticalScrollBar().setPreferredSize(
				new Dimension(getM_scrollWidth(), 0));
		getM_scrollPane().getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, getM_scrollWidth()));
	}

	protected CButton createButtonAction(String action, KeyStroke accelerator) {
		AppsAction act = new AppsAction(action, accelerator, false);
		act.setDelegate(this);
		CButton button = (CButton) act.getButton();
		button.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		button.setMinimumSize(getPreferredSize());
		button.setMaximumSize(getPreferredSize());
		button.setFocusable(false);
		return button;
	} // getButtonAction

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if (action == null || action.length() == 0)
			return;
		if ("Next".equalsIgnoreCase(e.getActionCommand())) {
			int rows = getTable().getRowCount();
			if (rows == 0)
				return;
			int row = getTable().getSelectedRow();
			row++;
			if (row >= rows)
				row = rows - 1;
			getTable().getSelectionModel().setSelectionInterval(row, row);
			getTable()
					.scrollRectToVisible(getTable().getCellRect(row, 1, true));
			return;
		} else if ("Previous".equalsIgnoreCase(e.getActionCommand())) {
			int rows = getTable().getRowCount();
			if (rows == 0)
				return;
			int row = getTable().getSelectedRow();
			row--;
			if (row < 0)
				row = 0;
			getTable().getSelectionModel().setSelectionInterval(row, row);
			getTable()
					.scrollRectToVisible(getTable().getCellRect(row, 1, true));
			return;
		} else if (action.equals("Plus")) {
			if (FLYEnv.instance.getC_Order_Line_ID() > 0) {
				MOrderLine line = new MOrderLine(Env.getCtx(),
						FLYEnv.instance.getC_Order_Line_ID(), null);
				if (line != null) {
					line.setQty(line.getQtyOrdered().add(Env.ONE));
					line.saveEx();
					updateTable(FLYEnv.instance.getC_Order_ID());
				}
				FLYSwingController.instance.refreshDocInfos(FLYEnv.instance.getC_Order_ID());
			}

		}
		// Minus
		else if (action.equals("Minus")) {
			if (FLYEnv.instance.getC_Order_Line_ID() > 0) {
				MOrderLine line = new MOrderLine(Env.getCtx(),
						FLYEnv.instance.getC_Order_Line_ID(), null);
				if (line != null) {
					line.setQty(line.getQtyOrdered().subtract(Env.ONE));
					line.saveEx();
					updateTable(FLYEnv.instance.getC_Order_ID());
				}
				FLYSwingController.instance.refreshDocInfos(FLYEnv.instance.getC_Order_ID());

			}

		} else if (action.equals("Cancel")) {
			int rows = getTable().getRowCount();
			if (rows != 0) {
				int row = getTable().getSelectedRow();
				if (row != -1) {
					MOrderLine line = new MOrderLine(Env.getCtx(),
							FLYEnv.instance.getC_Order_Line_ID(), null);
					line.delete(true);
					FLYEnv.instance.setC_Order_Line_ID(0);
					updateTable(FLYEnv.instance.getC_Order_ID());
					FLYSwingController.instance.refreshDocInfos(FLYEnv.instance.getC_Order_ID());

					// getTable().getSelectionModel().setSelectionInterval(-1,
					// -1);
				}
			}
		}

	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// if ( e.getValueIsAdjusting() )
		// return;

		int row = getTable().getSelectedRow();
		if (row != -1) {
			Object data = getTable().getModel().getValueAt(row, 0);
			if (data != null) {
				Integer id = (Integer) ((IDColumn) data).getRecord_ID();
				FLYEnv.instance.setC_Order_Line_ID(id);
			}
		}
		enableButtons();

	}
}
