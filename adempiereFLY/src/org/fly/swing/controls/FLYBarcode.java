/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.fly.swing.controls.base.FLYBaseControl;

public class FLYBarcode extends FLYBaseControl  {

	private static final long serialVersionUID = -7793171949019207259L;
	
	Font font = new Font("TimesRoman", Font.BOLD, 22);
	Font textFont = new Font("TimesRoman", Font.PLAIN, 22);
    
	private final JTextField barcode = new JTextField();
    private final JTextField quant = new JTextField();
    private final JButton addQuantity = new JButton(new AddQuantity("+"));
    private final JButton subQuantity = new JButton(new SubQuantity("-"));
    private final JButton okButton = new JButton(new OkAction(""));
    private final JButton kbdButton = new JButton(new KbdAction(""));
    
	private Icon m_ok_ico;
	private Icon m_keyboard_ico;

    
    public FLYBarcode() {
    	super();


    	addQuantity.setFont(font);
    	subQuantity.setFont(font);
    	
    	quant.setMinimumSize(new Dimension(80, 40));
    	quant.setMaximumSize(new Dimension(140, 40));
    	quant.setEditable(false);
    	quant.setFont(textFont);
    	quant.setText("1");
    	quant.setFocusable(false);
    	

    	barcode.setMinimumSize(new Dimension(150, 40));
    	barcode.setMaximumSize(new Dimension(600, 40));
    	barcode.setFont(textFont);
    	barcode.setText("");
    	
    	addQuantity.setMaximumSize(new Dimension(200, 60));
    	addQuantity.setFocusable(false);
    	subQuantity.setMaximumSize(new Dimension(200, 60));
    	subQuantity.setFocusable(false);
    	okButton.setMaximumSize(new Dimension(200, 60));
    	okButton.setFocusable(false);
    	m_ok_ico = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/Ok24.gif"));
    	okButton.setIcon(m_ok_ico);
    	//okButton.setVerticalTextPosition(SwingConstants.BOTTOM);
    	//okButton.setHorizontalTextPosition(SwingConstants.CENTER);

    	kbdButton.setMaximumSize(new Dimension(200, 60));
    	kbdButton.setFocusable(false);
    	m_keyboard_ico = new javax.swing.ImageIcon(getClass().getResource(
				"/org/compiere/images/pad.png"));
    	kbdButton.setIcon(m_keyboard_ico);
    	
    	
    	this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    	JPanel textLayout = new JPanel();
    	textLayout.setLayout(new BoxLayout(textLayout, BoxLayout.X_AXIS));
    	textLayout.add(quant);
    	textLayout.add(barcode);

    	
    	JPanel buttonsLayout = new JPanel();
    	buttonsLayout.setLayout(new BoxLayout(buttonsLayout, BoxLayout.X_AXIS));
    	buttonsLayout.add(addQuantity);
    	buttonsLayout.add(subQuantity);
    	buttonsLayout.add(okButton);
    	buttonsLayout.add(kbdButton);
    	
    	add(textLayout);
    	add(buttonsLayout);
    	
    }
    
    private void reset(){
    	quant.setText("1");
    	barcode.setText("");
    	
    }
    
    private class AddQuantity extends AbstractAction {

    	private static final long serialVersionUID = -6652246201361916167L;

		public AddQuantity(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int num = Integer.parseInt(quant.getText()) + 1;
            quant.setText(Integer.toString(num));
        	
        }
    }
    private class SubQuantity extends AbstractAction {

    	private static final long serialVersionUID = -6652246201361916167L;

		public SubQuantity(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int num = Integer.parseInt(quant.getText()) - 1;
            quant.setText(Integer.toString(num));
        }
    }
    private class OkAction extends AbstractAction {

    	private static final long serialVersionUID = -6652246201361916167L;

		public OkAction(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {

        }
    }

    private class KbdAction extends AbstractAction {

    	private static final long serialVersionUID = -6652246201361916167L;

		public KbdAction(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {

        }
    }
    
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame f = new JFrame();
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                f.add(new FLYBarcode());
                f.pack();
                f.setLocationRelativeTo(null);
                f.setVisible(true);
            }
        });
    }

}