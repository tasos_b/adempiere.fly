/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls;

import java.math.BigDecimal;
import java.util.List;

import javax.swing.ImageIcon;

import org.compiere.process.FLYProcess;
import org.compiere.process.FLYProcessList;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.Env;
import org.fly.core.FLYEnv;
import org.fly.core.FLYProcessResultHandler;
import org.fly.core.classes.FLYProduct;
import org.fly.core.classes.FLYProductCategory;
import org.fly.core.classes.FLYProductClass;
import org.fly.core.classes.FLYProductClassification;
import org.fly.core.classes.FLYProductGroup;
import org.fly.core.classes.FLYProductList;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.StringUtil;
import org.fly.swing.FLYSwingController;
import org.fly.swing.base.FLYButton;
import org.fly.swing.controls.base.FLYBaseButtonGroup;
import org.fly.swing.utils.SwingUtil;


public class FLYProducts extends FLYBaseButtonGroup{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2423399361585853708L;
	private int m_buttonWidth = 100;
	private int m_buttonHeight = 80;
	private int m_fontSize = 10;
	
	public FLYProducts(byte[] setup){
		super();
		if(setup != null && setup.length > 0){
			ProcessInfo pInfo = ByteArrayUtil.byteArrayToProcessInfo(setup);
			ProcessInfoParameter[] params = pInfo.getParameter();
			ProcessInfoParameter paramFound = FLYProcessUtil.getParameter(params, "Button_Width");
			if(paramFound != null)
				m_buttonWidth = paramFound.getParameterAsInt();
			paramFound = FLYProcessUtil.getParameter(params, "Button_Height");
			if(paramFound != null)
				m_buttonHeight = paramFound.getParameterAsInt();

		}	
		
	}
	
	public void loadButtons(int categoryType, int ID){
		disposeButtons();
		if(categoryType == FLYControl.PRODUCT_CATEGORIES)
			createButtonsByCategory(ID);
		else if(categoryType == FLYControl.PRODUCT_CLASSIFICATION)
			createButtonsByClassification(ID);
		else if(categoryType == FLYControl.PRODUCT_CLASS)
			createButtonsByClass(ID);
		else if(categoryType == FLYControl.PRODUCT_GROUP)
			createButtonsByGroup(ID);

		
	}
	
	private void createProductButtons(int idx, List<FLYProduct> products ){
		for (FLYProduct item : products) {
			FLYButton button = getButtons().get(idx);
			button.setTag(item);
			//Set Text
			String title = item.getName();
			if(!StringUtil.isNullOrEmpty(item.getTitle()))
				title = item.getTitle();
			SwingUtil.addTextToButton(button, item.getForecolor(), item.getBackcolor(), title, "black", m_fontSize);

			//Set Icon
			if (item.getImage() != null) {
				ImageIcon ico = item.getImage();
				SwingUtil.addImageToButton(button, ico);
			}
			button.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					FLYButton btn = (FLYButton)evt.getSource();
					Object tagObject = btn.getTag();
					if(tagObject instanceof FLYProduct){
						FLYProduct product = (FLYProduct)tagObject; 
						ProcessInfo pi = new ProcessInfo("Add Ticket Line", FLYEnv.instance.ID_ADD_TICKET_LINE);
						pi = FLYEnv.instance.setEnvVars(pi);
						pi.addParameter("C_Order_ID", FLYEnv.instance.getC_Order_ID(), "C_Order_ID");
						pi.addParameter("M_Product_ID", product.getM_Product_ID(), "M_Product_ID");
						
						FLYProcess process = new FLYProcess(pi, "org.compiere.process.AddTicketLine", FLYEnv.instance.ID_ADD_TICKET_LINE);
						FLYProcessList list = new FLYProcessList(true);
						list.add(process);
						pi = list.execute(FLYEnv.instance.getEnvVars());
						
						new FLYProcessResultHandler().handle(Env.getCtx(), pi, null);
						
					}
				}
			
			});
			
			idx++;
			
		}
		
		this.resizeButtons();
		
	}
	
	
	private void createButtonsByCategory(int ID){
		int idx = 0;
		List<FLYProductCategory> categories = FLYEnv.instance.getCache()
				.getProductCategories(new BigDecimal(ID));
		List<FLYProduct> products = new FLYProductList(FLYControl.PRODUCT_CATEGORIES, ID).getProducts();
		int size = categories.size() + products.size(); 
		this.setButtonCount(size);
		this.setButtonWidth(m_buttonWidth);
		this.setButtonHeight(m_buttonHeight);
		
		//create category buttons
		for (FLYProductCategory item : categories) {

			FLYButton button = getButtons().get(idx);
			button.setTag(item);
			
			//Set Text
			String title = item.getName();
			if(!StringUtil.isNullOrEmpty(item.getTitle()))
				title = item.getTitle();
			SwingUtil.addTextToButton(button, item.getForecolor(), item.getBackcolor(), title, "black", m_fontSize);

			//Set Icon
			if (item.getImage() != null) {
				ImageIcon ico = item.getImage();
				SwingUtil.addImageToButton(button, ico);
			}
			button.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					FLYButton btn = (FLYButton)evt.getSource();
					Object tagObject = btn.getTag();
					if(tagObject instanceof FLYProductCategory){
						FLYProductCategory category = (FLYProductCategory)tagObject;
						FLYSwingController.instance.categoryChanged(category);
						
					}
				}
			
			});
			
			idx++;
		}
		createProductButtons(idx, products);
		revalidate();
		repaint();
	}

	private void createButtonsByClassification(int ID){
		int idx = 0;
		List<FLYProductClassification> classifications = FLYEnv.instance.getCache()
				.getProductClassifications(new BigDecimal(ID));
		List<FLYProduct> products = new FLYProductList(FLYControl.PRODUCT_CLASSIFICATION, ID).getProducts();
		this.setButtonCount(classifications.size() + products.size());
		this.setButtonWidth(m_buttonWidth);
		this.setButtonHeight(m_buttonHeight);
		
		//create category buttons
		for (FLYProductClassification item : classifications) {

			FLYButton button = getButtons().get(idx);
			button.setTag(item);
			
			//Set Text
			String title = item.getName();
			if(!StringUtil.isNullOrEmpty(item.getTitle()))
				title = item.getTitle();
			SwingUtil.addTextToButton(button, item.getForecolor(), item.getBackcolor(), title, "black", m_fontSize);

			//Set Icon
			if (item.getImage() != null) {
				ImageIcon ico = item.getImage();
				SwingUtil.addImageToButton(button, ico);
			}
			button.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					FLYButton btn = (FLYButton)evt.getSource();
					Object tagObject = btn.getTag();
					if(tagObject instanceof FLYProductClassification){
						FLYProductClassification classification = (FLYProductClassification)tagObject;
						FLYSwingController.instance.classificationChanged(classification);
						
					}
				}
			
			});
			
			idx++;
		}
		createProductButtons(idx, products);

	}

	private void createButtonsByClass(int ID){
		int idx = 0;
		List<FLYProductClass> classes = FLYEnv.instance.getCache()
				.getProductClasses(new BigDecimal(ID));
		List<FLYProduct> products = new FLYProductList(FLYControl.PRODUCT_CLASS, ID).getProducts();
		this.setButtonCount(classes.size() + products.size());
		this.setButtonWidth(m_buttonWidth);
		this.setButtonHeight(m_buttonHeight);
		
		//create category buttons
		for (FLYProductClass item : classes) {

			FLYButton button = getButtons().get(idx);
			button.setTag(item);
			//Set Text
			String title = item.getName();
			if(!StringUtil.isNullOrEmpty(item.getTitle()))
				title = item.getTitle();
			SwingUtil.addTextToButton(button, item.getForecolor(), item.getBackcolor(), title, "black", m_fontSize);

			//Set Icon
			if (item.getImage() != null) {
				ImageIcon ico = item.getImage();
				SwingUtil.addImageToButton(button, ico);
			}
			button.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					FLYButton btn = (FLYButton)evt.getSource();
					Object tagObject = btn.getTag();
					if(tagObject instanceof FLYProductClass){
						FLYProductClass classi = (FLYProductClass)tagObject;
						FLYSwingController.instance.classChanged(classi);
						
					}
				}
			
			});
			
			idx++;
		}
		createProductButtons(idx, products);

	}

	private void createButtonsByGroup(int ID){
		int idx = 0;
		List<FLYProductGroup> groups = FLYEnv.instance.getCache()
				.getProductGroups(new BigDecimal(ID));
		List<FLYProduct> products = new FLYProductList(FLYControl.PRODUCT_GROUP, ID).getProducts();
		this.setButtonCount(groups.size() + products.size());
		this.setButtonWidth(m_buttonWidth);
		this.setButtonHeight(m_buttonHeight);
		
		//create category buttons
		for (FLYProductGroup item : groups) {

			FLYButton button = getButtons().get(idx);
			button.setTag(item);
			//Set Text
			String title = item.getName();
			if(!StringUtil.isNullOrEmpty(item.getTitle()))
				title = item.getTitle();
			SwingUtil.addTextToButton(button, item.getForecolor(), item.getBackcolor(), title, "black", m_fontSize);

			//Set Icon
			if (item.getImage() != null) {
				ImageIcon ico = item.getImage();
				SwingUtil.addImageToButton(button, ico);
			}
			button.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					FLYButton btn = (FLYButton)evt.getSource();
					Object tagObject = btn.getTag();
					if(tagObject instanceof FLYProductGroup){
						FLYProductGroup group = (FLYProductGroup)tagObject;
						FLYSwingController.instance.groupChanged(group);
						
					}
				}
			
			});
			
			idx++;
		}
		createProductButtons(idx, products);

	}

}
