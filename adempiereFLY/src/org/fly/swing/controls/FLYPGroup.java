/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing.controls;

import java.math.BigDecimal;
import java.util.List;

import javax.swing.ImageIcon;

import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.fly.core.FLYEnv;
import org.fly.core.classes.FLYProductGroup;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.StringUtil;
import org.fly.swing.FLYSwingController;
import org.fly.swing.base.FLYButton;
import org.fly.swing.controls.base.FLYBaseButtonGroup;
import org.fly.swing.utils.SwingUtil;

public class FLYPGroup extends FLYBaseButtonGroup{

	private static final long serialVersionUID = 322147562388659445L;
	
	private int m_buttonWidth = 100;
	private int m_buttonHeight = 80;
	private int m_fontSize = 10;
	private BigDecimal m_M_Product_Group_ID = null;

	public FLYPGroup(byte[] setup){
		
		super();
		if(setup != null && setup.length > 0){
			ProcessInfo pInfo = ByteArrayUtil.byteArrayToProcessInfo(setup);
			ProcessInfoParameter[] params = pInfo.getParameter();
			ProcessInfoParameter paramFound = FLYProcessUtil.getParameter(params, "Button_Width");
			if(paramFound != null)
				m_buttonWidth = paramFound.getParameterAsInt();
			paramFound = FLYProcessUtil.getParameter(params, "Button_Height");
			if(paramFound != null)
				m_buttonHeight = paramFound.getParameterAsInt();
			paramFound = FLYProcessUtil.getParameter(params, "M_Product_Group_ID");
			if(paramFound != null){
				m_M_Product_Group_ID = new BigDecimal(paramFound.getParameterAsInt());
			}
		}
		loadButtons();

	}
	
	private void loadButtons(){
		disposeButtons();
		List<FLYProductGroup> list = FLYEnv.instance.getCache().getProductGroups(m_M_Product_Group_ID);
		if(list == null)
			return;
		this.setButtonCount(list.size());
		this.setButtonWidth(m_buttonWidth);
		this.setButtonHeight(m_buttonHeight);
		int idx = 0;
		for (FLYProductGroup item : list) {

			FLYButton button = getButtons().get(idx);
			button.setTag(item);
			
			//Set Text
			String title = item.getName();
			if(!StringUtil.isNullOrEmpty(item.getTitle()))
				title = item.getTitle();
			SwingUtil.addTextToButton(button, item.getForecolor(), item.getBackcolor(), title, "black", m_fontSize);

			//Set Icon
			if (item.getImage() != null) {
				ImageIcon ico = item.getImage();
				SwingUtil.addImageToButton(button, ico);
			}
			button.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					FLYButton btn = (FLYButton)evt.getSource();
					Object tagObject = btn.getTag();
					if(tagObject instanceof FLYProductGroup){
						FLYProductGroup group = (FLYProductGroup)tagObject;
						FLYSwingController.instance.groupChanged(group);
					}
				}
			});
			idx++;
		}
	}

	public BigDecimal getM_M_Product_Group_ID() {
		return m_M_Product_Group_ID;
	}

	public void setM_M_Product_Group_ID(BigDecimal m_M_Product_Group_ID) {
		this.m_M_Product_Group_ID = m_M_Product_Group_ID;
	}

}
