package org.fly.swing.dialogs;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import org.compiere.process.ProcessInfo;
import org.fly.core.utils.FLYMath;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.Pair;
import org.fly.core.utils.StringUtil;
import org.fly.swing.base.FLYButton;
import org.fly.swing.controls.base.FLYBaseButtonGroup;

/**
 *
 * @author tasos
 */
public class PaymentChangesPanel extends javax.swing.JPanel implements
		ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4643735079122451641L;

	private BigDecimal m_total = new BigDecimal(0);


	private BigDecimal m_cash = new BigDecimal(0);
	private BigDecimal m_changes = new BigDecimal(0);

	private PaymentChangesDialog m_dialog;
	ProcessInfo m_pi;

	/**
	 * Creates new form PaymentChangesPanel
	 */
	public PaymentChangesPanel(PaymentChangesDialog aDialog) {
		
		m_dialog = aDialog;
		initComponents();
		m_pi = m_dialog.getM_pi();
		Object total = FLYProcessUtil.getParameterValue(m_pi, "Total");
		if (total != null) {
			m_total = (BigDecimal) total;
			calc();
		}
		if(m_total != null)
			tTotal.setText(FLYMath.bigDecimalToString(m_total));

	}

	public BigDecimal getCash() {
		return m_cash;
	}

	public void setCash(BigDecimal cash) {
		m_cash = cash;
	}

	public BigDecimal getChanges() {
		return m_changes;
	}

	public void setChanges(BigDecimal m_changes) {
		this.m_changes = m_changes;
	}

	public BigDecimal getTotal() {
		return m_total;
	}


	private void initComponents() {
		int fontSize = 14;
		jPanel1 = new javax.swing.JPanel();
		pHeader = new javax.swing.JPanel();
		lTotal = new javax.swing.JLabel();
		tCash = new javax.swing.JTextField();
		lCash = new javax.swing.JLabel();
		tTotal = new javax.swing.JTextField();
		lChanges = new javax.swing.JLabel();
		tChanges = new javax.swing.JTextField();
		pCalculator = new javax.swing.JPanel();

		pBanknotesBG = new FLYBaseButtonGroup();
		pCoinsBG = new FLYBaseButtonGroup();

		b1 = new FLYButton();
		b1.setTag(new Pair<String, Object>("PadNumber", "1"));
		b1.addActionListener(this);
		b2 = new FLYButton();
		b2.setTag(new Pair<String, Object>("PadNumber", "2"));
		b2.addActionListener(this);
		b3 = new FLYButton();
		b3.setTag(new Pair<String, Object>("PadNumber", "3"));
		b3.addActionListener(this);
		b4 = new FLYButton();
		b4.setTag(new Pair<String, Object>("PadNumber", "4"));
		b4.addActionListener(this);
		b5 = new FLYButton();
		b5.setTag(new Pair<String, Object>("PadNumber", "5"));
		b5.addActionListener(this);
		b6 = new FLYButton();
		b6.setTag(new Pair<String, Object>("PadNumber", "6"));
		b6.addActionListener(this);
		b7 = new FLYButton();
		b7.setTag(new Pair<String, Object>("PadNumber", "7"));
		b7.addActionListener(this);
		b8 = new FLYButton();
		b8.setTag(new Pair<String, Object>("PadNumber", "8"));
		b8.addActionListener(this);
		b9 = new FLYButton();
		b9.setTag(new Pair<String, Object>("PadNumber", "9"));
		b9.addActionListener(this);
		bPlus = new FLYButton();
		b0 = new FLYButton();
		b0.setTag(new Pair<String, Object>("PadNumber", "0"));
		b0.addActionListener(this);
		bComma = new FLYButton();
		bComma.setTag(new Pair<String, Object>("PadNumber", FLYMath
				.getDecimalSeparator()));
		bComma.addActionListener(this);
		bBack = new FLYButton();
		bMinus = new FLYButton();
		bClear = new FLYButton();
		bClear.setTag(new Pair<String, Object>("PadClear", ""));
		bClear.addActionListener(this);
		bEnter = new FLYButton();
		pCoins = new javax.swing.JPanel();

		lTotal.setText("Total");

		tCash.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
		tCash.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		tCash.setText("");

		lCash.setText("Cash");

		tTotal.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
		tTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		tTotal.setText("");
		
		lChanges.setText("Changes");

		tChanges.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
		tChanges.setHorizontalAlignment(javax.swing.JTextField.CENTER);
		tChanges.setText("");

		javax.swing.GroupLayout pHeaderLayout = new javax.swing.GroupLayout(
				pHeader);
		pHeader.setLayout(pHeaderLayout);
		pHeaderLayout
				.setHorizontalGroup(pHeaderLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								pHeaderLayout
										.createSequentialGroup()
										.addComponent(lTotal)
										.addGap(252, 252, 252)
										.addComponent(lCash)
										.addGap(32, 32, 32)
										.addComponent(
												tCash,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												124,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(84, 84, 84)
										.addComponent(lChanges)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												tChanges,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												124,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(0, 0, Short.MAX_VALUE))
						.addGroup(
								pHeaderLayout
										.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												pHeaderLayout
														.createSequentialGroup()
														.addGap(64, 64, 64)
														.addComponent(
																tTotal,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																124,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addContainerGap(686,
																Short.MAX_VALUE))));
		pHeaderLayout
				.setVerticalGroup(pHeaderLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								pHeaderLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												pHeaderLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(lTotal)
														.addComponent(lCash)
														.addComponent(lChanges))
										.addContainerGap(18, Short.MAX_VALUE))
						.addComponent(tChanges,
								javax.swing.GroupLayout.Alignment.TRAILING)
						.addComponent(tCash,
								javax.swing.GroupLayout.Alignment.TRAILING)
						.addGroup(
								pHeaderLayout
										.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(
												tTotal,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												45, Short.MAX_VALUE)));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				pHeader, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				pHeader, javax.swing.GroupLayout.PREFERRED_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.PREFERRED_SIZE));
		b1.setText(StringUtil.stringToHtml("1", "blue", fontSize));

		b2.setText(StringUtil.stringToHtml("2", "blue", fontSize));

		b3.setText(StringUtil.stringToHtml("3", "blue", fontSize));

		b4.setText(StringUtil.stringToHtml("4", "blue", fontSize));

		b5.setText(StringUtil.stringToHtml("5", "blue", fontSize));

		b6.setText(StringUtil.stringToHtml("6", "blue", fontSize));

		b7.setText(StringUtil.stringToHtml("7", "blue", fontSize));

		b8.setText(StringUtil.stringToHtml("8", "blue", fontSize));

		b9.setText(StringUtil.stringToHtml("9", "blue", fontSize));

		b0.setText(StringUtil.stringToHtml("0", "blue", fontSize));

		bPlus.setText(StringUtil.stringToHtml("*", "red", fontSize));

		bComma.setText(StringUtil.stringToHtml(FLYMath.getDecimalSeparator(),
				"red", fontSize));

		bBack.setText(StringUtil.stringToHtml("B", "blue", fontSize));

		bMinus.setText(StringUtil.stringToHtml("-", "red", fontSize));

		bClear.setText(StringUtil.stringToHtml("C", "red", fontSize));

		bEnter.setText(StringUtil.stringToHtml("Entr", "blue", fontSize));

		javax.swing.GroupLayout pCalculatorLayout = new javax.swing.GroupLayout(
				pCalculator);
		pCalculator.setLayout(pCalculatorLayout);
		pCalculatorLayout
				.setHorizontalGroup(pCalculatorLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								pCalculatorLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												pCalculatorLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																pCalculatorLayout
																		.createSequentialGroup()
																		.addComponent(
																				b1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				b2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		// .addGap(8,
																		// 8, 8)
																		.addComponent(
																				b3,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		// /
																		// .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		// .addComponent(bBack,
																		// javax.swing.GroupLayout.DEFAULT_SIZE,
																		// javax.swing.GroupLayout.DEFAULT_SIZE,
																		// Short.MAX_VALUE))
																		.addComponent(
																				bBack,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																pCalculatorLayout
																		.createSequentialGroup()
																		.addComponent(
																				bPlus,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				b0,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				bComma,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				bEnter,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																pCalculatorLayout
																		.createSequentialGroup()
																		.addComponent(
																				b4,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				b5,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				b6,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				bMinus,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																pCalculatorLayout
																		.createSequentialGroup()
																		.addComponent(
																				b7,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				b8,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				b9,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				bClear,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				67,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap()));
		pCalculatorLayout
				.setVerticalGroup(pCalculatorLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								pCalculatorLayout
										.createSequentialGroup()
										.addGap(22, 22, 22)
										.addGroup(
												pCalculatorLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addGroup(
																pCalculatorLayout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(
																				b2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				63,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				b3,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				63,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				b1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				63,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addComponent(
																bBack,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																62,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												pCalculatorLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																b5,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																b6,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																b4,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																bMinus,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																62,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												pCalculatorLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																b8,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																b9,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																b7,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																bClear,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																62,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												pCalculatorLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																b0,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																bComma,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																bPlus,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																63,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																bEnter,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																62,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(43, Short.MAX_VALUE)));

		javax.swing.GroupLayout pCoinsLayout = new javax.swing.GroupLayout(
				pCoins);
		pCoins.setLayout(pCoinsLayout);
		pCoinsLayout.setHorizontalGroup(pCoinsLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 0,
				Short.MAX_VALUE));
		pCoinsLayout.setVerticalGroup(pCoinsLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 335,
				Short.MAX_VALUE));

		javax.swing.GroupLayout pBanknotesBGLayout = new javax.swing.GroupLayout(
				pBanknotesBG);
		pBanknotesBG.setLayout(pBanknotesBGLayout);
		pBanknotesBGLayout.setHorizontalGroup(pBanknotesBGLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 555, Short.MAX_VALUE));
		pBanknotesBGLayout.setVerticalGroup(pBanknotesBGLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, /* 158 */300, Short.MAX_VALUE));

		javax.swing.GroupLayout pCoinsBGLayout = new javax.swing.GroupLayout(
				pCoinsBG);
		pCoinsBG.setLayout(pCoinsBGLayout);
		pCoinsBGLayout.setHorizontalGroup(pCoinsBGLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 0,
				Short.MAX_VALUE));
		pCoinsBGLayout.setVerticalGroup(pCoinsBGLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 0,
				Short.MAX_VALUE));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.TRAILING)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		pCalculator,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING,
																				true)
																				.addComponent(
																						pBanknotesBG,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						pCoinsBG,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE))
																.addGap(0,
																		0,
																		Short.MAX_VALUE))
												.addComponent(
														jPanel1,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(pCoins,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addComponent(jPanel1,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(
														pCalculator,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addComponent(
														pCoins,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		pBanknotesBG,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(
																		pCoinsBG,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE)))
								.addContainerGap()));
	}// </editor-fold>

	public void actionPerformed(ActionEvent e) {
		FLYButton btn = (FLYButton) e.getSource();
		Object tagObject = btn.getTag();
		if (tagObject instanceof Pair<?, ?>) {
			@SuppressWarnings("unchecked")
			Pair<String, String> pair = (Pair<String, String>) tagObject;
			String key = pair.getKey();
			String value = pair.getValue();
			if (key.equals("PadNumber")) {
				tCash.setText(tCash.getText() + value);
				String sCash = tCash.getText();
				m_cash = FLYMath.stringToBigDecimal(sCash);
				calc();
			} else if (key.equals("PadClear")) {
				zeroCash();
				calc();
			}
		}

	}

	public void calc() {
		m_changes = m_cash.subtract(m_total);
		tCash.setText(FLYMath.bigDecimalToString(m_cash));
		if(m_changes.floatValue() > 0)
			tChanges.setText(FLYMath.bigDecimalToString(m_changes));
		else
			tChanges.setText("");
	}

	private void zeroCash() {
		tCash.setText("");
		m_cash = new BigDecimal(0);
	}

	public FLYBaseButtonGroup getBanknotesBG() {
		return pBanknotesBG;
	}

	public FLYBaseButtonGroup getCoinsBG() {
		return pCoinsBG;
	}

	// Variables declaration - do not modify
	private FLYButton b0;
	private FLYButton b1;
	private FLYButton b2;
	private FLYButton b3;
	private FLYButton b4;
	private FLYButton b5;
	private FLYButton b6;
	private FLYButton b7;
	private FLYButton b8;
	private FLYButton b9;
	private FLYButton bBack;
	private FLYButton bClear;
	private FLYButton bComma;
	private FLYButton bEnter;
	private FLYButton bMinus;
	private FLYButton bPlus;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JLabel lCash;
	private javax.swing.JLabel lChanges;
	private javax.swing.JLabel lTotal;
	private javax.swing.JPanel pCalculator;
	private javax.swing.JPanel pCoins;
	private javax.swing.JPanel pHeader;
	private javax.swing.JTextField tCash;
	private javax.swing.JTextField tChanges;
	private javax.swing.JTextField tTotal;
	private FLYBaseButtonGroup pBanknotesBG;
	private FLYBaseButtonGroup pCoinsBG;

	// End of variables declaration
}
