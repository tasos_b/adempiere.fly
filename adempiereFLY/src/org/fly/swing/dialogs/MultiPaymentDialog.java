package org.fly.swing.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.util.Properties;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import net.miginfocom.swing.MigLayout;

import org.compiere.minigrid.MiniTable;
import org.compiere.process.ProcessInfo;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.base.FLYBaseDialog;
import org.fly.swing.base.FLYButton;


public class MultiPaymentDialog  extends FLYBaseDialog {
	
	private MultiPaymentPanel paymentPanel = new MultiPaymentPanel();
	private MiniTable m_table = new MiniTable();
	private JScrollPane m_scrollPane = new JScrollPane();
	private int m_scrollWidth = 30;

	

	public MultiPaymentDialog(Frame owner, String title, boolean modal,
			ProcessInfo pi, Properties ctx) throws HeadlessException {
		super(owner, title, modal, pi, ctx);
		setControls();
	}
	
	public MultiPaymentDialog(String title, ProcessInfo pi, Properties ctx)
			throws HeadlessException {
		this(FLYMainFrame.instance.getFrame(), title, true, pi, ctx);
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = -3987077924682529827L;

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	public MiniTable getTable(){
		return m_table;
	}
	private void setControls() {

		setButtons(true, false, false, false, false, false, false);

		getPanel().add(paymentPanel, BorderLayout.CENTER);
		
		paymentPanel.pGrid.setLayout(new MigLayout("fill, ins 0 0"));
		paymentPanel.pGrid.add(m_scrollPane , "growx, spanx, growy, pushy, h 200:300:");
		m_scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		m_scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		m_scrollPane.getViewport().add(m_table, null);
		m_scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(m_scrollWidth, 0));
		m_scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(0,m_scrollWidth));
		getTable().setRowSelectionAllowed(true);
		getTable().setColumnSelectionAllowed(false);
		getTable().setMultiSelection(false);
		getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getTable().setRowHeight(30);
		getTable().setAutoResize(false);

		fillPaymentMethods();

	}

	private void fillPaymentMethods() {
		paymentPanel.pPaymentMethods.setButtonWidth(150);
		paymentPanel.pPaymentMethods.setButtonHeight(80);
		paymentPanel.pPaymentMethods.setButtonCount(3);
		FLYButton cashButton = paymentPanel.pPaymentMethods.getButtons().get(0);
		cashButton.setText("Cash");
		FLYButton creditCardButton = paymentPanel.pPaymentMethods.getButtons().get(1);
		creditCardButton.setText("Credit Card");
		FLYButton checkButton = paymentPanel.pPaymentMethods.getButtons().get(2);
		checkButton.setText("Check");
		
	}


}
