package org.fly.swing.dialogs;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.ImageIcon;

import org.adempiere.exceptions.DBException;
import org.compiere.apps.FLYConfirmPanel;
import org.compiere.process.CashPayoffProcess;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.fly.core.FLYEnv;
import org.fly.core.FLYProcessResultHandler;
import org.fly.core.classes.FLYBanknote;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.sql.SQLBankNotesAndCoins;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.base.FLYBaseDialog;
import org.fly.swing.base.FLYButton;
import org.fly.swing.controls.base.FLYBaseButtonGroup;

public class PaymentChangesDialog extends FLYBaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2934831990002272699L;

	private PaymentChangesPanel changesPanel = new PaymentChangesPanel(this);

	private FLYBaseButtonGroup pBanknotesBG;
	private FLYBaseButtonGroup pCoinsBG;
	private int C_Currency_ID;

	private int coinWidth = 75;
	private int banknoteWidth = 170;
	private int banknoteHeight = 80;

	public PaymentChangesDialog(Frame owner, String title, boolean modal,
			ProcessInfo pi, Properties ctx) throws HeadlessException {
		super(owner, title, modal, pi, ctx);
		Object tmp = FLYProcessUtil.getParameterValue(pi, "C_Currency_ID");
		if (tmp != null)
			C_Currency_ID = (int) tmp;
		setControls();

	}

	public PaymentChangesDialog(String title, ProcessInfo pi, Properties ctx)
			throws HeadlessException {
		this(FLYMainFrame.instance.getFrame(), title, true, pi, ctx);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		BigDecimal total = changesPanel.getTotal();
		BigDecimal cash = changesPanel.getCash();
		if (cash == null || (cash.doubleValue() < total.doubleValue()))
			return;
		if (e.getActionCommand().equals(FLYConfirmPanel.A_CANCEL))
			dispose();
		else if (e.getActionCommand().equals(FLYConfirmPanel.A_OK)) {
			boolean toPayCash = false;
			ProcessInfo pi = getM_pi();
			ProcessInfoParameter paramFound = FLYProcessUtil.getParameter(pi,
					"DoCashPayoff");
			if (paramFound != null)
				toPayCash = paramFound.getParameterAsBoolean();
			if (toPayCash) {
				new CashPayoffProcess().startProcess(getM_ctx(), pi, null);
				new FLYProcessResultHandler().handle(getM_ctx(), getM_pi(),
						null);
			}
			dispose();
		}

	}

	private void setControls() {

		setButtons(true, false, false, false, false, false, false);

		getPanel().add(changesPanel, BorderLayout.CENTER);

		pBanknotesBG = changesPanel.getBanknotesBG();
		pCoinsBG = changesPanel.getCoinsBG();

		createBankNotesAndCoins();
	}

	private void createBankNotesAndCoins() {
		ArrayList<FLYBanknote> banknotes = FLYEnv.instance.getCache()
				.getBanknotes(C_Currency_ID);
		ArrayList<FLYBanknote> coins = FLYEnv.instance.getCache().getCoins(
				C_Currency_ID);

		if (banknotes == null) {
			banknotes = new ArrayList<FLYBanknote>();
			String sql = SQLBankNotesAndCoins.GET_BANKNOTES;
			SQLUtil util = new SQLUtil(sql, null, C_Currency_ID);

			try {
				assignBanknotes(util.getResultSet(), banknotes);
			} catch (SQLException e) {
				throw new DBException(e, sql);
			} finally {
				util.close();
			}
			FLYEnv.instance.getCache().setBanknotes(C_Currency_ID, banknotes);

		}
		if (coins == null) {
			coins = new ArrayList<FLYBanknote>();
			String sql = SQLBankNotesAndCoins.GET_COINS;
			SQLUtil util = new SQLUtil(sql, null, C_Currency_ID);

			try {
				assignCoins(util.getResultSet(), coins);
			} catch (SQLException e) {
				throw new DBException(e, sql);
			} finally {
				util.close();
			}
			FLYEnv.instance.getCache().setCoins(C_Currency_ID, coins);
		}
		pBanknotesBG.setButtonWidth(banknoteWidth);
		pBanknotesBG.setButtonHeight(banknoteHeight);
		pCoinsBG.setButtonWidth(coinWidth);
		pCoinsBG.setButtonHeight(coinWidth);

		pBanknotesBG.setButtonCount(banknotes.size());
		pCoinsBG.setButtonCount(coins.size());

		for (int i = 0; i < banknotes.size(); i++) {
			FLYButton button = pBanknotesBG.getButtons().get(i);
			FLYBanknote banknote = banknotes.get(i);
			button.setTag(banknote);
			byte[] binarydata = banknote.getBinarydata();
			if (binarydata != null && binarydata.length > 0) {
				BufferedImage bi = ByteArrayUtil.byteArrayToIcon(binarydata);
				Image icon = bi.getScaledInstance(-1, button.getHeight(),
						Image.SCALE_DEFAULT);
				button.setIcon(new ImageIcon(icon));
			} else {
				button.setText(banknote.getMoneyvalue().toPlainString());
			}
			button.addActionListener(new java.awt.event.ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					FLYButton button = (FLYButton) e.getSource();
					FLYBanknote banknote = (FLYBanknote) button.getTag();
					BigDecimal value = banknote.getMoneyvalue();
					changesPanel.setCash(changesPanel.getCash().add(value));
					changesPanel.calc();
				}
			});
		}

		for (int i = 0; i < coins.size(); i++) {
			FLYButton button = pCoinsBG.getButtons().get(i);
			FLYBanknote banknote = coins.get(i);
			button.setTag(banknote);
			byte[] binarydata = banknote.getBinarydata();
			if (binarydata != null && binarydata.length > 0) {
				BufferedImage bi = ByteArrayUtil.byteArrayToIcon(binarydata);
				Image icon = bi.getScaledInstance(-1, button.getHeight(),
						Image.SCALE_DEFAULT);
				button.setIcon(new ImageIcon(icon));
			} else {
				button.setText(banknote.getMoneyvalue().toPlainString());
			}
			button.addActionListener(new java.awt.event.ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					FLYButton button = (FLYButton) e.getSource();
					FLYBanknote banknote = (FLYBanknote) button.getTag();
					BigDecimal value = banknote.getMoneyvalue();
					changesPanel.setCash(changesPanel.getCash().add(value));
					changesPanel.calc();
				}
			});
		}

	}

	private void assignCoins(ResultSet resultSet, ArrayList<FLYBanknote> coins)
			throws SQLException {
		while (resultSet.next()) {
			FLYBanknote banknote = new FLYBanknote();
			banknote.setName(resultSet.getString("name"));
			banknote.setMoneyvalue(resultSet.getBigDecimal("moneyvalue"));
			byte[] binarydata = resultSet.getBytes("binarydata");
			if (binarydata != null && binarydata.length > 0) {
				banknote.setBinarydata(binarydata);
			}
			coins.add(banknote);
		}
	}

	private void assignBanknotes(ResultSet resultSet,
			ArrayList<FLYBanknote> banknotes) throws SQLException {
		while (resultSet.next()) {
			FLYBanknote banknote = new FLYBanknote();
			banknote.setName(resultSet.getString("name"));
			banknote.setMoneyvalue(resultSet.getBigDecimal("moneyvalue"));
			byte[] binarydata = resultSet.getBytes("binarydata");
			if (binarydata != null && binarydata.length > 0) {
				banknote.setBinarydata(binarydata);
			}
			banknotes.add(banknote);

		}

	}
}
