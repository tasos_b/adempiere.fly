package org.fly.swing.dialogs;

import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.KeyStroke;

import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MWarehousePrice;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.swing.CLabel;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.base.FLYBaseGridDialog;
import org.fly.swing.base.FLYTextField;

public class SelectProductDialog extends FLYBaseGridDialog {

	private static final long serialVersionUID = 1L;

	private CLabel lSearch = new CLabel("SearchKey");
	private FLYTextField fSearch = new FLYTextField();

	private CLabel lEAN = new CLabel("UPC/EAN");
	private FLYTextField fEAN = new FLYTextField();

	private CLabel lName = new CLabel("Name");
	private FLYTextField fName = new FLYTextField();

	private CLabel lSKU = new CLabel("SKU");
	private FLYTextField fSKU = new FLYTextField();

	/** From Clause */
	private static String s_sqlFrom = "RV_WarehousePrice";
	/** Where Clause */
	private static String s_sqlWhere = "IsActive='Y'";

	private int M_PriceList_Version_ID;

	private int M_Warehouse_ID;

	public SelectProductDialog(Frame owner, String title, boolean modal,
			ProcessInfo pi, Properties ctx) throws HeadlessException {
		super(owner, title, modal, pi, ctx);
		setControls();

	}

	public SelectProductDialog(String title, ProcessInfo pi, Properties ctx)
			throws HeadlessException {
		this(FLYMainFrame.instance.getFrame(), title, true, pi, ctx);
	}

	public void setControls() {

		setButtons(true, true, true, false, false, false, false);

		JButton previous = createButtonAction("Previous", KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0));
		getConfirmPanel().addButton(previous);
		JButton next = createButtonAction("Next", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0));
		getConfirmPanel().addButton(next);

		addPairControl(lSearch, labelDimension, fSearch, ComponentDimension);
		addPairControl(lEAN, labelDimension, fEAN, ComponentDimension);
		addPairControl(lName, labelDimension, fName, ComponentDimension);
		addPairControl(lSKU, labelDimension, fSKU, ComponentDimension);

		getTable().prepareTable(s_layout, s_sqlFrom, s_sqlWhere, false,
				"RV_WarehousePrice");
		getTable().addMouseListener(this);
		getTable().setColumnVisibility(getTable().getColumn(0), false);
		getTable().getColumn(1).setPreferredWidth(175);
		getTable().getColumn(2).setPreferredWidth(175);
		getTable().getColumn(3).setPreferredWidth(100);
		getTable().getColumn(4).setPreferredWidth(75);
		getTable().getColumn(5).setPreferredWidth(75);
		getTable().getColumn(6).setPreferredWidth(75);
		getTable().getColumn(7).setPreferredWidth(75);
		getTable().setFillsViewportHeight(true); 

		refreshScrollBars();
	}

	/** Table Column Layout Info */
	private static ColumnInfo[] s_layout = new ColumnInfo[] {
			new ColumnInfo(" ", "M_Product_ID", IDColumn.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Value"), "Value",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "Name",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "UPC"), "UPC",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "SKU"), "SKU",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "QtyAvailable"),
					"QtyAvailable", Double.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "QtyOnHand"),
					"QtyOnHand", Double.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "PriceStd"), "PriceStd",
					BigDecimal.class) };

	public void setM_pi(ProcessInfo m_pi) {
		super.setM_pi(m_pi);
		ProcessInfoParameter[] params = m_pi.getParameter();
		ProcessInfoParameter param = FLYProcessUtil.getParameter(params,
				"M_PriceList_Version_ID");
		if (param != null)
			M_PriceList_Version_ID = param.getParameterAsInt();
		param = FLYProcessUtil.getParameter(params, "M_Warehouse_ID");
		if (param != null)
			M_Warehouse_ID = param.getParameterAsInt();

	}

	@Override
	public void doSelection(Integer ID) {
		if (ID != null) {
			int M_Product_ID = ID.intValue();
			FLYProcessUtil.setParameter(getM_pi(), "M_Product_ID", M_Product_ID);
			if (getHandler() != null)
				getHandler().handle(getM_ctx(), getM_pi(), null);
			dispose();

		}
	}
	
	@Override
	public void handleRefresh() {
		setResults(MWarehousePrice.find(getM_ctx(), M_PriceList_Version_ID,
				M_Warehouse_ID, fSearch.getText(), fName.getText(),
				fEAN.getText(), fSKU.getText(), null));
	}

	@Override
	public void handleReset() {
		fEAN.setText(null);
		fName.setText(null);
		fSKU.setText(null);
		fSearch.setText(null);
		setResults(new MWarehousePrice[0]);
	}
}
