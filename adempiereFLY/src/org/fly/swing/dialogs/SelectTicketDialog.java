package org.fly.swing.dialogs;

import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.KeyStroke;

import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.Mticket;
import org.compiere.process.ProcessInfo;
import org.compiere.swing.CLabel;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.core.utils.StringUtil;
import org.fly.swing.FLYDialog;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.base.FLYBaseGridDialog;
import org.fly.swing.base.FLYTextField;
import org.fly.swing.base.FLYVDate;

public class SelectTicketDialog extends FLYBaseGridDialog {

	// fl_ticket_order_infos_v
	private static String s_sqlFrom = "fl_ticket_order_infos_v";
	private static String s_sqlWhere = "IsActive='Y'";

	private static final long serialVersionUID = 3107472035979915450L;

	private CLabel l_dateFrom = new CLabel("Date from");
	private FLYVDate f_dateFrom = new FLYVDate();
	private CLabel l_dateTo = new CLabel("Date to");
	private FLYVDate f_dateTo = new FLYVDate();
	private CLabel l_documentno = new CLabel("Document No");
	private FLYTextField f_documentno = new FLYTextField();

	public SelectTicketDialog(Frame owner, String title, boolean modal,
			ProcessInfo pi, Properties ctx) throws HeadlessException {
		super(owner, title, modal, pi, ctx);
		setControls();

	}

	public SelectTicketDialog(String title, ProcessInfo pi, Properties ctx)
			throws HeadlessException {
		this(FLYMainFrame.instance.getFrame(), title, true, pi, ctx);
	}

	private static ColumnInfo[] s_layout = new ColumnInfo[] {

			new ColumnInfo(" ", "fl_ticket_id", IDColumn.class),
			new ColumnInfo(" ", "C_BPartner_ID", Integer.class),
			new ColumnInfo(" ", "C_Order_ID", Integer.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "DocumentNo"),
					"DocumentNo", String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "C_BPartner_ID"),
					"Name", String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "DateOrdered"),
					"DateOrdered", Timestamp.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "SubTotal"), "Amount",
					Double.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "TaxAmt"), "TaxAmt",
					Double.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "GrandTotal"),
					"GrandTotal", Double.class) };

	private void setControls() {

		setButtons(true, true, true, false, false, false, false);
		JButton previous = createButtonAction("Previous", KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0));
		getConfirmPanel().addButton(previous);
		JButton next = createButtonAction("Next", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0));
		getConfirmPanel().addButton(next);
		JButton delete = createButtonAction("Delete", KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		getConfirmPanel().addButton(delete);

		
		addPairControl(l_dateFrom, labelDimension, f_dateFrom,
				ComponentDimension);
		addPairControl(l_dateTo, labelDimension, f_dateTo, ComponentDimension);
		addPairControl(l_documentno, labelDimension, f_documentno,
				ComponentDimension);

		f_dateFrom.setValue(Env.getContextAsDate(Env.getCtx(), "#Date"));
		f_dateTo.setValue(Env.getContextAsDate(Env.getCtx(), "#Date"));

		getTable().prepareTable(s_layout, s_sqlFrom, s_sqlWhere, false,
				s_sqlFrom);

		getTable().addMouseListener(this);
		getTable().setColumnVisibility(getTable().getColumn(0), false);
		getTable().setColumnVisibility(getTable().getColumn(1), false);
		getTable().setColumnVisibility(getTable().getColumn(2), false);
		getTable().getColumn(3).setPreferredWidth(130);
		getTable().getColumn(4).setPreferredWidth(200);
		getTable().getColumn(5).setPreferredWidth(100);
		getTable().getColumn(6).setPreferredWidth(100);
		getTable().getColumn(7).setPreferredWidth(100);
		getTable().getColumn(8).setPreferredWidth(100);
		getTable().setFillsViewportHeight(true);
		refreshScrollBars();
		handleRefresh();
	}

	private void doResults(Timestamp dateOrderedFrom, Timestamp dateOrderedTo,
			String documentNo, String isClosed) {

		ArrayList<Object> prms = new ArrayList<Object>();

		StringBuffer clause = new StringBuffer("SELECT ");
		clause.append("fl_ticket_id ");
		clause.append(",C_BPartner_ID ");
		clause.append(",C_Order_ID ");
		clause.append(",DocumentNo ");
		clause.append(",Name ");
		clause.append(",DateOrdered ");
		clause.append(",Amount ");
		clause.append(",taxamount ");
		clause.append(",GrandTotal ");
		clause.append("FROM fl_ticket_order_infos_v ");
		clause.append("WHERE TRUE ");

		if (!StringUtil.isNullOrEmpty(documentNo))
			clause.append("AND DocumentNo like'" + documentNo + "%' ");
		if (dateOrderedFrom != null) {
			clause.append("AND DateOrdered>=? ");
			prms.add(dateOrderedFrom);
		}
		if (dateOrderedTo != null) {
			clause.append("AND DateOrdered<=? ");
			prms.add(dateOrderedTo);
		}

		SQLUtil util = new SQLUtil(clause.toString(), null, prms.toArray());
		try {
			getTable().loadTable(util.getResultSet());
			if (getTable().getRowCount() > 0)
				getTable().setRowSelectionInterval(0, 0);
		} finally {
			util.close();
		}

	}

	@Override
	public void doSelection(Integer ID) {
		if (ID != null) {
			int selectedRow = getTable().getSelectedRow();
			int fl_ticket_id = ID;
			int C_BPartner_ID = (int) getTable().getValueAt(selectedRow, 1);
			int C_Order_ID = (int) getTable().getValueAt(selectedRow, 2);
			FLYProcessUtil
					.setParameter(getM_pi(), "fl_ticket_id", fl_ticket_id);
			FLYProcessUtil.setParameter(getM_pi(), "C_BPartner_ID",
					C_BPartner_ID);
			FLYProcessUtil.setParameter(getM_pi(), "C_Order_ID", C_Order_ID);
			if (getHandler() != null)
				getHandler().handle(getM_ctx(), getM_pi(), null);
			dispose();

		}
	}

	@Override
	public void handleRefresh() {
		doResults(f_dateFrom.getTimestamp(), f_dateTo.getTimestamp(),
				f_documentno.getText(), "'N'");
	}

	@Override
	public void handleReset() {
		f_dateFrom.setValue(Env.getContextAsDate(Env.getCtx(), "#Date"));
		f_dateTo.setValue(Env.getContextAsDate(Env.getCtx(), "#Date"));
		f_documentno.setText(null);
		setResults(new Mticket[0]);
	}
	
	@Override
	public void handleDelete() {
		Integer ID = getTable().getSelectedRowKey();
		if (ID != null) {
			if(!FLYDialog.ask("Delete this ticket?"))
				return;
			Mticket.deleteTicket(getM_ctx(), ID.intValue());
			int fl_ticket_id = 0;
			
			int C_Order_ID = 0;
			FLYProcessUtil
					.setParameter(getM_pi(), "fl_ticket_id", fl_ticket_id);

			FLYProcessUtil.setParameter(getM_pi(), "C_Order_ID", C_Order_ID);
			if (getHandler() != null)
				getHandler().handle(getM_ctx(), getM_pi(), null);
			handleRefresh();
		}
	}	
}
