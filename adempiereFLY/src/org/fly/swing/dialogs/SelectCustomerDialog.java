package org.fly.swing.dialogs;

import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.KeyStroke;

import org.compiere.minigrid.ColumnInfo;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MBPartnerInfo;
import org.compiere.process.ProcessInfo;
import org.compiere.swing.CLabel;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.fly.core.MSG;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.swing.FLYMainFrame;
import org.fly.swing.base.FLYBaseGridDialog;
import org.fly.swing.base.FLYTextField;

public class SelectCustomerDialog extends FLYBaseGridDialog {

	private static final long serialVersionUID = 5262256928680369585L;

	private FLYTextField f_value = new FLYTextField();
	private FLYTextField f_name = new FLYTextField();
	private FLYTextField f_email = new FLYTextField();
	private FLYTextField f_phone = new FLYTextField();
	private FLYTextField f_city = new FLYTextField();

	private CLabel l_value = new CLabel(MSG.get("Value"));
	private CLabel l_name = new CLabel(MSG.get("Name"));
	private CLabel l_email = new CLabel(MSG.get("Email"));
	private CLabel l_phone = new CLabel(MSG.get("Phone"));
	private CLabel l_city = new CLabel(MSG.get("City"));

	/** From Clause */
	private static String s_sqlFrom = "RV_BPartner";
	/** Where Clause */
	private static String s_sqlWhere = "IsActive='Y'";

	public SelectCustomerDialog(Frame owner, String title, boolean modal,
			ProcessInfo pi, Properties ctx) throws HeadlessException {
		super(owner, title, modal, pi, ctx);
		setControls();

	}

	public SelectCustomerDialog(String title, ProcessInfo pi, Properties ctx)
			throws HeadlessException {
		this(FLYMainFrame.instance.getFrame(), title, true, pi, ctx);
	}

	/** Table Column Layout Info */
	private static ColumnInfo[] s_layout = new ColumnInfo[] {
			new ColumnInfo(" ", "C_BPartner_ID", IDColumn.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Value"), "Value",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Name"), "Name",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Email"), "Email",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Phone"), "Phone",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "Postal"), "Postal",
					String.class),
			new ColumnInfo(Msg.translate(Env.getCtx(), "City"), "City",
					String.class) };

	private void setControls() {
		setButtons(true, true, true, false, false, false, false);
		JButton previous = createButtonAction("Previous",
				KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0));
		getConfirmPanel().addButton(previous);
		JButton next = createButtonAction("Next",
				KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0));
		getConfirmPanel().addButton(next);

		addPairControl(l_value, labelDimension, f_value, ComponentDimension);
		addPairControl(l_name, labelDimension, f_name, ComponentDimension);
		addPairControl(l_email, labelDimension, f_email, ComponentDimension);
		addPairControl(l_phone, labelDimension, f_phone, ComponentDimension);
		addPairControl(l_city, labelDimension, f_city, ComponentDimension);

		getTable().prepareTable(s_layout, s_sqlFrom, s_sqlWhere, false,
				"RV_BPartner");
		getTable().addMouseListener(this);

		getTable().setColumnVisibility(getTable().getColumn(0), false);
		getTable().getColumn(1).setPreferredWidth(100);
		getTable().getColumn(2).setPreferredWidth(200);
		getTable().getColumn(3).setPreferredWidth(100);
		getTable().getColumn(4).setPreferredWidth(100);
		getTable().getColumn(5).setPreferredWidth(100);
		getTable().getColumn(6).setPreferredWidth(150);

		refreshScrollBars();
		// handleRefresh();
	}

	@Override
	public void handleRefresh() {
		/*
		  //3.8.0 master
		setResults(MBPartnerInfo.find(getM_ctx(), f_value.getText(),
				f_name.getText(), null, f_email.getText(), f_phone.getText(),
				f_city.getText()));
				
		*/
		
		//hotfix/3.8.0#002
		setResults(MBPartnerInfo.find(getM_ctx(), f_value.getText(),
				null, f_name.getText(), null, null, f_email.getText(), f_phone.getText(),
				f_city.getText()));

	}

	@Override
	public void handleReset() {
		f_value.setText(null);
		f_name.setText(null);
		f_email.setText(null);
		f_phone.setText(null);
		f_city.setText(null);
		setResults(new MBPartnerInfo[0]);
	}

	@Override
	public void doSelection(Integer ID) {
		if (ID != null && ID.intValue() != 0) {
			FLYProcessUtil.setParameter(getM_pi(), "C_BPartner_ID",
					ID.intValue());
			if (getHandler() != null)
				getHandler().handle(getM_ctx(), getM_pi(), null);
			dispose();

		}
	}

}
