/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.swing;


import java.util.List;

import javax.swing.SwingConstants;

import org.compiere.model.Mlayout;
import org.fly.core.FLYEnv;
import org.fly.core.utils.StringUtil;
import org.fly.swing.base.FLYButton;
import org.fly.swing.controls.base.FLYBaseButtonGroup;



public class FLYStaticMenuBG extends FLYBaseButtonGroup{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3301597612185455578L;
	
	public static int B_EXIT			= 0;
	public static int B_PREFERENCES		= 1;
	public static int B_THEMES			= 2;
	public static int B_SAVE_LAYOUT		= 3;
	public static int B_UNLOCK_LAYOUT	= 4;
	public static int B_TEST			= 5;
	
	private int BUTTONCOUNT				= 6;

	public FLYStaticMenuBG() {
		int layoutSize = 0;
		List<Mlayout> layouts = FLYEnv.instance.getLayouts();
		if(layouts != null)
			layoutSize = layouts.size();
		setButtonWidth( Integer.parseInt(FLYEnv.instance.getProperty(FLYEnv.P_BUTTON_WIDTH)));
		setButtonHeight(Integer.parseInt(FLYEnv.instance.getProperty(FLYEnv.P_BUTTON_HEIGHT)));
		setButtonCount(BUTTONCOUNT + layoutSize);
		for(int i = 0; i < layoutSize; i++){
			FLYButton button = getButtons().get(i + BUTTONCOUNT);
			Mlayout layout = layouts.get(i) ;
			button.setTag(layout);
			button.setText(StringUtil.stringToHtml(layout.getName(), "blue", 10));
			button.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					FLYButton btn = (FLYButton)evt.getSource();
					Object tagObject = btn.getTag();
					if(tagObject instanceof Mlayout){
						Mlayout layout = (Mlayout)tagObject;
						FLYEnv.instance.setM_current_layout(layout);
						FLYSwingController.instance.loadControls(layout.get_ID());
						FLYSwingController.instance.refreshControls(FLYEnv.instance.getC_Order_ID());
					}
				}
			});
			button.setVerticalTextPosition(SwingConstants.BOTTOM);
			button.setHorizontalTextPosition(SwingConstants.CENTER);

		}
	}

}
