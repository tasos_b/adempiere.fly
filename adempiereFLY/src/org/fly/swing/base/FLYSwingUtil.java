package org.fly.swing.base;

import java.awt.Dimension;

public class FLYSwingUtil {

	public static final int POS_BUTTON_WIDTH = 80;
	
	public static final int POS_BUTTON_HEIGHT = 50;
	
	public static final Dimension	POS_BUTTON_SIZE = new Dimension(POS_BUTTON_WIDTH,POS_BUTTON_HEIGHT);

	
}
