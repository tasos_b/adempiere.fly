package org.fly.swing.base;

import java.awt.Dimension;
import java.util.Properties;

import org.compiere.process.ProcessCall;
import org.compiere.process.ProcessInfo;
import org.compiere.util.Trx;
import org.fly.swing.FLYMainFrame;

public class ExecuteTestDialog implements ProcessCall{

	@Override
	public boolean startProcess(Properties ctx, ProcessInfo pi, Trx trx) {
		TestBaseDialog form = new TestBaseDialog("Test", pi, ctx);
		form.setM_pi(pi);
		form.setM_ctx(ctx);
		//form.setPreferredSize(new Dimension(300, 200));
		form.setSize(new Dimension(800, 600));
		form.setLocationRelativeTo(FLYMainFrame.instance.getFrame());
		form.setVisible(true);
		return false;
	}

}
