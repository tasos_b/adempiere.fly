package org.fly.swing.base;

import java.awt.event.ActionEvent;
import java.util.Properties;

import org.compiere.apps.FLYConfirmPanel;
import org.compiere.process.ProcessInfo;
import org.compiere.swing.CButton;

public class TestBaseDialog extends FLYBaseGridDialog{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3048987291744593875L;

	public TestBaseDialog(String text, ProcessInfo pi, Properties ctx){
		super(text, pi,ctx);
		setButtons(true, false, false, false, false, false, false);
		getConfirmPanel().addButton(new CButton("aaa"));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(FLYConfirmPanel.A_CANCEL))
			dispose();
		
	}

}
