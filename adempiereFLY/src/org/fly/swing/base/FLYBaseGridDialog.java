package org.fly.swing.base;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.compiere.apps.FLYConfirmPanel;
import org.compiere.minigrid.MiniTable;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfo;
import org.compiere.swing.CLabel;
import org.compiere.swing.CPanel;
import org.fly.swing.FLYMainFrame;

public abstract class FLYBaseGridDialog extends FLYBaseDialog implements
		MouseListener {

	private static final long serialVersionUID = 7698791019288863058L;

	private CPanel mainPanel = new CPanel();
	private CPanel controlPanel = new CPanel();
	private MiniTable m_table = new MiniTable();
	private JScrollPane m_gridScrollPane = new JScrollPane();
	private JScrollPane m_controlScrollPane = new JScrollPane();
	private int m_scrollWidth = 30;

	public Dimension labelDimension = new Dimension(100, 30);
	public Dimension ComponentDimension = new Dimension(200, 30);

	public FLYBaseGridDialog(Frame owner, String title, boolean modal,
			ProcessInfo pi, Properties ctx) throws HeadlessException {
		super(owner, title, modal, pi, ctx);

		init();

	}

	public FLYBaseGridDialog(String title, ProcessInfo pi, Properties ctx)
			throws HeadlessException {
		this(FLYMainFrame.instance.getFrame(), title, true, pi, ctx);
	}

	private void init() {

		getPanel().add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		m_controlScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		m_controlScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		m_controlScrollPane.getViewport().add(controlPanel, null);
		m_controlScrollPane.getVerticalScrollBar().setPreferredSize(
				new Dimension(m_scrollWidth, 0));
		m_controlScrollPane.getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, m_scrollWidth));

		mainPanel.add(m_controlScrollPane);
		// controlPanel.setBackground(Color.gray);

		m_gridScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		m_gridScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		m_gridScrollPane.getViewport().add(m_table, null);
		m_gridScrollPane.getVerticalScrollBar().setPreferredSize(
				new Dimension(m_scrollWidth, 0));
		m_gridScrollPane.getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, m_scrollWidth));

		getTable().setRowSelectionAllowed(true);
		getTable().setColumnSelectionAllowed(false);
		getTable().setMultiSelection(false);
		getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getTable().setRowHeight(30);
		getTable().setAutoResize(false);

		mainPanel.add(m_gridScrollPane);
		controlPanel.setPreferredSize(new Dimension(400, 200));
		controlPanel.setLayout(new FlowLayout());
	}

	public void refreshScrollBars() {
		m_gridScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		m_gridScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		m_gridScrollPane.getVerticalScrollBar().setPreferredSize(
				new Dimension(m_scrollWidth, 0));
		m_gridScrollPane.getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, m_scrollWidth));

		m_controlScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		m_controlScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		m_controlScrollPane.getVerticalScrollBar().setPreferredSize(
				new Dimension(m_scrollWidth, 0));
		m_controlScrollPane.getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, m_scrollWidth));
	}

	public CPanel getMainPanel() {
		return mainPanel;
	}

	public void setMainPanel(CPanel mainPanel) {
		this.mainPanel = mainPanel;
	}

	public CPanel getControlPanel() {
		return controlPanel;
	}

	public void setControlPanel(CPanel controlPanel) {
		this.controlPanel = controlPanel;

	}

	public MiniTable getTable() {
		return m_table;
	}

	public void setTable(MiniTable m_table) {
		this.m_table = m_table;
	}

	public void addPairControl(CLabel label, Dimension labelDimension,
			Component component, Dimension componentDimension) {
		label.setPreferredSize(labelDimension);
		component.setPreferredSize(componentDimension);
		CPanel panel = new CPanel();
		panel.setLayout(new FlowLayout());
		panel.add(label);
		panel.add(component);
		getControlPanel().add(panel);
	}

	public void setResults(ResultSet results) {
		getTable().loadTable(results);
		if (getTable().getRowCount() > 0)
			getTable().setRowSelectionInterval(0, 0);
	}

	public void setResults(PO[] results) {
		getTable().loadTable(results);
		if (getTable().getRowCount() > 0)
			getTable().setRowSelectionInterval(0, 0);
	}

	public void doSelection(Integer ID) {

	}

	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() > 0 && getTable().getSelectedRow() != -1) {
			Integer ID = getTable().getSelectedRowKey();
			if (ID != null) {
				doSelection(ID);
			}
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void handleRefresh() {
	}

	public void handleReset() {
	}
	public void handleDelete() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(FLYConfirmPanel.A_CANCEL))
			dispose();
		else if ("Refresh".equals(e.getActionCommand()))
			handleRefresh();

		else if ("Reset".equals(e.getActionCommand()))
			handleReset();
		else if ("Delete".equals(e.getActionCommand()))
			handleDelete();

		else if (e.getActionCommand().equals(FLYConfirmPanel.A_OK)) {
			Integer ID = getTable().getSelectedRowKey();
			if (ID != null) {
				doSelection(ID);
			}
		} else if ("Previous".equalsIgnoreCase(e.getActionCommand())) {
			int rows = m_table.getRowCount();
			if (rows == 0)
				return;
			int row = m_table.getSelectedRow();
			row--;
			if (row < 0)
				row = 0;
			m_table.getSelectionModel().setSelectionInterval(row, row);
			
		} else if ("Next".equalsIgnoreCase(e.getActionCommand())) {
			int rows = m_table.getRowCount();
			if (rows == 0)
				return;
			int row = m_table.getSelectedRow();
			row++;
			if (row >= rows)
				row = rows - 1;
			m_table.getSelectionModel().setSelectionInterval(row, row);
			
		}

	}

}
