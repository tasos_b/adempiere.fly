package org.fly.swing.base;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Locale;

import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import virtualkeyboard.gui.DialogVirtualKeyboardReal;

public class FLYTextField extends JTextField{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4043295826105043330L;
	public JTextComponent instance;
	Locale locale = Locale.getDefault();
	public FLYTextField(){
		super();
		instance = this;
		setSize(200, 60);
		addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
            	DialogVirtualKeyboardReal dlg = new DialogVirtualKeyboardReal(null, true, instance);
            	dlg.setLocaleL(locale);
            	
            }
        });
	}

}
