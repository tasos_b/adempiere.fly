package org.fly.swing.base;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.util.Properties;

import javax.swing.KeyStroke;

import org.compiere.apps.AppsAction;
import org.compiere.apps.FLYConfirmPanel;
import org.compiere.process.ProcessInfo;
import org.compiere.swing.CButton;
import org.compiere.swing.CDialog;
import org.compiere.swing.CPanel;
import org.fly.core.FLYProcessHandler;
import org.fly.swing.FLYMainFrame;

public abstract class FLYBaseDialog extends CDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2980095116319068227L;

	private FLYConfirmPanel m_confirmPanel = new FLYConfirmPanel(true);

	private BorderLayout m_layout = new BorderLayout();

	private CPanel m_panel = new CPanel();

	private ProcessInfo m_pi;

	private Properties m_ctx;

	private FLYProcessHandler handler;

	public FLYBaseDialog(Frame owner, String title, boolean modal,
			ProcessInfo pi, Properties ctx) throws HeadlessException {

		super(owner, title, false);
		setM_pi(pi);
		setM_ctx(ctx);
		initComponents();
	}

	public FLYBaseDialog(String title, ProcessInfo pi, Properties ctx) throws HeadlessException {
		this(FLYMainFrame.instance.getFrame(), title, true, pi, ctx);
	}

	public void initComponents() {

		getContentPane().add(m_panel);
		m_panel.setLayout(m_layout);
		m_panel.add(m_confirmPanel, BorderLayout.SOUTH);
		m_confirmPanel.addActionListener(this);

	}

	public void setButtons(boolean withCancelButton, boolean withRefreshButton,
			boolean withResetButton, boolean withCustomizeButton,
			boolean withHistoryButton, boolean withZoomButton, boolean withText) {

		m_panel.remove(m_confirmPanel);
		m_confirmPanel = new FLYConfirmPanel(withCancelButton,
				withRefreshButton, withResetButton, withCustomizeButton,
				withHistoryButton, withZoomButton, withText);
		m_panel.add(m_confirmPanel, BorderLayout.SOUTH);
		m_confirmPanel.addActionListener(this);
	}

	public FLYConfirmPanel getConfirmPanel() {
		return m_confirmPanel;
	}

	public BorderLayout getMLayout() {
		return m_layout;
	}

	public CPanel getPanel() {
		return m_panel;
	}

	public ProcessInfo getM_pi() {
		return m_pi;
	}

	public void setM_pi(ProcessInfo m_pi) {
		this.m_pi = m_pi;
	}

	public Properties getM_ctx() {
		return m_ctx;
	}

	public void setM_ctx(Properties m_ctx) {
		this.m_ctx = m_ctx;
	}

	public FLYProcessHandler getHandler() {
		return handler;
	}

	public void setHandler(FLYProcessHandler handler) {
		this.handler = handler;
	}
	
	protected CButton createButtonAction(String action, KeyStroke accelerator) {
		AppsAction act = new AppsAction(action, accelerator, false);
		act.setDelegate(this);
		CButton button = (CButton)act.getButton();
		button.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		button.setMinimumSize(getPreferredSize());
		button.setMaximumSize(getPreferredSize());
		button.setFocusable(false);
		return button;
	}	//	getButtonAction

	public abstract void actionPerformed(ActionEvent e);
}
