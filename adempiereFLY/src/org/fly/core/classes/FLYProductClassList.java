/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.adempiere.exceptions.DBException;
import org.compiere.util.Env;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.sql.SQLProduct;
import org.fly.swing.forms.ButtonInfo;

public class FLYProductClassList {
	public FLYProductClassList() {
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		init(AD_Client_ID);
	}

	private ArrayList<FLYProductClass> productClasses;

	public List<FLYProductClass> getProductClasses(BigDecimal parent) {
		if (parent == null)
			return productClasses;
		else {
			List<FLYProductClass> subClasses = productClasses
					.stream()
					.filter(p -> (p.getM_product_class_parent_id() != null)
							&& p.getM_product_class_parent_id().intValue() == parent
									.intValue()).collect(Collectors.toList());
			return subClasses;
		}
	}

	private void init(int AD_Client_ID) {
		if (productClasses == null)
			productClasses = new ArrayList<FLYProductClass>();
		productClasses.clear();

		String sql = SQLProduct.GET_PRODUCT_CLASSES;
		
		SQLUtil util = new SQLUtil(sql, null, AD_Client_ID);
		
		try{
			assignClasses(util.getResultSet());
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

	}

	private void assignClasses(ResultSet rs) throws SQLException {
		while (rs.next()) {
			FLYProductClass classi = new FLYProductClass();
			classi.setM_product_class_id(rs
					.getInt("M_Product_Class_ID"));
			classi.setAD_Org_ID(rs.getInt("AD_Org_ID"));
			classi.setM_product_class_parent_id(rs
					.getBigDecimal("M_Product_Class_Parent_ID"));
			classi.setName(rs.getString("Name"));
			classi.setDescription(rs.getString("Description"));
			byte[] buttoninfo = rs.getBytes("buttoninfo");
			if(buttoninfo != null && buttoninfo.length > 0){
				ButtonInfo info = ByteArrayUtil.byteArrayToButtonInfo(buttoninfo);
				classi.setBackcolor(info.getM_background());
				classi.setForecolor(info.getM_foreground());
				classi.setTitle(info.getM_title());
				classi.setImage(info.getImage());
			}else{
				classi.setBackcolor(null);
				classi.setForecolor(null);
				classi.setTitle(null);
			}

			productClasses.add(classi);
		}
		
	}

}
