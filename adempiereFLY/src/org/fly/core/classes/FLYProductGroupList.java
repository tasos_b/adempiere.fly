/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.adempiere.exceptions.DBException;
import org.compiere.util.Env;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.sql.SQLProduct;
import org.fly.swing.forms.ButtonInfo;

public class FLYProductGroupList {
	public FLYProductGroupList() {
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		init(AD_Client_ID);
	}

	private ArrayList<FLYProductGroup> productGroups;

	public List<FLYProductGroup> getProductGroups(BigDecimal parent) {
		if (parent == null)
			return productGroups;
		else {
			List<FLYProductGroup> subClasses = productGroups
					.stream()
					.filter(p -> (p.getM_product_group_parent_id() != null)
							&& p.getM_product_group_parent_id().intValue() == parent
									.intValue()).collect(Collectors.toList());
			return subClasses;
		}
	}

	private void init(int AD_Client_ID) {
		if (productGroups == null)
			productGroups = new ArrayList<FLYProductGroup>();
		productGroups.clear();

		String sql = SQLProduct.GET_PRODUCT_GROUPS;
		
		SQLUtil util = new SQLUtil(sql, null, AD_Client_ID);
		
		try{
			assignGroups(util.getResultSet());
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

	}

	private void assignGroups(ResultSet rs) throws SQLException {
		while (rs.next()) {
			FLYProductGroup group = new FLYProductGroup();
			group.setM_product_group_id(rs
					.getInt("M_Product_Group_ID"));
			group.setAD_Org_ID(rs.getInt("AD_Org_ID"));
			group.setM_product_group_parent_id(rs
					.getBigDecimal("M_Product_Group_Parent_ID"));
			group.setName(rs.getString("Name"));
			group.setDescription(rs.getString("Description"));
			byte[] buttoninfo = rs.getBytes("buttoninfo");
			if(buttoninfo != null && buttoninfo.length > 0){
				ButtonInfo info = ByteArrayUtil.byteArrayToButtonInfo(buttoninfo);
				group.setBackcolor(info.getM_background());
				group.setForecolor(info.getM_foreground());
				group.setTitle(info.getM_title());
				group.setImage(info.getImage());
			}else{
				group.setBackcolor(null);
				group.setForecolor(null);
				group.setTitle(null);
			}
			productGroups.add(group);
		}
		
	}

}
