/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;

import java.math.BigDecimal;

import javax.swing.ImageIcon;


/**
 * FLYPInstance class contains  data 
 * to setup a button that fires an action (fly process)
 * or an action group (fly process group)
 *
 * @author tasosbull@gmail.com   
 *
 */

public class FLYPInstance {

	private String name;
	private String title;
	private ImageIcon image;
	BigDecimal forecolor;
	BigDecimal backcolor;
	BigDecimal fl_process_group_id;
	BigDecimal fl_process_id;
	byte[] data;
	String classname;
	BigDecimal AD_Process_ID;

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Get the getAD_Process_ID
	 * if it is not an action group but a single action 
	 * @return AD_Process_ID
	 * */
	public BigDecimal getAD_Process_ID() {
		return AD_Process_ID;
	}
	/**
	 * Set the getAD_Process_ID
	 * @param AD_Process_ID
	 * */
	public void setAD_Process_ID(BigDecimal aD_Process_ID) {
		AD_Process_ID = aD_Process_ID;
	}

	/**
	 * Get the name of this Action
	 * @return the name
	 * */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of this Action
	 * @param name the name
	 * */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the image of this Action
	 * to decorate the button
	 * @return the image
	 * */
	public ImageIcon getImage() {
		return image;
	}

	/**
	 * Set the image of this Action
	 * to decorate the button
	 * @param binaryData the image
	 * */
	public void setImage(ImageIcon binaryData) {
		image = binaryData;
	}

	/**
	 * Get the foreground color of this Action
	 * to decorate the button
	 * @return the foreground
	 * */
	public BigDecimal getForecolor() {
		return forecolor;
	}

	/**
	 * Set the foreground color of this Action
	 * to decorate the button
	 * @param forecolor the foreground
	 * */
	public void setForecolor(BigDecimal forecolor) {
		this.forecolor = forecolor;
	}

	/**
	 * Get the background color of this Action
	 * to decorate the button
	 * @return the image
	 * */
	public BigDecimal getBackcolor() {
		return backcolor;
	}

	
	/**
	 * Set the background color of this Action
	 * to decorate the button
	 * @param backcolor the background
	 * */
	public void setBackcolor(BigDecimal backcolor) {
		this.backcolor = backcolor;
	}

	/**
	 * Get the action group 
	 * (if is a group and not a single action)
	 * @return the action group
	 * */
	public BigDecimal getFl_process_group_id() {
		return fl_process_group_id;
	}

	/**
	 * Set the action group 
	 * (if is a group and not a single action)
	 * @param fl_process_group_id the action group
	 * */
	public void setFl_process_group_id(BigDecimal fl_process_group_id) {
		this.fl_process_group_id = fl_process_group_id;
	}

	/**
	 * Get the action  
	 * (if it is a single action)
	 * @return the action 
	 * */
	public BigDecimal getFl_process_id() {
		return fl_process_id;
	}

	/**
	 * Set the action  
	 * (if it is a single action)
	 * @param fl_process_id the action 
	 * */
	public void setFl_process_id(BigDecimal fl_process_id) {
		this.fl_process_id = fl_process_id;
	}

	/**
	 * Get the action params  
	 * (if it is a single action)
	 * @return the action params
	 * */
	public byte[] getData() {
		return data;
	}

	/**
	 * Set the action params  
	 * (if it is a single action)
	 * @param data the action params
	 * */
	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * Get the class name that executes this action 
	 * (if it is a single action)
	 * @return the class name
	 * */	
	public String getClassname() {
		return classname;
	}

	/**
	 * Set the class name that executes this action 
	 * (if it is a single action)
	 * @param classname the class name
	 * */	
	public void setClassname(String classname) {
		this.classname = classname;
	}
	
}
