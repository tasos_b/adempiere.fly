/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;


import java.math.BigDecimal;

import javax.swing.ImageIcon;


/**
 * 
 * Data to create the buttons for a ProductClassification button group
 * 
 * @author tasos boulassikis tasosbull@gmail.com
 *
 */
public class FLYProductClassification {

	private int m_product_classification_id;
	private int AD_Org_ID;
	private String name;
	private String description;
	private BigDecimal m_product_classification_parent_id;
	private String title;
	private BigDecimal forecolor;
	private BigDecimal backcolor;
	private ImageIcon image;


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	public ImageIcon getImage() {
		return image;
	}

	public void setImage(ImageIcon image) {
		this.image = image;
	}
	
	public int getM_product_classification_id() {
		return m_product_classification_id;
	}
	public void setM_product_classification_id(int m_product_classification_id) {
		this.m_product_classification_id = m_product_classification_id;
	}
	public int getAD_Org_ID() {
		return AD_Org_ID;
	}
	public void setAD_Org_ID(int aD_Org_ID) {
		AD_Org_ID = aD_Org_ID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getM_product_classification_parent_id() {
		return m_product_classification_parent_id;
	}
	public void setM_product_classification_parent_id(
			BigDecimal m_product_classification_parent_id) {
		this.m_product_classification_parent_id = m_product_classification_parent_id;
	}

	public BigDecimal getForecolor() {
		return forecolor;
	}
	public void setForecolor(BigDecimal forecolor) {
		this.forecolor = forecolor;
	}
	public BigDecimal getBackcolor() {
		return backcolor;
	}
	public void setBackcolor(BigDecimal backcolor) {
		this.backcolor = backcolor;
	}


	
	
}
