/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.adempiere.exceptions.DBException;
import org.compiere.util.Env;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.sql.SQLProduct;
import org.fly.swing.controls.FLYControl;
import org.fly.swing.forms.ButtonInfo;


public class FLYProductList {
	
	private ArrayList<FLYProduct> products;
	

	public FLYProductList(int categoryType , int ID){
		String sql = null;
		if(categoryType == FLYControl.PRODUCT_CATEGORIES)
			sql = SQLProduct.GET_PRODUCTS_BY_CATEGORY;
		else if(categoryType == FLYControl.PRODUCT_CLASSIFICATION)
			sql = SQLProduct.GET_PRODUCTS_BY_CLASSIFICATION;
		else if(categoryType == FLYControl.PRODUCT_CLASS)
			sql = SQLProduct.GET_PRODUCTS_BY_CLASS;
		else if(categoryType == FLYControl.PRODUCT_GROUP)
			sql = SQLProduct.GET_PRODUCTS_BY_GROUP;
		
		
		SQLUtil util = new SQLUtil(sql, null, Env.getAD_Client_ID(Env.getCtx()), ID);
		
		try{
			assignProducts(util.getResultSet());
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

		
	}

	public ArrayList<FLYProduct> getProducts() {
		return products;
	}

	private void assignProducts(ResultSet rs)  throws SQLException{
		if(products == null)
			products = new ArrayList<FLYProduct>();
		else
			products.clear();
		while (rs.next()) {
			
			FLYProduct product = new FLYProduct();
			product.setM_Product_ID(rs
					.getInt("M_Product_ID"));
			product.setValue(rs.getString("Value"));
			product.setName(rs.getString("Name"));
			
			byte[] buttoninfo = rs.getBytes("buttoninfo");
			if(buttoninfo != null && buttoninfo.length > 0){
				ButtonInfo info = ByteArrayUtil.byteArrayToButtonInfo(buttoninfo);
				product.setBackcolor(info.getM_background());
				product.setForecolor(info.getM_foreground());
				product.setTitle(info.getM_title());
				product.setImage(info.getImage());
			}
			else{
				product.setBackcolor(null);
				product.setForecolor(null);
				product.setTitle(null);
			}
				
			products.add(product);

		}
		
	}

}
