/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;

import java.math.BigDecimal;

import javax.swing.ImageIcon;

/**
 * 
 * Information to create a button to sale a product
 * 
 * @author tasos boulassikis tasosbull@gmail.com  
 *
 */
public class FLYProduct {
	
	private int M_Product_ID;
	private String value;
	private String name;
	//buttoninfo
	private String title;
	private BigDecimal forecolor;
	private BigDecimal backcolor;
	private ImageIcon image;
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	/**
	 * Get the product ID
	 * @return the product ID 
	 * */
	public int getM_Product_ID() {
		return M_Product_ID;
	}

	/**
	 * Set the product ID
	 * @param m_Product_ID the product ID 
	 * */
	public void setM_Product_ID(int m_Product_ID) {
		M_Product_ID = m_Product_ID;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Get the product Name
	 * @return the product Name 
	 * */
	public String getName() {
		return name;
	}

	/**
	 * Set the product name
	 * @param name the product name 
	 * */

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the button foreground 
	 * @return the product foreground 
	 * */
	public BigDecimal getForecolor() {
		return forecolor;
	}

	/**
	 * Set the button foreground color
	 * @param forecolor the product foreground color
	 * */
	public void setForecolor(BigDecimal forecolor) {
		this.forecolor = forecolor;
	}

	/**
	 * Get the button background color
	 * @return the product background color
	 * */
	public BigDecimal getBackcolor() {
		return backcolor;
	}

	/**
	 * Set the button background color
	 * @param backcolor the product background color
	 * */
	public void setBackcolor(BigDecimal backcolor) {
		this.backcolor = backcolor;
	}

	/**
	 * Get the button image
	 * @return the product image
	 * */
	public ImageIcon getImage() {
		return image;
	}

	/**
	 * Set the button image
	 * @param image the product image
	 * */
	public void setImage(ImageIcon image) {
		this.image = image;
	}
	
	

}
