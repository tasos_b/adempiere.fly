package org.fly.core.classes;

import java.math.BigDecimal;

public class FLYBanknote {
	
	private String name;
	private BigDecimal moneyvalue;
	private byte[] binarydata;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getMoneyvalue() {
		return moneyvalue;
	}
	public void setMoneyvalue(BigDecimal moneyvalue) {
		this.moneyvalue = moneyvalue;
	}
	public byte[] getBinarydata() {
		return binarydata;
	}
	public void setBinarydata(byte[] binarydata) {
		this.binarydata = binarydata;
	}
}
