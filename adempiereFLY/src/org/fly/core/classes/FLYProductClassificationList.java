/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.adempiere.exceptions.DBException;
import org.compiere.util.Env;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.sql.SQLProduct;
import org.fly.swing.forms.ButtonInfo;

public class FLYProductClassificationList {
	public FLYProductClassificationList() {
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		init(AD_Client_ID);
	}

	private ArrayList<FLYProductClassification> productClassifications;

	public List<FLYProductClassification> getProductClassifications(BigDecimal parent) {
		if (parent == null)
			return productClassifications;
		else {
			List<FLYProductClassification> subClassifications = productClassifications
					.stream()
					.filter(p -> (p.getM_product_classification_parent_id() != null)
							&& p.getM_product_classification_parent_id().intValue() == parent
									.intValue()).collect(Collectors.toList());
			return subClassifications;
		}
	}

	private void init(int AD_Client_ID) {
		if (productClassifications == null)
			productClassifications = new ArrayList<FLYProductClassification>();
		productClassifications.clear();

		String sql = SQLProduct.GET_PRODUCT_CLASSIFICATIONS;
		
		SQLUtil util = new SQLUtil(sql, null, AD_Client_ID);
		
		try{
			assignClasifications(util.getResultSet());
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

	}

	private void assignClasifications(ResultSet rs) throws SQLException {
		while (rs.next()) {
			FLYProductClassification classification = new FLYProductClassification();
			classification.setM_product_classification_id(rs
					.getInt("M_Product_Classification_ID"));
			classification.setAD_Org_ID(rs.getInt("AD_Org_ID"));
			classification.setM_product_classification_parent_id(rs
					.getBigDecimal("M_Classification_Parent_ID"));
			classification.setName(rs.getString("Name"));
			classification.setDescription(rs.getString("Description"));
			byte[] buttoninfo = rs.getBytes("buttoninfo");
			if(buttoninfo != null && buttoninfo.length > 0){
				ButtonInfo info = ByteArrayUtil.byteArrayToButtonInfo(buttoninfo);
				classification.setBackcolor(info.getM_background());
				classification.setForecolor(info.getM_foreground());
				classification.setTitle(info.getM_title());
				classification.setImage(info.getImage());
			}else{
				classification.setBackcolor(null);
				classification.setForecolor(null);
				classification.setTitle(null);
			}

			productClassifications.add(classification);
		}
		
	}

}
