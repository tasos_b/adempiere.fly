/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.adempiere.exceptions.DBException;
import org.compiere.util.Env;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.sql.SQLProduct;
import org.fly.swing.forms.ButtonInfo;

/**
 * 
 * Data to create a ProductCategories button group
 * given a parent category
 * @author tasos boulassikis tasosbull@gmail.com  
 *
 */
public class FLYProductCategoryList {

	public FLYProductCategoryList() {
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		init(AD_Client_ID);
	}

	private ArrayList<FLYProductCategory> productCategories;

	/**
	 * Get a list given a parent category
	 * @return the list of product categories
	 * */
	public List<FLYProductCategory> getProductCategories(BigDecimal parent) {
		if (parent == null)
			return productCategories;
		else {
			List<FLYProductCategory> subCategoies = productCategories
					.stream()
					.filter(p -> (p.getM_product_category_parent_id() != null)
							&& p.getM_product_category_parent_id().intValue() == parent
									.intValue()).collect(Collectors.toList());
			return subCategoies;
		}
	}

	/**
	 * Load data
	 * @param AD_Client_ID Load data for this client
	 * */
	private void init(int AD_Client_ID) {
		if (productCategories == null)
			productCategories = new ArrayList<FLYProductCategory>();
		productCategories.clear();

		String sql = SQLProduct.GET_PRODUCT_CATEGORIES;
		
		SQLUtil util = new SQLUtil(sql, null, AD_Client_ID);
		
		try{
			assignCategories(util.getResultSet());
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

	}

	/**
	 * Create the list items after load
	 * @param rs the ResultSet
	 * */
	private void assignCategories(ResultSet rs) throws SQLException {
		while (rs.next()) {
			FLYProductCategory category = new FLYProductCategory();
			category.setM_product_category_id(rs
					.getInt("M_Product_Category_ID"));
			category.setAD_Org_ID(rs.getInt("AD_Org_ID"));
			category.setM_product_category_parent_id(rs
					.getBigDecimal("M_Product_Category_Parent_ID"));
			category.setName(rs.getString("Name"));
			category.setDescription(rs.getString("Description"));
			
			byte[] buttoninfo = rs.getBytes("buttoninfo");
			if(buttoninfo != null && buttoninfo.length > 0){
				ButtonInfo info = ByteArrayUtil.byteArrayToButtonInfo(buttoninfo);
				category.setBackcolor(info.getM_background());
				category.setForecolor(info.getM_foreground());
				category.setTitle(info.getM_title());
				category.setImage(info.getImage());
			}
			else{
				category.setBackcolor(null);
				category.setForecolor(null);
				category.setTitle(null);
			}
			
			productCategories.add(category);
		}
		
	}

}
