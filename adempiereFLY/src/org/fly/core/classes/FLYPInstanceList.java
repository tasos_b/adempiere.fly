/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core.classes;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.adempiere.exceptions.DBException;
import org.compiere.process.FLYProcess;
import org.compiere.process.FLYProcessList;
import org.compiere.process.ProcessInfo;
import org.fly.core.utils.ByteArrayUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.sql.SQLButtons;
import org.fly.swing.forms.ButtonInfo;


/**
 * 
 * Used to create a FreeButtons control
 * Each button executes an action or an action group
 * The decoration and the functionality of a button are described 
 * by a FLYPInstance object
 * 
 * @author tasos boulassikis tasosbull@gmail.com  
 *
 */
public class FLYPInstanceList {
	
	private ArrayList<FLYPInstance> pInstances;
	int m_fl_layoutcontrol_id;

/**
 * Default Constructor
 * */	
	public FLYPInstanceList(int fl_layoutcontrol_id){
		m_fl_layoutcontrol_id = fl_layoutcontrol_id;
		init();
	}
	/**
	 * Collect data
	 * */
	private void init() {
		String sql = SQLButtons.GET_BUTTONS;
		
		SQLUtil util = new SQLUtil(sql, null, m_fl_layoutcontrol_id);
		
		try{
			assignInstances(util.getResultSet());
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

		
	}

	/**
	 * Foreach row create a FLYPInstance
	 * @param rs the ResultSet
	 * */
	private void assignInstances(ResultSet rs) throws SQLException {
		pInstances = new ArrayList<FLYPInstance>();
		while (rs.next()) {
			FLYPInstance button = new FLYPInstance();
			button.setName(rs.getString("name"));
			
			
			byte[] buttoninfo = rs.getBytes("buttoninfo");
			if(buttoninfo != null && buttoninfo.length > 0){
				ButtonInfo info = ByteArrayUtil.byteArrayToButtonInfo(buttoninfo);
				button.setBackcolor(info.getM_background());
				button.setForecolor(info.getM_foreground());
				button.setTitle(info.getM_title());
				button.setImage(info.getImage());
			}
			else{
				button.setBackcolor(null);
				button.setForecolor(null);
				button.setTitle(null);
			}			
			
			button.setFl_process_group_id(rs.getBigDecimal("fl_process_group_id"));
			button.setFl_process_id(rs.getBigDecimal("fl_process_id"));
			button.setData(rs.getBytes("data"));
			button.setClassname(rs.getString("classname"));
			button.setAD_Process_ID(rs.getBigDecimal("AD_Process_ID"));
			pInstances.add(button);

		}
		
	}
	
	/**
	 * Get the list
	 * @return the list
	 * */
	public ArrayList<FLYPInstance> getFreeButtons() {
		return pInstances;
	}

	/**
	 * Set the list
	 * @param pInstances the list
	 * */
	public void setFreeButtons(ArrayList<FLYPInstance> pInstances) {
		this.pInstances = pInstances;
	}
	
	
	/**
	 * Get the action list object 
	 * return the list 0bject
	 * */
	public FLYProcessList getProcessListFromPInstance(FLYPInstance pInstance){
		FLYProcessList processList = new FLYProcessList(true);
		ProcessInfo pi = null;
		if(pInstance.getFl_process_id() != null){
			if(pInstance.getData() != null && pInstance.getData().length > 0){
				pi = ByteArrayUtil.byteArrayToProcessInfo(pInstance.getData());
			}
			else 
				pi = new ProcessInfo("Process Info", pInstance.getAD_Process_ID().intValue());
			
			pi.setAD_Process_ID(pInstance.getAD_Process_ID().intValue());
			FLYProcess process = new FLYProcess(pi, pInstance.getClassname(), pInstance.getAD_Process_ID().intValue());
			processList.add(process);
		}
		else if (pInstance.getFl_process_group_id() != null){
			String sql = SQLButtons.GET_PROCESS_GROUP;
			SQLUtil util = new SQLUtil(sql, null, pInstance.getFl_process_group_id());
			
			try{
				ResultSet rs = util.getResultSet();
				while (rs.next()) {
					byte[] data = rs.getBytes("data");
					String className = rs.getString("classname");
					int AD_Process_ID = rs.getInt("AD_Process_ID");
					if(data != null && data.length > 0)
						pi = ByteArrayUtil.byteArrayToProcessInfo(data);
					else
						pi = new ProcessInfo("Process Info", AD_Process_ID);
					pi.setAD_Process_ID(AD_Process_ID);
					FLYProcess process = new FLYProcess(pi, className, AD_Process_ID);
					processList.add(process);
				}
			}catch (SQLException e) {
				throw new DBException(e, sql);
			}
			finally{
				util.close();
			}

		}
		
		return processList;
	}




}
