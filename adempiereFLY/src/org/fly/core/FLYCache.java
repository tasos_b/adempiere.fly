/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.fly.core.classes.FLYBanknote;
import org.fly.core.classes.FLYProductCategoryList;
import org.fly.core.classes.FLYProductCategory;
import org.fly.core.classes.FLYProductClass;
import org.fly.core.classes.FLYProductClassList;
import org.fly.core.classes.FLYProductClassification;
import org.fly.core.classes.FLYProductClassificationList;
import org.fly.core.classes.FLYProductGroup;
import org.fly.core.classes.FLYProductGroupList;

/**
 *  Cache some data that are always used
 *  like Product Categories etc
 * @author: tasos boulassikis tasosbull@gmail.com */

public class FLYCache {
	
	private FLYProductCategoryList m_product_categories;
	private FLYProductClassificationList m_product_classifications;
	private FLYProductClassList m_product_classes;
	private FLYProductGroupList m_product_groups;

	private HashMap<Integer, ArrayList<FLYBanknote>> banknotes = new HashMap<Integer, ArrayList<FLYBanknote>>();
	private HashMap<Integer, ArrayList<FLYBanknote>> coins = new HashMap<Integer, ArrayList<FLYBanknote>>();
	
	
	/**
	 * Default Constructor */
	public FLYCache(){
		
		m_product_categories = new FLYProductCategoryList();
		m_product_classifications = new FLYProductClassificationList();
		m_product_classes = new FLYProductClassList();
		m_product_groups = new FLYProductGroupList();
		
	}

	/**
	 *  Get a list with product categories
	 *  @return the list list with product categories
	 *  */
	public List<FLYProductCategory> getProductCategories(BigDecimal parent){
		return m_product_categories.getProductCategories(parent);
	}

	/**
	 *  Get a list with product Classifications
	 *  @return the list list with product Classifications
	 *  */
	public List<FLYProductClassification> getProductClassifications(BigDecimal parent){
		return m_product_classifications.getProductClassifications(parent);
	}

	
	/**
	 *  Get a list with product Classes
	 *  @return the list list with product Classes
	 *  */
	public List<FLYProductClass> getProductClasses(BigDecimal parent){
		return m_product_classes.getProductClasses(parent);
	}

	
	/**
	 *  Get a list with product Groups
	 *  @return the list list with product Groups
	 *  */
	public List<FLYProductGroup> getProductGroups(BigDecimal parent){
		return m_product_groups.getProductGroups(parent);
	}
	
	public ArrayList<FLYBanknote> getBanknotes(Integer C_Currency_ID){
		return banknotes.get(C_Currency_ID);
	}

	public void setBanknotes(Integer C_Currency_ID, ArrayList<FLYBanknote> notes){
		banknotes.put(C_Currency_ID, notes);
	}

	public ArrayList<FLYBanknote> getCoins(Integer C_Currency_ID){
		return coins.get(C_Currency_ID);
	}

	public void setCoins(Integer C_Currency_ID, ArrayList<FLYBanknote> acoins){
		coins.put(C_Currency_ID, acoins);
	}



}
