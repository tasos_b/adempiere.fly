package org.fly.core;

import java.util.Properties;

import org.compiere.process.ProcessInfo;
import org.compiere.util.Trx;

public abstract class FLYProcessHandler {

	public abstract void handle(Properties ctx, ProcessInfo pi, Trx trx);

}
