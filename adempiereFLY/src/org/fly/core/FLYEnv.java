/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.adempiere.exceptions.DBException;
import org.compiere.model.Mlayout;
import org.compiere.model.Mlayoutcontrol;
import org.compiere.model.Mterminal;
import org.compiere.model.Mticket;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfo;
import org.compiere.util.Env;
import org.compiere.util.Ini;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.SQLUtil;
import org.fly.swing.FLYSwingController;



/**
 *  Application Environment Variables.

 * @author: tasos boulassikis tasosbull@gmail.com */
public class FLYEnv {
	
	public static FLYEnv instance;

	FLYCache m_cache = null;
	Mterminal m_terminal;
	int m_terminal_ID;
	List<Mlayout> m_layouts = null;
	Mlayout m_current_layout = null;

	int C_BPartner_ID = 0;
	int fl_ticket_id = 0;
	int C_Order_ID = 0;
	int C_Order_Line_ID = 0;
	int M_PriceList_ID = 0;
	int M_PriceList_Version_ID = 0;
	int M_Warehouse_ID = 0;
	int C_DocType_ID = 0;
	int SalesRep_ID = 0;
	int C_CashBook_ID = 0;
	int C_Currency_ID = 0;
	int C_BankAccount_ID = 0;
	boolean ismodifyprice = false;

	
	public final int ID_NEW_TICKET = FLYProcessUtil.getProcessID("Create_New_Ticket");
	public final int ID_FIND_TICKET= FLYProcessUtil.getProcessID("Find_Ticket");
	public final int ID_ADD_TICKET_LINE = FLYProcessUtil.getProcessID("Add_Ticket_Line");
	
	
	
	//Ini replacement
	public static final String P_TERMINAL = "Terminal";
	public static final String DEFAULT_TERMINAL = "Default Terminal";
	
	public static final String P_BUTTON_WIDTH = "ButtonWidth";
	public static final String DEFAULT_BUTTON_WIDTH= "130";
	
	public static final String P_BUTTON_HEIGHT = "ButtonHeight";
	public static final String DEFAULT_BUTTON_HEIGHT= "70";

	public static final String P_LAST_LAYOUT_ID = "LastLayoutID";
	public static final String DEFAULT_LAST_LAYOUT_ID= "0";

	/**
	 *	Create FLYEnv instance
	 */
	public static void initialize(){
		instance = new FLYEnv();
		
	}

	/**
	 * Standard Constructor
	 */
	public FLYEnv() {
		loadData();
		m_cache = new FLYCache();
		
	}
	
	/**
	 *	Get Terminal ID
	 *  @return Terminal ID
	 */
	public int getTerminalID(){
		
		return m_terminal_ID;
	}
	
	/**
	 *	Get Cache Object
	 *  @return Cache Object
	 */
	public FLYCache getCache(){
		return m_cache;
	}

	/**
	 *	Load environment variables
	 */
	public void loadData(){
		String terminal = getProperty(P_TERMINAL);
		m_terminal = Mterminal.get(Env.getCtx(), terminal);
		if(m_terminal != null){
			m_terminal_ID = m_terminal.get_ID();
			C_BPartner_ID = m_terminal.getC_BPartner_ID();
			M_PriceList_ID = m_terminal.getM_PriceList_ID();
			M_Warehouse_ID = m_terminal.getM_Warehouse_ID();
			C_DocType_ID = m_terminal.getC_DocType_ID();
			SalesRep_ID = m_terminal.getSalesRep_ID();
			C_CashBook_ID = m_terminal.getC_CashBook_ID();
			C_Currency_ID = getC_Currency_ID(C_CashBook_ID);
			C_BankAccount_ID = m_terminal.getC_BankAccount_ID();
			ismodifyprice = m_terminal.isModifyPrice();
			m_layouts = getLayouts(m_terminal_ID);
			m_current_layout = getLastLayout();
			setPriceListVersion();
			
		}
		
	}
	
	public String getProperty(String key){
		String result = "";
		if(key.equals(P_TERMINAL)){
			result = Ini.getProperty(key);
			if(result.equals(""))
				return DEFAULT_TERMINAL;
		}
		else if(key.equals(P_BUTTON_WIDTH)){
			result = Ini.getProperty(key);
			if(result.equals(""))
				return DEFAULT_BUTTON_WIDTH;
		}
		else if(key.equals(P_BUTTON_HEIGHT)){
			result = Ini.getProperty(key);
			if(result.equals(""))
				return DEFAULT_BUTTON_HEIGHT;
		}
		else if(key.equals(P_LAST_LAYOUT_ID)){
			result = Ini.getProperty(key);
			if(result.equals(""))
				return DEFAULT_LAST_LAYOUT_ID;
		}

		//P_LAST_LAYOUT_ID
		
		return result;
	}
	
	

	/**
	 *	Assign M_PriceList_Version_ID
	 */
	private void setPriceListVersion(){
		if(C_BPartner_ID != 0 && M_PriceList_ID != 0){
			HashMap<String, Object> data = Mticket.getM_PriceList_Version_ID(Env.getCtx(), M_PriceList_ID, C_BPartner_ID);
			if(data.containsKey("M_PriceList_Version_ID")){
				M_PriceList_Version_ID = (int)data.get("M_PriceList_Version_ID");
			}
		}
	}
	
	/**
	 *	Set a text to status bar
	 *  The color will be black 
	 *  @param status text
	 */
	public void setStatus(String status){
		FLYSwingController.instance.setStatus(status);
	}

	/**
	 *	Set an error text to status bar
	 *  The color will be red 
	 *  @param error text
	 */
	public void setError(String status){
		FLYSwingController.instance.setError(status);
	}
	

	/**
	 *	Set environment variables to a ProcessInfo
	 *  The color will be red 
	 *  @param pi ProcessInfo 
	 */
	public ProcessInfo setEnvVars(ProcessInfo pi){
		addParameter(pi, "AD_Client_ID", Env.getAD_Client_ID(Env.getCtx()));
		addParameter(pi, "AD_Org_ID", Env.getAD_Org_ID(Env.getCtx()));
		addParameter(pi, "AD_User_ID", Env.getAD_User_ID(Env.getCtx()));
		addParameter(pi, "C_DocType_ID", C_DocType_ID);
		addParameter(pi, "fl_terminal_id", m_terminal.get_ID());
		addParameter(pi, "C_BPartner_ID", m_terminal.getC_BPartner_ID());
		addParameter(pi, "M_PriceList_ID", m_terminal.getM_PriceList_ID());
		addParameter(pi, "M_PriceList_Version_ID", M_PriceList_Version_ID);
		addParameter(pi, "M_Warehouse_ID", m_terminal.getM_Warehouse_ID());
		addParameter(pi, "C_Order_ID", C_Order_ID);
		addParameter(pi, "C_Order_Line_ID", C_Order_Line_ID);
		addParameter(pi, "fl_ticket_id", fl_ticket_id);
		addParameter(pi, "SalesRep_ID", m_terminal.getSalesRep_ID());
		addParameter(pi, "C_CashBook_ID", m_terminal.getC_CashBook_ID());
		addParameter(pi, "C_Currency_ID", getC_Currency_ID( m_terminal.getC_CashBook_ID()));
		addParameter(pi, "C_BankAccount_ID", m_terminal.getC_BankAccount_ID());
		addParameter(pi, "isModifyPrice", m_terminal.isModifyPrice());
		
		addParameter(pi, "Error", " ");
		addParameter(pi, "Info", " ");
		
		return pi;
	}
	
	private void addParameter(ProcessInfo pi, String name, Object value){
		FLYProcessUtil.setParameter(pi, name, value);
	}
	
	public ProcessInfo getEnvVars(){
		ProcessInfo pi = new ProcessInfo("Env", 0);
		addParameter(pi, "AD_Client_ID", Env.getAD_Client_ID(Env.getCtx()));
		addParameter(pi, "AD_Org_ID", Env.getAD_Org_ID(Env.getCtx()));
		addParameter(pi, "AD_User_ID", Env.getAD_User_ID(Env.getCtx()));
		addParameter(pi, "C_DocType_ID", C_DocType_ID);
		addParameter(pi, "fl_terminal_id", m_terminal.get_ID());
		addParameter(pi, "C_BPartner_ID", m_terminal.getC_BPartner_ID());
		addParameter(pi, "M_PriceList_ID", m_terminal.getM_PriceList_ID());
		addParameter(pi, "M_PriceList_Version_ID", M_PriceList_Version_ID);
		addParameter(pi, "M_Warehouse_ID", m_terminal.getM_Warehouse_ID());
		addParameter(pi, "C_Order_ID", C_Order_ID);
		addParameter(pi, "C_Order_Line_ID", C_Order_Line_ID);
		addParameter(pi, "fl_ticket_id", fl_ticket_id);

		addParameter(pi, "SalesRep_ID", m_terminal.getSalesRep_ID());
		addParameter(pi, "C_CashBook_ID", m_terminal.getC_CashBook_ID());
		addParameter(pi, "C_Currency_ID", getC_Currency_ID( m_terminal.getC_CashBook_ID()));
		addParameter(pi, "C_BankAccount_ID", m_terminal.getC_BankAccount_ID());
		addParameter(pi, "isModifyPrice", m_terminal.isModifyPrice());

		
		addParameter(pi, "Error", " ");
		addParameter(pi, "Info", " ");
		
		return pi;
	}

	
	
	/**
	 *	Get the last selected terminal layout
	 *  @result Mlayout PO
	 */
	public Mlayout getLastLayout(){
		String str = getProperty(P_LAST_LAYOUT_ID);
		int lastLayoutID = Integer.parseInt(str);
		Optional<Mlayout> lastLayout = m_layouts
				.stream()
				.filter(p -> (p.get_ID() == lastLayoutID))
				.findFirst();
		if(lastLayout.isPresent())
			return lastLayout.get();
		return null;
	}
	
	/**
	 *	Get a list with all terminal layouts for the 
	 *    current terminal
	 *  @result List<Mlayout>
	 */
	public List<Mlayout> getLayouts (){
		return m_layouts;
	}
	
	/**
	 *	Get a list with all terminal layouts for a
	 *	specific terminal
	 * 	@param terminal_ID a terminal ID
	 *  @result List<Mlayout>
	 */
	private List<Mlayout> getLayouts (int terminal_ID){
		final String whereClause = "fl_terminal_ID=?";
		List<Mlayout> layouts = new Query(Env.getCtx(),Mlayout.Table_Name,whereClause,null)
		.setParameters(terminal_ID).list();
		return layouts;
	}
	
	/**
	 *	Get a list with all layout controls for a
	 *	specific layout
	 * 	@param layout_ID a layout ID
	 *  @result List<Mlayoutcontrol>
	 */
	public List<Mlayoutcontrol> getControls (int layout_ID){
		final String whereClause = "fl_layout_ID=?";
		List<Mlayoutcontrol> controls = new Query(Env.getCtx(),Mlayoutcontrol.Table_Name,whereClause,null)
		.setParameters(layout_ID).list();
		return controls;
	}

	/**
	 *	Get the current layout object
	 *	@return Mlayout object
	 */
	public Mlayout getM_current_layout() {
		return m_current_layout;
	}

	/**
	 *	Set a layout object as the current layout
	 *	and save it to Properties as the last layout used
	 *	@param m_current_layout the new current layout
	 */
	public void setM_current_layout(Mlayout m_current_layout) {
		this.m_current_layout = m_current_layout;
		Ini.setProperty(P_LAST_LAYOUT_ID, Integer.toString(m_current_layout.get_ID()));
	}
	
	/**
	 * 	Get the current Business Partner ID
	 * 	@return C_BPartner_ID
	 * 
	 */
	public int getC_BPartner_ID() {
		return C_BPartner_ID;
	}

	/**
	 * 	Set the current Business Partner ID
	 * 	@param m_defaultCustomer_ID the C_BPartner_ID
	 * 
	 */
	public void setC_BPartner_ID(int m_defaultCustomer_ID) {
		this.C_BPartner_ID = m_defaultCustomer_ID;
	}
	
	/**
	 * 	Get the ID of the current ticket
	 * 	@return fl_ticket_id
	 * 
	 */
	public int getFl_ticket_id() {
		return fl_ticket_id;
	}

	/**
	 * 	Set the ID of the current ticket
	 * 	@param fl_ticket_id the id
	 * 
	 */
	public void setFl_ticket_id(int fl_ticket_id) {
		this.fl_ticket_id = fl_ticket_id;
	}

	/**
	 * 	Get the ID of the current ticket's current order
	 * 	@return C_Order_ID the id
	 * 
	 */
	public int getC_Order_ID() {
		return C_Order_ID;
	}

	/**
	 * 	Set the ID of the current ticket's current order
	 * 	@param C_Order_ID the id
	 * 
	 */
	public void setC_Order_ID(int c_Order_ID) {
		C_Order_ID = c_Order_ID;
	}

	/**
	 * 	Get the ID of the 
	 * 	->current ticket's 
	 * 	->current order
	 * 	->current line(last selected line)
	 * 	@return C_Order_Line_ID the id
	 * 
	 */
	public int getC_Order_Line_ID() {
		return C_Order_Line_ID;
	}

	/**
	 * 	Set the ID of the 
	 * 	->current ticket's 
	 * 	->current order
	 * 	->current line(last selected line)
	 * 	@param C_Order_Line_ID the id
	 * 
	 */
	public void setC_Order_Line_ID(int c_Order_Line_ID) {
		C_Order_Line_ID = c_Order_Line_ID;
	}
	
	/**
	 * 	Get the Default PriceList for this Terminal 
	 * 	@return M_PriceList_ID the id
	 * 
	 */
	public int getM_PriceList_ID() {
		return M_PriceList_ID;
	}

	/**
	 * 	Set the Default PriceList for this Terminal 
	 * 	@param m_PriceList_ID the id
	 * 
	 */
	public void setM_PriceList_ID(int m_PriceList_ID) {
		M_PriceList_ID = m_PriceList_ID;
	}



	/**
	 * 	Get the Default PriceList Version for this Terminal 
	 * 	@return M_PriceList_Version_ID the id
	 * 
	 */
	public int getM_PriceList_Version_ID() {
		return M_PriceList_Version_ID;
	}

	/**
	 * 	Set the Default PriceList Version for this Terminal 
	 * 	@param M_PriceList_Version_ID the id
	 * 
	 */
	public void setM_PriceList_Version_ID(int m_PriceList_Version_ID) {
		M_PriceList_Version_ID = m_PriceList_Version_ID;
	}

	/**
	 * 	Get the Warehouse for this Terminal 
	 * 	@return M_Warehouse_ID the id
	 * 
	 */
	public int getM_Warehouse_ID() {
		return M_Warehouse_ID;
	}

	/**
	 * 	Set the Warehouse for this Terminal 
	 * 	@param M_Warehouse_ID the id
	 * 
	 */
	public void setM_Warehouse_ID(int m_Warehouse_ID) {
		M_Warehouse_ID = m_Warehouse_ID;
	}

	/**
	 * 	Get the DocType selected for this Terminal 
	 * 	@return C_DocType_ID the id
	 * 
	 */
	public int getC_DocType_ID() {
		return C_DocType_ID;
	}

	/**
	 * 	Set the DocType selected for this Terminal 
	 * 	@param C_DocType_ID the id
	 * 
	 */
	public void setC_DocType_ID(int c_DocType_ID) {
		C_DocType_ID = c_DocType_ID;
	}
	
	public static int getC_Currency_ID(int C_CashBook_ID){
		
		String sql = "SELECT C_Currency_ID " 
				+"from C_Cashbook "
				+"WHERE C_CashBook_ID=?";
		SQLUtil util = new SQLUtil(sql, null, C_CashBook_ID);
		
		try{
			ResultSet rs= util.getResultSet();
			while (rs.next()) {
				return rs.getInt("C_Currency_ID");
				
			}
			return 0;
		}catch (SQLException e) {
			throw new DBException(e, sql);
		}
		finally{
			util.close();
		}

	}

}
