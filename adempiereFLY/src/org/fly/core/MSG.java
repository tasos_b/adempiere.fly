package org.fly.core;

import org.compiere.util.Env;
import org.compiere.util.Msg;

public class MSG {
	public static String get(String msg){
		return Msg.translate(Env.getCtx(), msg);
	}
}
