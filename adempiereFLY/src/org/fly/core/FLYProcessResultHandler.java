/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.core;

import java.util.Properties;

import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.Trx;
import org.fly.core.utils.FLYProcessUtil;
import org.fly.core.utils.StringUtil;
import org.fly.swing.FLYSwingController;

/**
 * Handle the Process(Action) result Set in some cases some Environment globals
 * like a new ticket(and order) id
 * 
 * @author: tasos boulassikis tasosbull@gmail.com
 */
public class FLYProcessResultHandler extends FLYProcessHandler{

	/**
	 * Handle the Process(Action) result Set in some cases some Environment
	 * globals like a new ticket(and order) id
	 * 
	 * @param pi
	 *            the ProcessInfo to handle
	 */
	public void handle(Properties ctx, ProcessInfo pi, Trx trx) {
		final int ID_NEW_TICKET = FLYEnv.instance.ID_NEW_TICKET;
		final int ID_FIND_TICKET = FLYEnv.instance.ID_FIND_TICKET;
		ProcessInfoParameter[] params = pi.getParameter();
		ProcessInfoParameter param = FLYProcessUtil.getParameter(params,
				"Error");
		if (param != null) {
			String error = param.getParameterAsString();
			if ((!StringUtil.isNullOrEmpty(error)) && (!error.equals(" "))) {
				FLYEnv.instance.setError(param.getParameterAsString());
				return;
			}
		}
		
		if (pi.getAD_Process_ID() == ID_NEW_TICKET){

			param = FLYProcessUtil.getParameter(params, "fl_ticket_id");
			if (param != null)
				FLYEnv.instance.setFl_ticket_id(param.getParameterAsInt());
			param = FLYProcessUtil.getParameter(params, "C_Order_ID");
			if (param != null)
				FLYEnv.instance.setC_Order_ID(param.getParameterAsInt());
		}
		
		if (pi.getAD_Process_ID() == ID_FIND_TICKET){

			param = FLYProcessUtil.getParameter(params, "fl_ticket_id");
			if (param != null)
				FLYEnv.instance.setFl_ticket_id(param.getParameterAsInt());
			param = FLYProcessUtil.getParameter(params, "C_Order_ID");
			if (param != null)
				FLYEnv.instance.setC_Order_ID(param.getParameterAsInt());
			param = FLYProcessUtil.getParameter(params, "C_BPartner_ID");
			if (param != null)
				FLYEnv.instance.setC_BPartner_ID(param.getParameterAsInt());

		}

		param = FLYProcessUtil.getParameter(params, "C_Order_ID");
		if (param != null) {
			FLYSwingController.instance.refreshLines(param.getParameterAsInt());
			FLYSwingController.instance.refreshDocInfos(param
					.getParameterAsInt());
		}

	}

}
