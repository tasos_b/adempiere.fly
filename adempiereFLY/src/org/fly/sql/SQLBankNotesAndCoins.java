package org.fly.sql;
//test
public class SQLBankNotesAndCoins {
	public static String GET_BANKNOTES =
			"SELECT BN.name, "
			+"BN.moneyvalue, "
			+"IMG.binarydata "
			+"FROM fl_banknote BN "
			+"INNER JOIN AD_Image IMG "
			+"ON IMG.AD_Image_ID=BN.AD_Image_ID "
			+"WHERE BN.fl_payment_setup_id= "
			+"(SELECT fl_payment_setup_id FROM fl_payment_setup "
			+"where C_Currency_ID=?) "
			+"ORDER BY BN.line ASC";


	public static String GET_COINS =
			"SELECT CN.name, "
			+"CN.moneyvalue, "
			+"IMG.binarydata "
			+"FROM fl_coin CN "
			+"INNER JOIN AD_Image IMG "
			+"ON IMG.AD_Image_ID=CN.AD_Image_ID "
			+"WHERE CN.fl_payment_setup_id= "
			+"(SELECT fl_payment_setup_id FROM fl_payment_setup "
			+"where C_Currency_ID=?) "
			+"ORDER BY CN.line ASC";




}
