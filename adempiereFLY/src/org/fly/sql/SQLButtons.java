/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.sql;

public class SQLButtons {

	public static String GET_BUTTONS =

	"SELECT BTN.name, " 														
	+"BTN.buttoninfo, "															
	+"BTN.fl_process_group_id, " 												
	+"BTN.fl_process_id, "       												
	+"FLPR.data, " 																
	+"PR.AD_Process_ID, " 														
	+"PR.classname " 															
	+"FROM fl_layoutcontrol_button BTN " 										
	+"LEFT OUTER JOIN fl_process FLPR "											
	+"	ON(BTN.fl_process_id=FLPR.fl_process_id) "								
	+"LEFT OUTER JOIN AD_PROCESS PR "											
	+"	ON(FLPR.AD_PROCESS_ID=PR.AD_PROCESS_ID) "								
	+"WHERE BTN.fl_layoutcontrol_id=? "											
	+"ORDER BY BTN.line ASC";


	
	public static String GET_PROCESS_GROUP =

	"SELECT FLPR.data, " 														
	+"PR.AD_Process_ID, " 														
	+"PR.classname "																
	+"FROM fl_process_group_link LINK " 											
	+"LEFT OUTER JOIN fl_process FLPR "											
	+"	ON(LINK.fl_process_id=FLPR.fl_process_id) "								
	+"LEFT OUTER JOIN AD_PROCESS PR "											
	+"	ON(FLPR.AD_PROCESS_ID=PR.AD_PROCESS_ID) "								
	+"WHERE LINK.fl_process_group_id=? " 										
	+"ORDER BY LINK.line asc ";

	
}
