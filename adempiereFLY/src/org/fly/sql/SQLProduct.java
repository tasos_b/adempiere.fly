/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) metas All Rights Reserved.                                   *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *                                                                            *
 * @author: tasos boulassikis tasosbull@gmail.com                                                     *
 *****************************************************************************/

package org.fly.sql;

public class SQLProduct {

	public static String GET_PRODUCT_CATEGORIES =

	"SELECT PC.M_Product_Category_ID, " 
	+ "PC.AD_Org_ID, "
	+ "PC.M_Product_Category_Parent_ID, " 
	+ "PC.Name, "
	+ "PC.Description, " 
	+ "PC.buttoninfo "
	+ "FROM M_Product_Category PC " 
	+ "WHERE PC.AD_Client_ID=? "
	+ "AND PC.IsActive='Y' ";

	public static String GET_PRODUCT_CLASSIFICATIONS =

	"SELECT PC.M_Product_Classification_ID, " 
	+ "PC.AD_Org_ID, "
	+ "PC.M_Classification_Parent_ID, " 
	+ "PC.Name, "
	+ "PC.Description, " 
	+ "PC.buttoninfo "
	+ "FROM M_Product_Classification PC "
	+ "WHERE PC.AD_Client_ID=? "
	+ "AND PC.IsActive='Y' ";

	public static String GET_PRODUCT_CLASSES =

	"SELECT PC.M_Product_Class_ID, " 
	+ "PC.AD_Org_ID, "
	+ "PC.M_Product_Class_Parent_ID, " 
	+ "PC.Name, "
	+ "PC.Description, " 
	+ "PC.buttoninfo "
	+ "FROM M_Product_Class PC "
	+ "WHERE PC.AD_Client_ID=? "
	+ "AND PC.IsActive='Y' ";

	public static String GET_PRODUCT_GROUPS =

	"SELECT PC.M_Product_Group_ID, " 
	+ "PC.AD_Org_ID, "
	+ "PC.M_Product_Group_Parent_ID, " 
	+ "PC.Name, "
	+ "PC.Description, " 
	+ "PC.buttoninfo "
	+ "FROM M_Product_Group PC "
	+ "WHERE PC.AD_Client_ID=? "
	+ "AND PC.IsActive='Y' ";

	public static String GET_PRODUCTS_BY_CATEGORY =

	"SELECT PD.M_PRODUCT_ID, " 
	+ "PD.Value, " 
	+ "PD.Name, " 
	+ "PD.buttoninfo "
	+ "FROM M_Product PD "
	+ "WHERE PD.AD_Client_ID=? "
	+ "AND PD.M_Product_Category_ID=? " 
	+ "AND PD.IsActive='Y' ";

	public static String GET_PRODUCTS_BY_CLASSIFICATION =

	"SELECT PD.M_PRODUCT_ID, " 
	+ "PD.Value, " 
	+ "PD.Name, " 
	+ "PD.buttoninfo "
	+ "FROM M_Product PD "
	+ "WHERE PD.AD_Client_ID=? "
	+ "AND PD.M_Product_Classification_ID=? " 
	+ "AND PD.IsActive='Y' ";
	
	public static String GET_PRODUCTS_BY_CLASS =

	"SELECT PD.M_PRODUCT_ID, " 
	+ "PD.Value, " 
	+ "PD.Name, " 
	+ "PD.buttoninfo "
	+ "FROM M_Product PD "
	+ "WHERE PD.AD_Client_ID=? "
	+ "AND PD.M_Product_Class_ID=? " 
	+ "AND PD.IsActive='Y' ";
	
	public static String GET_PRODUCTS_BY_GROUP =

	"SELECT PD.M_PRODUCT_ID, " 
	+ "PD.Value, " 
	+ "PD.Name, " 
	+ "PD.buttoninfo "
	+ "FROM M_Product PD "
	+ "WHERE PD.AD_Client_ID=? "
	+ "AND PD.M_Product_Group_ID=? " 
	+ "AND PD.IsActive='Y' ";
	
	
}
